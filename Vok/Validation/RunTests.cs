﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;

namespace Vok.Validation
{
    static class RunTests
    {
        /* Runs all of the tests in the class with the assembly-qualified name classname.
         * All tests in the class must be public and static, take no parameters, and return a bool.
         */
        public static string RunTestsFor(string classname)
        {
            string output = "";
            Type type = Type.GetType(classname);

            if (type != null)
            {
                // Only gets public and static methods with no parameters and bool return type.
                IEnumerable<MethodInfo> methodInfos = type.GetMethods(BindingFlags.Public | BindingFlags.Static)
                    .Where(x => x.ReturnType.Name.Equals("Boolean") && x.GetParameters().Length == 0);

                // Can only run tests if they exist.
                if (methodInfos.Count() == 0)
                {
                    output = "No tests found.";
                }
                else
                {
                    // Runs all tests, adding a note if a test fails.
                    foreach (MethodInfo method in methodInfos)
                    {
                        Func<bool> ans = (Func<bool>)method.CreateDelegate(typeof(Func<bool>));
                        if (!ans())
                        {
                            string name = ans.Method.Name;
                            output += "Test " + name + " failed.\n";
                        }
                    }

                    // No notes added indicates that all tests returned true.
                    if (output.Equals(""))
                    {
                        output = "All tests passed.\n";
                    }
                }
            }
            else
            {
                output = "Class not found.\n";
            }

            return output;
        }

        /* Runs all of the asynchronous tests in the class with the assembly-qualified name classname.
         * All tests in the class must be public and static, take no parameters, and return a Task.
         */
        public static async Task<string> RunTestsForAsync(string classname)
        {
            string output = "";
            Type type = Type.GetType(classname);

            if (type != null)
            {
                // Only gets public and static methods with no parameters and bool return type.
                IEnumerable<MethodInfo> methodInfos = type.GetMethods(BindingFlags.Public | BindingFlags.Static)
                    .Where(x => x.ReturnType.Name.Equals("Task`1") && x.GetParameters().Length == 0);

                // Can only run tests if they exist.
                if (methodInfos.Count() == 0)
                {
                    output = "No tests found.";
                }
                else
                {
                    // Runs all tests, adding a note if a test fails.
                    foreach (MethodInfo method in methodInfos)
                    {
                        Func<Task<bool>> test = (Func<Task<bool>>)method.CreateDelegate(typeof(Func<Task<bool>>));
                        bool result = await test();
                        if (!result)
                        {
                            string name = test.Method.Name;
                            output += "Test " + name + " failed.\n";
                        }
                    }

                    // No notes added indicates that all tests returned true.
                    if (output.Equals(""))
                    {
                        output = "All tests passed.\n";
                    }
                }
            }
            else
            {
                output = "Class not found.\n";
            }

            return output;
        }
    }
}
