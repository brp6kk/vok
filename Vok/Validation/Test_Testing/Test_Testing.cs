﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vok.Src.Test;
using Vok.Src.Database;

namespace Vok.Validation.Test_Testing
{
    /* Tests for the Vok.Src.Test.Test class.
     */
    static class Test_Testing
    {
        /* Ensures that the CreateTest method properly returns a Test
         * when passed valid parameters.
         */ 
        public static bool TestCreationValid()
        {
            List<Word> terms = new List<Word>() { new Word("Word1"), new Word("Word2") };
            List<string> promptFormat = new List<string>() { "Definitions" };
            List<string> answerFormat = new List<string>() { "Term" };
            Test test = Test.CreateTest(terms, promptFormat, answerFormat, false);

            bool check = test != null && test.CurrentWordIndex == 0 && 
                         test.TestLength == 2 && test.NumberCorrect == 0 && 
                         !test.Graded;

            return check;
        }

        /* Ensures that the CreateTest method properly returns null
         * when any of the parameters are null
         * and when any of the list parameters contains null value(s).
         */ 
        public static bool TestCreationNull()
        {
            bool check = TestCreationValid();

            // Lists are null.
            Test test = Test.CreateTest(null, null, null, false);
            check = check && test is null;

            List<Word> terms = new List<Word>() { new Word("Word1"), null, new Word("Word2") };
            List<string> promptFormat = new List<string>() { null, "Definitions" };
            List<string> answerFormat = new List<string>() { "Term", null };

            // Lists contain null.
            test = Test.CreateTest(terms, promptFormat, answerFormat, false);
            check = check && test is null;

            // Combination lists contain null & list is null.
            test = Test.CreateTest(terms, promptFormat, null, false);
            check = check && test is null;

            // Combination valid list, null in list, and null list.
            terms = new List<Word>() { new Word("Word1"), new Word("Word2") };
            test = Test.CreateTest(terms, promptFormat, null, false);
            check = check && test is null;

            return check;
        }

        /* Ensures that the CreateTest method properly returns null
         * when any of the list parameters have 0 entries.
         */ 
        public static bool TestCreationEmpty()
        {
            // All empty lists.
            Test test = Test.CreateTest(new List<Word>(), new List<string>(), new List<string>(), false);
            bool check = TestCreationValid() && test is null;

            List<Word> terms = new List<Word>() { new Word("Word1"), new Word("Word2") };
            List<string> answerFormat = new List<string>() { "Term" };

            // Some empty lists, some non-empty lists.
            test = Test.CreateTest(terms, new List<string>(), answerFormat, false);
            check = check && test is null;


            return check;
        }

        /* Ensures that the CreateTest method properly returns null
         * when any of the contents of promptFormat are not legal 
         * field names or alternative spelling types.
         * Note: ChineseA from testDB should be loaded into Vok.
         */ 
        public static bool TestCreationPromptFormat()
        {
            List<Word> terms = new List<Word>() { new Word("Word1"), new Word("Word2") };
            List<string> promptFormat = new List<string>() { "Pinyin" };
            List<string> answerFormat = new List<string>() { "Term" };

            // Alternative spelling type is valid prompt format.
            Test test = Test.CreateTest(terms, promptFormat, answerFormat, false);
            bool check = test != null;

            // List includes invalid prompt format.
            promptFormat.Add("Not pinyin");
            test = Test.CreateTest(terms, promptFormat, answerFormat, false);
            check = check && test is null;

            // List consists only of invalid prompt format.
            promptFormat.RemoveAt(0);
            test = Test.CreateTest(terms, promptFormat, answerFormat, false);
            check = check && test is null;

            return check;
        }

        /* Ensures that the CreateTest method properly returns null
         * when any of the contents of answerFormat are not legal
         * field names or alternative spelling types.
         * Note: ChineseA from testDB should be loaded into Vok.
         */ 
        public static bool TestCreationAnswerFormat()
        {
            List<Word> terms = new List<Word>() { new Word("Word1"), new Word("Word2") };
            List<string> promptFormat = new List<string>() { "Term" };
            List<string> answerFormat = new List<string>() { "Pinyin" };

            // Alternative spelling type is valid answer format.
            Test test = Test.CreateTest(terms, promptFormat, answerFormat, false);
            bool check = test != null;

            // List includes invalid answer format.
            answerFormat.Add("Not pinyin");
            test = Test.CreateTest(terms, promptFormat, answerFormat, false);
            check = check && test is null;

            // List consists only of invalid answer format.
            answerFormat.RemoveAt(0);
            test = Test.CreateTest(terms, promptFormat, answerFormat, false);
            check = check && test is null;

            return check;
        }

        /* Ensures that the IsTestOver method properly returns false
         * when the test is not over, ie if the CurrentWordIndex
         * is less than or equal the test length.
         */ 
        public static bool TestIsTestOverValidFalse()
        {
            List<Word> terms = new List<Word>() { new Word("Word1"), new Word("Word2") };
            List<string> promptFormat = new List<string>() { "Definitions" };
            List<string> answerFormat = new List<string>() { "Term" };
            Test test = Test.CreateTest(terms, promptFormat, answerFormat, false);

            bool check = TestCreationValid() && !test.IsTestOver();

            return check;
        }

        /* Ensures that the NextPrompt method properly updates
         * CurrentWordIndex and returns the expected prompt.
         */ 
        public static bool TestNextPromptValid()
        {
            List<Word> terms = new List<Word>() { new Word("Word1"), new Word("Word2") };
            terms[0].AddDefinition("Definition1");
            terms[1].AddDefinition("Definition2");
            List<string> promptFormat = new List<string>() { "Definitions" };
            List<string> answerFormat = new List<string>() { "Term" };
            Test test = Test.CreateTest(terms, promptFormat, answerFormat, false);

            Dictionary<string, string> prompt = test?.NextPrompt();

            bool check = prompt != null && prompt.Keys.Contains("Definitions") && 
                         prompt["Definitions"].Equals("Definition2");

            return check;
        }

        /* Ensures that the NextPrompt method properly returns
         * null when going to the next prompt results in a
         * test over condition.
         */ 
        public static bool TestNextPromptTestOver()
        {
            List<Word> terms = new List<Word>() { new Word("Word1"), new Word("Word2") };
            terms[0].AddDefinition("Definition1");
            terms[1].AddDefinition("Definition2");
            List<string> promptFormat = new List<string>() { "Definitions" };
            List<string> answerFormat = new List<string>() { "Term" };
            Test test = Test.CreateTest(terms, promptFormat, answerFormat, false);

            test?.NextPrompt();
            Dictionary<string, string> prompt = test?.NextPrompt();

            bool check = TestNextPromptValid() && prompt is null;

            return check;
        }

        /* Ensures that the IsTestOver method properly returns true
         * when CurrentWordIndex is greater than or equal to TestLength.
         * Note that NextPrompt needs to be at least partially implemented
         * since that is the only way for outside code to update CurrentWordIndex.
         */ 
        public static bool TestIsTestOverValidTrue()
        {
            List<Word> terms = new List<Word>() { new Word("Word1"), new Word("Word2") };
            terms[0].AddDefinition("Definition1");
            terms[1].AddDefinition("Definition2");
            List<string> promptFormat = new List<string>() { "Definitions" };
            List<string> answerFormat = new List<string>() { "Term" };
            Test test = Test.CreateTest(terms, promptFormat, answerFormat, false);

            test?.NextPrompt();
            test?.NextPrompt();

            bool check = TestIsTestOverValidFalse() && test.IsTestOver();

            return check;
        }

        /* Ensures that the GetCurrentPrompt method properly returns
         * the first prompt when the test starts.
         */ 
        public static bool TestGetCurrentPromptStart()
        {
            List<Word> terms = new List<Word>() { new Word("Word1"), new Word("Word2") };
            terms[0].AddDefinition("Definition1");
            terms[1].AddDefinition("Definition2");
            List<string> promptFormat = new List<string>() { "Definitions" };
            List<string> answerFormat = new List<string>() { "Term" };
            Test test = Test.CreateTest(terms, promptFormat, answerFormat, false);

            Dictionary<string, string> prompt = test?.GetCurrentPrompt();

            bool check = prompt != null && prompt.Keys.Contains("Definitions") && 
                         prompt["Definitions"].Equals("Definition1");

            return check;
        }

        /* Ensures that the GetCurrentPrompt method properly returns
         * a prompt after progressing through the test
         * but before the test is over.
         */ 
        public static bool TestGetCurrentPromptMiddle()
        {
            List<Word> terms = new List<Word>() { new Word("Word1"), new Word("Word2"), new Word("Word3")};
            terms[0].AddDefinition("Definition1");
            terms[1].AddDefinition("Definition2");
            terms[2].AddDefinition("Definition3");
            List<string> promptFormat = new List<string>() { "Definitions" };
            List<string> answerFormat = new List<string>() { "Term" };
            Test test = Test.CreateTest(terms, promptFormat, answerFormat, false);

            test?.NextPrompt();
            Dictionary<string, string> prompt = test?.GetCurrentPrompt();

            bool check = prompt != null && prompt.Keys.Contains("Definitions") &&
                         prompt["Definitions"].Equals("Definition2");

            return check;
        }

        /* Ensures that the GetCurrentPrompt method properly returns
         * null if the test is over.
         */ 
        public static bool TestGetCurrentPromptTestOver()
        {
            List<Word> terms = new List<Word>() { new Word("Word1"), new Word("Word2") };
            terms[0].AddDefinition("Definition1");
            terms[1].AddDefinition("Definition2");
            List<string> promptFormat = new List<string>() { "Definitions" };
            List<string> answerFormat = new List<string>() { "Term" };
            Test test = Test.CreateTest(terms, promptFormat, answerFormat, false);

            test?.NextPrompt();
            test?.NextPrompt();
            Dictionary<string, string> prompt = test?.GetCurrentPrompt();

            bool check = TestGetCurrentPromptStart() && prompt is null;

            return check;
        }

        /* Ensures that the GetCurrentAnswer method properly returns
         * the first answer when the test starts.
         */
        public static bool TestGetCurrentAnswerStart()
        {
            List<Word> terms = new List<Word>() { new Word("Word1"), new Word("Word2") };
            terms[0].AddDefinition("Definition1");
            terms[1].AddDefinition("Definition2");
            List<string> promptFormat = new List<string>() { "Definitions" };
            List<string> answerFormat = new List<string>() { "Term" };
            Test test = Test.CreateTest(terms, promptFormat, answerFormat, false);

            Dictionary<string, string> answer = test?.GetCurrentAnswer();

            bool check = answer != null && answer.Keys.Contains("Term") &&
                         answer["Term"].Equals("Word1");

            return check;
        }

        /* Ensures that the GetCurrentAnswer method properly returns
         * an answer after progressing through the test
         * but before the test is over.
         */
        public static bool TestGetCurrentAnswerMiddle()
        {
            List<Word> terms = new List<Word>() { new Word("Word1"), new Word("Word2"), new Word("Word3") };
            terms[0].AddDefinition("Definition1");
            terms[1].AddDefinition("Definition2");
            terms[2].AddDefinition("Definition3");
            List<string> promptFormat = new List<string>() { "Definitions" };
            List<string> answerFormat = new List<string>() { "Term" };
            Test test = Test.CreateTest(terms, promptFormat, answerFormat, false);

            test?.NextPrompt();
            Dictionary<string, string> answer = test?.GetCurrentAnswer();

            bool check = answer != null && answer.Keys.Contains("Term") &&
                         answer["Term"].Equals("Word2");

            return check;
        }

        /* Ensures that the GetCurrentAnswer method properly returns
         * null if the test is over.
         */
        public static bool TestGetCurrentAnswerTestOver()
        {
            List<Word> terms = new List<Word>() { new Word("Word1"), new Word("Word2") };
            terms[0].AddDefinition("Definition1");
            terms[1].AddDefinition("Definition2");
            List<string> promptFormat = new List<string>() { "Definitions" };
            List<string> answerFormat = new List<string>() { "Term" };
            Test test = Test.CreateTest(terms, promptFormat, answerFormat, false);

            test?.NextPrompt();
            test?.NextPrompt();
            Dictionary<string, string> answer = test?.GetCurrentAnswer();

            bool check = TestGetCurrentAnswerStart() && answer is null;

            return check;
        }

        /* Ensures that the SubmitAnswer method properly returns true
         * when the passed answer matches the expected answer.
         */ 
        public static bool TestSubmitAnswerCorrect()
        {
            List<Word> terms = new List<Word>() { new Word("Word1"), new Word("Word2") };
            terms[0].AddDefinition("Definition1");
            terms[1].AddDefinition("Definition2");
            List<string> promptFormat = new List<string>() { "Definitions" };
            List<string> answerFormat = new List<string>() { "Term" };
            Test test = Test.CreateTest(terms, promptFormat, answerFormat, false);

            Dictionary<string, string> answer = new Dictionary<string, string>() { { "Term", "Word1" } };
            bool check = test != null && test.SubmitAnswer(answer);

            return check;
        }

        /* Ensures that the SubmitAnswer method properly returns true
         * when the passed answer (which has multiple parts) matches
         * the expected answer.
         */ 
        public static bool TestSubmitAnswerCorrectMultiple()
        {
            List<Word> terms = new List<Word>() { new Word("Word1"), new Word("Word2") };
            terms[0].AddDefinition("Definition1");
            terms[0].AddAlternativeSpelling("Pinyin", "Pinyin1");
            terms[1].AddDefinition("Definition2");
            terms[1].AddAlternativeSpelling("Pinyin", "Pinyin2");
            List<string> promptFormat = new List<string>() { "Definitions" };
            List<string> answerFormat = new List<string>() { "Term", "Pinyin" };
            Test test = Test.CreateTest(terms, promptFormat, answerFormat, false);

            Dictionary<string, string> answer = new Dictionary<string, string>() { { "Term", "Word1" }, { "Pinyin", "Pinyin1" } };
            bool check = test != null && test.SubmitAnswer(answer);

            return check;
        }

        /* Ensures that the SubmitAnswer method properly returns false
         * when the passed answer has the same keys, but at least one value
         * does not match the expected answer.
         */ 
        public static bool TestSubmitAnswerIncorrect()
        {
            List<Word> terms = new List<Word>() { new Word("Word1"), new Word("Word2") };
            terms[0].AddDefinition("Definition1");
            terms[0].AddAlternativeSpelling("Pinyin", "Pinyin1");
            terms[1].AddDefinition("Definition2");
            terms[1].AddAlternativeSpelling("Pinyin", "Pinyin2");
            List<string> promptFormat = new List<string>() { "Definitions" };
            List<string> answerFormat = new List<string>() { "Term", "Pinyin" };
            Test test = Test.CreateTest(terms, promptFormat, answerFormat, false);

            // All parts of answer are wrong.
            Dictionary<string, string> answer = new Dictionary<string, string>() { { "Term", "Word2" }, { "Pinyin", "Pinyin2" } };
            bool check = TestSubmitAnswerCorrect() && test != null && 
                         !test.SubmitAnswer(answer);

            // One part of answer is wrong.
            answer = new Dictionary<string, string>() { { "Term", "Word1" }, { "Pinyin", "Pinyin2" } };
            check = check && test != null && !test.SubmitAnswer(answer);

            return check;
        }

        /* Ensures that the SubmitAnswer method properly returns false
         * when the dictionary is null.
         * Note that a dictionary entry cannot be null, 
         * so this is not tested.
         */ 
        public static bool TestSubmitAnswerNull()
        {
            List<Word> terms = new List<Word>() { new Word("Word1"), new Word("Word2") };
            terms[0].AddDefinition("Definition1");
            terms[1].AddDefinition("Definition2");
            List<string> promptFormat = new List<string>() { "Definitions" };
            List<string> answerFormat = new List<string>() { "Term" };
            Test test = Test.CreateTest(terms, promptFormat, answerFormat, false);

            bool check = TestSubmitAnswerCorrect() && test != null && 
                         !test.SubmitAnswer(null);

            return check;
        }

        /* Ensures that the SubmitAnswer method properly returns false
         * when one of the dictionary keys is not a valid answer field.
         */ 
        public static bool TestSubmitAnswerInvalidKey()
        {
            List<Word> terms = new List<Word>() { new Word("Word1"), new Word("Word2") };
            terms[0].AddDefinition("Definition1");
            terms[1].AddDefinition("Definition2");
            List<string> promptFormat = new List<string>() { "Definitions" };
            List<string> answerFormat = new List<string>() { "Term" };
            Test test = Test.CreateTest(terms, promptFormat, answerFormat, false);

            Dictionary<string, string> answer = new Dictionary<string, string>() { { "Not Term", "Term1" } };
            bool check = TestSubmitAnswerCorrect() && test != null && 
                         !test.SubmitAnswer(answer);

            return check;
        }

        /* Ensures that the SubmitAnswer method properly returns false
         * when the user tries to submit an answer when the game is over.
         */ 
        public static bool TestSubmitAnswerTestOver()
        {
            List<Word> terms = new List<Word>() { new Word("Word1"), new Word("Word2") };
            terms[0].AddDefinition("Definition1");
            terms[1].AddDefinition("Definition2");
            List<string> promptFormat = new List<string>() { "Definitions" };
            List<string> answerFormat = new List<string>() { "Term" };
            Test test = Test.CreateTest(terms, promptFormat, answerFormat, false);

            test?.NextPrompt();
            test?.NextPrompt();

            Dictionary<string, string> answer = new Dictionary<string, string>() { { "Term", "Term3" } };
            bool check = TestSubmitAnswerCorrect() && test != null && 
                         !test.SubmitAnswer(answer);

            return check;
        }

        /* Ensures that the UpdateScore method properly updates
         * the current score when passed true.
         */ 
        public static bool TestUpdateScoreTrue()
        {
            List<Word> terms = new List<Word>() { new Word("Word1"), new Word("Word2") };
            List<string> promptFormat = new List<string>() { "Definitions" };
            List<string> answerFormat = new List<string>() { "Term" };
            Test test = Test.CreateTest(terms, promptFormat, answerFormat, false);

            bool check = test != null && test.NumberCorrect == 0;

            test?.UpdateScore(true);
            check = check && test.NumberCorrect == 1;

            test?.UpdateScore();
            check = check && test.NumberCorrect == 2;

            return check;
        }

        /* Ensures that the UpdateScore method does nothing
         * when passed false.
         */ 
        public static bool TestUpdateScoreFalse()
        {
            List<Word> terms = new List<Word>() { new Word("Word1"), new Word("Word2") };
            List<string> promptFormat = new List<string>() { "Definitions" };
            List<string> answerFormat = new List<string>() { "Term" };
            Test test = Test.CreateTest(terms, promptFormat, answerFormat, false);

            bool check = TestUpdateScoreTrue() && test != null && 
                         test.NumberCorrect == 0;

            test?.UpdateScore(false);
            check = check && test.NumberCorrect == 0;

            return check;
        }

        /* Ensures that the UpdateScore method properly updates
         * the current score, NumberTimesStudied, and NumberTimesCorrect
         * for a graded test when passed true.
         */ 
        public static bool TestUpdateScoreGraded()
        {
            List<Word> terms = new List<Word>() { new Word("Word1"), new Word("Word2") };
            List<string> promptFormat = new List<string>() { "Definitions" };
            List<string> answerFormat = new List<string>() { "Term" };
            Test test = Test.CreateTest(terms, promptFormat, answerFormat, true);

            bool check = TestUpdateScoreTrue() && test != null &&
                         test.NumberCorrect == 0 && terms[0].NumTimesStudied == 0 && 
                         terms[0].NumTimesCorrect == 0;

            test?.UpdateScore();
            check = check && test.NumberCorrect == 1 && 
                    terms[0].NumTimesStudied == 1 && terms[0].NumTimesCorrect == 1;

            test?.UpdateScore(false);
            check = check && test.NumberCorrect == 1 &&
                    terms[0].NumTimesStudied == 2 && terms[0].NumTimesCorrect == 1;

            return check;
        }

        /* Ensures that the UpdateScore method does nothing
         * when the test is over.
         */ 
        public static bool TestUpdateScoreTestOver()
        {
            List<Word> terms = new List<Word>() { new Word("Word1"), new Word("Word2") };
            List<string> promptFormat = new List<string>() { "Definitions" };
            List<string> answerFormat = new List<string>() { "Term" };
            Test test = Test.CreateTest(terms, promptFormat, answerFormat, false);

            test?.NextPrompt();
            test?.NextPrompt();

            bool check = TestUpdateScoreTrue() && test != null &&
                         test.NumberCorrect == 0;

            test?.UpdateScore();
            check = check && test.NumberCorrect == 0;

            return check;
        }
    }
}
