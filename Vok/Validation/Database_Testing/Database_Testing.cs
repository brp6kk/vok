﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vok.Src.Database;
using static Vok.Src.Common.Common;
using Vok.Src.Common;
using System.Reflection;

namespace Vok.Validation.Database_Testing
{
    static class Database_Testing
    {
        /* Tests that the constructor of the Database class works properly.
         * While the constructor is not asynchronous, this test is asynchronous
         * so that the testing mechanism picks it up.
         */ 
        public static async Task<bool> DatabaseConstructorTest()
        {
            Database db = await Database.CreateDatabase("Chinese");

            bool test = db.Language.Equals("Chinese");

            return test;
        }

        /* Tests that the LoadLanguageAsync method properly loads in and
         * alternative spelling types and characteristics.
         * Note that this test also checks FileIO.GetAlternativeSpellingTypesAsync
         * and FileIO.GetCharacteristicsAsync.
         */ 
        public static async Task<bool> DatabaseLoadTest()
        {
            Database db = await Database.CreateDatabase("ChineseA");
            // CreateDatabase automatically calls LoadLanguageAsync.

            // Check characteristics.
            bool test = db.Characteristics != null && db.Characteristics.Trees.Count == 3 &&
                        db.Characteristics.Search("family") != null;

            // Check alternative spelling types.
            test = test && db.AlternativeSpellingTypes != null && db.AlternativeSpellingTypes.Count == 2 &&
                   db.AlternativeSpellingTypes.Contains("Pinyin") && db.AlternativeSpellingTypes.Contains("Traditional");

            return test;
        }

        /* Tests that the AddWordsAsync method properly adds words
         * to the database when given a legal list of words.
         * Note that this test also checks FileIO.EditDatabaseAsync
         * and FileIO.QueryDatabaseAsync.
         */ 
        public static async Task<bool> DatabaseAddWordsValid()
        {
            // Establish database.
            Database db = await Database.CreateDatabase("ChineseA");

            // Add word to database.
            Word testWord = new Word("做", PartOfSpeech.Verb);
            testWord.AddDefinition("to do");
            testWord.AddCharacteristic("lesson02");
            testWord.AddAlternativeSpelling("Pinyin", "zuo4");
            await db.AddWordAsync(testWord);

            // Make sure word was added to database.
            string sql = "Select * from ChineseA_Terms where Term='做'";
            List<Word> results = await FileIO.QueryDatabaseAsync(sql);
            bool test = results != null && results.Count == 1 && results[0].Term.Equals("做");

            // Remove word from database, if it was added.
            if (test)
            {
                await db.RemoveWordAsync(testWord);
            }

            return test;
        }

        /* Tests that the AddWordsAsync method does not add
         * null words and can handle a null list.
         */ 
        public static async Task<bool> DatabaseAddWordsNull()
        {
            // Establish database.
            Database db = await Database.CreateDatabase("ChineseA");
            bool test = false;

            // Gets number of terms.
            string sql = "Select * from ChineseA_Terms";
            List<Word> results = await FileIO.QueryDatabaseAsync(sql);

            if (results != null)
            {
                int count = results.Count();

                // Pass null list.
                await db.AddWordsAsync(null);
                results = await FileIO.QueryDatabaseAsync(sql);
                test = (await DatabaseAddWordsValid()) && results != null && count == results.Count();
                // Pass list containing null Word.
                await db.AddWordsAsync(new List<Word>() { null });
                results = await FileIO.QueryDatabaseAsync(sql);
                test = test && results != null && count == results.Count();
            }

            return test;
        }

        /* Tests that the AddWordsAsync method does not add a word
         * to the database that already exactly matches (sans ID) 
         * a word already stored in the database.
         * Note that this test also checks FileIO.QueryDatabaseAsync.
         */ 
        public static async Task<bool> DatabaseAddWordsDuplicate()
        {
            // Establish database.
            Database db = await Database.CreateDatabase("ChineseA");
            bool test = false;

            // Gets number of terms.
            string sql = "Select * from ChineseA_Terms where Term='好'";
            List<Word> results = await FileIO.QueryDatabaseAsync(sql);

            if (results != null)
            {
                int count = results.Count();

                // Try to add word to database.
                Word testWord = new Word("好", PartOfSpeech.Adjective);
                testWord.AddDefinition("fine;good;nice;O.K.;it's settled");
                testWord.AddCharacteristic("ic/level01/lesson01");
                testWord.AddAlternativeSpelling("Pinyin", "hao3");
                await db.AddWordsAsync(new List<Word>() { testWord });

                // Ensure word was not actually added to database.
                results = await FileIO.QueryDatabaseAsync(sql);
                test = (await DatabaseAddWordsValid()) && results != null && count == results.Count();
            }

            return test;
        }

        /* Tests that the RemoveWordsAsync method properly removes words
         * from the database when given a legal list of words.
         * Note that this test also checks FileIO.EditDatabaseAsync.
         */ 
        public static async Task<bool> DatabaseRemoveWordsValid()
        {
            // Establish database.
            Database db = await Database.CreateDatabase("ChineseA");

            // Add word to database.
            Word testWord = new Word("做", PartOfSpeech.Verb);
            testWord.AddDefinition("to do");
            testWord.AddCharacteristic("lesson02");
            testWord.AddAlternativeSpelling("Pinyin", "zuo4");
            await db.AddWordsAsync(new List<Word>() { testWord });

            // Make sure word was added to database.
            string sql = "Select * from ChineseA_Terms where Term='做'";
            List<Word> results = await FileIO.QueryDatabaseAsync(sql);
            bool test = results != null && results.Count == 1 && results[0].Term.Equals("做");

            await db.RemoveWordAsync(testWord);

            // Make sure word was removed database.
            results = await FileIO.QueryDatabaseAsync(sql);
            test = test && results != null && results.Count == 0;

            return test;
        }

        /* Tests that the RemoveWordAsync method does nothing when passed
         * null words or a null list.
         */ 
        public static async Task<bool> DatabaseRemoveWordsNull()
        {
            // Establish database.
            Database db = await Database.CreateDatabase("ChineseA");
            bool test = false;

            // Gets number of terms.
            string sql = "Select * from ChineseA_Terms";
            List<Word> results = await FileIO.QueryDatabaseAsync(sql);

            if (results != null)
            {
                int count = results.Count();

                // Pass null list.
                await db.RemoveWordsAsync(null);
                results = await FileIO.QueryDatabaseAsync(sql);
                test = (await DatabaseRemoveWordsValid()) && results != null && count == results.Count();

                // Pass list containing null Word.
                await db.RemoveWordsAsync(new List<Word>() { null });
                results = await FileIO.QueryDatabaseAsync(sql);
                test = test && results != null && count == results.Count();
            }

            return test;
        }

        /* Tests that the EditWordAsync method properly edits a word
         * in the database when given legal words.
         * Note that this test also checks FileIO.EditDatabaseAsync
         * and FileIO.QueryDatabaseAsync.
         */ 
        public static async Task<bool> DatabaseEditWordValid()
        {
            Database db = await Database.CreateDatabase("ChineseA");
            bool test = false;

            // Set up words.
            Word oldWord = new Word("妈妈", PartOfSpeech.Noun);
            oldWord.AddDefinition("mother, mom");
            oldWord.AddAlternativeSpelling("Pinyin", "ma1ma");
            oldWord.AddAlternativeSpelling("Traditional", "媽媽");
            oldWord.AddCharacteristic("ic/level01/lesson02");
            oldWord.AddCharacteristic("family");
            Word newWord = new Word("母亲", PartOfSpeech.Noun);
            newWord.AddDefinition("mother, mom");
            newWord.AddAlternativeSpelling("Pinyin", "mu3qin1");
            newWord.AddAlternativeSpelling("Traditional", "母親");

            // Ensure old word already exists in database & new word does not.
            string sql = "Select * from ChineseA_Terms where Term='妈妈'";
            List<Word> results = await FileIO.QueryDatabaseAsync(sql);
            test = results != null && results.Count == 1;

            sql = "Select * from ChineseA_Terms where Term='母亲'";
            results = await FileIO.QueryDatabaseAsync(sql);
            test = test && results != null && results.Count == 0;

            if (test)
            {
                await db.EditWordAsync(oldWord, newWord);

                // 妈妈 should no longer be in database.
                sql = "Select * from ChineseA_Terms where Term='妈妈'";
                results = await FileIO.QueryDatabaseAsync(sql);
                test = test && results != null && results.Count == 0;

                // 母亲 should now be in database.
                sql = "Select * from ChineseA_Terms where Term='母亲'";
                results = await FileIO.QueryDatabaseAsync(sql);
                test = test && results != null && results.Count == 1;

                // Reset word edit if successful test.
                if (test)
                {
                    await db.EditWordAsync(newWord, oldWord);
                }
            }

            return test;
        }

        /* Tests that the EditWordAsync method does nothing when
         * either/both passed words are null.
         */ 
        public static async Task<bool> DatabaseEditWordNull()
        {
            Database db = await Database.CreateDatabase("ChineseA");
            bool test = false;

            // Set up words.
            Word oldWord = new Word("妈妈", PartOfSpeech.Noun);
            oldWord.AddDefinition("mother, mom");
            oldWord.AddAlternativeSpelling("Pinyin", "ma1ma");
            oldWord.AddAlternativeSpelling("Traditional", "媽媽");
            oldWord.AddCharacteristic("ic/level01/lesson02");
            oldWord.AddCharacteristic("family");
            Word newWord = new Word("母亲", PartOfSpeech.Noun);
            newWord.AddDefinition("mother, mom");
            newWord.AddAlternativeSpelling("Pinyin", "mu3qin1");
            newWord.AddAlternativeSpelling("Traditional", "母親");

            // Ensure old word already exists in database.
            string sql = "Select * from ChineseA_Terms where Term='妈妈'";
            List<Word> results = await FileIO.QueryDatabaseAsync(sql);
            test = (await DatabaseEditWordValid()) && results != null && results.Count == 1;

            if (test)
            {
                // Pass two null words.
                // Unable to check this programmatically,
                // but presumably it works if no exceptions are thrown.
                await db.EditWordAsync(null, null);

                // Pass null old word.
                sql = "Select * from ChineseA_Terms where Term='母亲'";
                await db.EditWordAsync(null, newWord);
                results = await FileIO.QueryDatabaseAsync(sql);
                test = test && results != null && results.Count() == 0;

                // Pass null new word.
                sql = "Select * from ChineseA_Terms where Term='妈妈'";
                await db.EditWordAsync(oldWord, null);
                results = await FileIO.QueryDatabaseAsync(sql);
                test = test && results != null && results.Count() == 1;
            }

            return test;
        }

        /* Tests that the EditWordAsync method does nothing when
         * the old word does not exist in the database.
         * Note that this test also checks FileIO.QueryDatabaseAsync.
         */ 
        public static async Task<bool> DatabaseEditWordNonExistentOld()
        {
            Database db = await Database.CreateDatabase("ChineseA");
            bool test = false;

            // Set up words.
            Word oldWord = new Word("爷爷", PartOfSpeech.Noun);
            oldWord.AddDefinition("paternal grandfather");
            oldWord.AddAlternativeSpelling("Pinyin", "ye2ye");
            oldWord.AddAlternativeSpelling("Traditional", "爺爺");
            Word newWord = new Word("祖父", PartOfSpeech.Noun);
            newWord.AddDefinition("paternal grandfather");
            newWord.AddAlternativeSpelling("Pinyin", "zu3fu4");
            newWord.AddAlternativeSpelling("Traditional", "");

            // Ensure old word already exists in database & new word does not.
            string sql = "Select * from ChineseA_Terms where Term='爷爷'";
            List<Word> results = await FileIO.QueryDatabaseAsync(sql);
            test = (await DatabaseEditWordValid()) && results != null && results.Count == 0;

            if (test)
            {
                await db.EditWordAsync(oldWord, newWord);
                results = await FileIO.QueryDatabaseAsync(sql);
                test = test && results != null && results.Count == 0;
            }

            return test;
        }

        /* **************************************************************************************************************************
         * TO-DO: After implementing SearchCommand class, 
         * write tests for Database's SearchAsync method.
         */ 

        /* Tests that the RandomTermsAsync method returns a random list of terms
         * given a parameter smaller than the database size.
         */ 
        public static async Task<bool> DatabaseRandomTermsValid()
        {
            Database db = await Database.CreateDatabase("ChineseA");
            bool test = false;

            List<Word> rand1 = await db.RandomTermsAsync(10);
            List<Word> rand2 = await db.RandomTermsAsync(10);

            if (rand1 != null && rand2 != null && rand1.Count == 10 && rand2.Count == 10)
            {
                // Technically, this could result in a false negative, 
                // since there is a possibility that all of the same
                // terms will be randomly chosen.
                for (int index = 0; index < 10; index++)
                {
                    if (!rand1.Contains(rand2[index]))
                    {
                        test = true;
                    }
                }
            }

            return test;
        }

        /* Tests that the RandomTermsAsync method returns the full list of terms
         * given a parameter greater than or equal to the number of terms in 
         * the database.
         * Note that this test also checks FileIO.QueryDatabaseAsync.
         */ 
        public static async Task<bool> DatabaseRandomTermsLargeNum()
        {
            Database db = await Database.CreateDatabase("ChineseA");
            bool test = false;

            List<Word> rand = await db.RandomTermsAsync(100);

            string sql = "Select * from ChineseA_Terms";
            List<Word> results = await FileIO.QueryDatabaseAsync(sql);

            test = rand != null && results != null && rand.Count == results.Count;

            return test;
        }

        /* Tests that the RandomTermsAsync method returns null 
         * when passed an integer less than 1.
         */ 
        public static async Task<bool> DatabaseRandomTermsNegative()
        {
            Database db = await Database.CreateDatabase("ChineseA");
            bool test = false;

            List<Word> resultsNegative = await db.RandomTermsAsync(-53);
            List<Word> resultsZero = await db.RandomTermsAsync(0);

            test = (await DatabaseRandomTermsValid()) && resultsNegative is null && resultsZero is null;

            return test;
        }

        /* Tests that the AddCharacteristicAsync method properly adds
         * a characteristic to a database when passed a valid characteristic.
         * Note that this test also checks FileIO.EditDatabaseAsync and
         * FileIO.QueryDatabaseAsync.
         */ 
        public static async Task<bool> DatabaseAddCharacteristicValid()
        {
            Database db = await Database.CreateDatabase("ChineseA");
            bool test = false;
            string characteristic = "new characteristic";

            await db.AddCharacteristicAsync(characteristic);
            Forest<string> allCharacteristics = await FileIO.GetCharacteristicsAsync("ChineseA");

            test = allCharacteristics != null && db.Characteristics != null && 
                   allCharacteristics.Search(characteristic) != null && db.Characteristics.Search(characteristic) != null;

            if (test)
            {
                await db.RemoveCharacteristicAsync(characteristic);
            }

            return test;
        }

        /* Tests that the AddCharacteristicAsync method does nothing 
         * when passed a null characteristic.
         */ 
        public static async Task<bool> DatabaseAddCharacteristicNull()
        {
            Database db = await Database.CreateDatabase("ChineseA");
            bool test = false;

            Forest<string> allCharacteristics = await FileIO.GetCharacteristicsAsync("ChineseA");

            if (allCharacteristics != null && db.Characteristics != null)
            {
                int dbPreCount = db.Characteristics.ToList().Count;
                int testdbPreCount = allCharacteristics.ToList().Count;

                await db.AddCharacteristicAsync(null);
                allCharacteristics = await FileIO.GetCharacteristicsAsync("ChineseA");

                test = (await DatabaseAddCharacteristicValid()) && allCharacteristics != null && 
                       db.Characteristics != null && dbPreCount == db.Characteristics.ToList().Count &&
                       testdbPreCount == allCharacteristics.ToList().Count;
            }

            return test;
        }

        /* Tests that the AddCharacteristicAsync method does not add
         * a duplicate characteristic to the database.
         * Note that this test also checks FileIO.QueryDatabaseAsync.
         */ 
        public static async Task<bool> DatabaseAddCharacteristicDuplicate()
        {
            Database db = await Database.CreateDatabase("ChineseA");
            bool test = false;

            Forest<string> allCharacteristics = await FileIO.GetCharacteristicsAsync("ChineseA");

            if (allCharacteristics != null && db.Characteristics != null && 
                db.Characteristics.Search("family") != null)
            {
                int dbPreCount = db.Characteristics.ToList().Count;
                int testdbPreCount = allCharacteristics.ToList().Count;

                await db.AddCharacteristicAsync("family");
                allCharacteristics = await FileIO.GetCharacteristicsAsync("ChineseA");

                test = (await DatabaseAddCharacteristicValid()) && allCharacteristics != null && 
                       db.Characteristics != null && dbPreCount == db.Characteristics.ToList().Count &&
                       testdbPreCount == allCharacteristics.ToList().Count;
            }

            return test;
        }

        /* Tests that the AddCharacteristicAsync method does not add
         * a characteristic that does not 100% match a directory, but
         * that contains a directory that already exists.
         * Note that this test also checks FileIO.QueryDatabaseAsync.
         */ 
        public static async Task<bool> DatabaseAddCharacteristicDuplicateDirectory()
        {
            Database db = await Database.CreateDatabase("ChineseA");
            bool test = false;

            Forest<string> allCharacteristics = await FileIO.GetCharacteristicsAsync("ChineseA");

            if (allCharacteristics != null && db.Characteristics != null &&
                db.Characteristics.Search("level01") != null && 
                db.Characteristics.ToListWithDirectories().Contains("/ic/level01/lesson01"))
            {
                int dbPreCount = db.Characteristics.ToList().Count;
                int testdbPreCount = allCharacteristics.ToList().Count;

                await db.AddCharacteristicAsync("level01");
                allCharacteristics = await FileIO.GetCharacteristicsAsync("ChineseA");

                test = (await DatabaseAddCharacteristicValid()) && allCharacteristics != null && 
                       db.Characteristics != null && dbPreCount == db.Characteristics.ToList().Count &&
                       testdbPreCount == allCharacteristics.ToList().Count;
            }

            return test;
        }

        /* Tests that the RemoveCharacteristicAsync method properly removes
         * a characteristic from a database when passed a valid characteristic.
         * Note that this test also checks FileIO.EditDatabaseAsync and
         * FileIO.QueryDatabaseAsync.
         */ 
        public static async Task<bool> DatabaseRemoveCharacteristicValid()
        {
            Database db = await Database.CreateDatabase("ChineseA");
            bool test = false;
            string characteristic = "new characteristic";

            if (await DatabaseAddCharacteristicValid())
            {
                await db.AddCharacteristicAsync(characteristic);
                await db.RemoveCharacteristicAsync(characteristic);

                Forest<string> allCharacteristics = await FileIO.GetCharacteristicsAsync("ChineseA");

                test = allCharacteristics != null && db.Characteristics != null &&
                       allCharacteristics.Search(characteristic) is null && db.Characteristics.Search(characteristic) is null;
            }

            return test;
        }

        /* Tests that the RemoveCharacteristicAsync method does nothing
         * when passed a null characteristic.
         */ 
        public static async Task<bool> DatabaseRemoveCharacteristicNull()
        {
            Database db = await Database.CreateDatabase("ChineseA");
            bool test = false;

            Forest<string> allCharacteristics = await FileIO.GetCharacteristicsAsync("ChineseA");

            if (allCharacteristics != null && db.Characteristics != null)
            {
                int testdbPreCount = allCharacteristics.ToList().Count;
                int dbPreCount = db.Characteristics.ToList().Count;

                await db.RemoveCharacteristicAsync(null);

                allCharacteristics = await FileIO.GetCharacteristicsAsync("ChineseA");

                test = (await DatabaseRemoveCharacteristicValid()) && testdbPreCount == allCharacteristics.ToList().Count &&
                       dbPreCount == db.Characteristics.ToList().Count;
            }

            return test;
        }

        /* Tests that the RemoveCharacteristicAsync method does nothing
         * when passed a characteristic that does not exist
         * in the database.
         * Note that this test also checks FileIO.QueryDatabaseAsync.
         */
        public static async Task<bool> DatabaseRemoveCharacteristicNonExistent()
        {
            Database db = await Database.CreateDatabase("ChineseA");
            bool test = false;

            Forest<string> allCharacteristics = await FileIO.GetCharacteristicsAsync("ChineseA");

            if (allCharacteristics != null && db.Characteristics != null && 
                db.Characteristics.Search("not a characteristic") is null)
            {
                int testdbPreCount = allCharacteristics.ToList().Count;
                int dbPreCount = db.Characteristics.ToList().Count;

                await db.RemoveCharacteristicAsync("not a characteristic");

                allCharacteristics = await FileIO.GetCharacteristicsAsync("ChineseA");

                test = (await DatabaseRemoveCharacteristicValid()) && testdbPreCount == allCharacteristics.ToList().Count &&
                       dbPreCount == db.Characteristics.ToList().Count;
            }

            return test;
        }

        /* Tests that the AddAlternativeSpellingTypeAsync method properly adds
         * an alternative spelling type to a database when passed a valid
         * alternative spelling type.
         * Note that this test also checks FileIO.EditDatabaseAsync and
         * FileIO.QueryDatabaseAsync.
         */
        public static async Task<bool> DatabaseAddAlternativeSpellingTypeValid()
        {
            Database db = await Database.CreateDatabase("ChineseA");
            bool test = false;
            string type = "new_type";

            await db.AddAlternativeSpellingTypeAsync(type);
            List<string> allTypes = await FileIO.GetAlternativeSpellingTypesAsync("ChineseA");

            test = allTypes != null && db.AlternativeSpellingTypes != null &&
                   allTypes.Contains(type) && db.AlternativeSpellingTypes.Contains(type);

            if (test)
            {
                await db.RemoveAlternativeSpellingTypeAsync(type);
            }

            return test;
        }

        /* Tests that the AddAlternativeSpellingTypeAsync method does nothing
         * when passed a null alternative spelling type.
         */ 
        public static async Task<bool> DatabaseAddAlternativeSpellingTypeNull()
        {
            Database db = await Database.CreateDatabase("ChineseA");
            bool test = false;

            List<string> allTypes = await FileIO.GetAlternativeSpellingTypesAsync("ChineseA");

            if (allTypes != null && db.AlternativeSpellingTypes != null)
            {
                int dbPreCount = db.AlternativeSpellingTypes.Count;
                int testdbPreCount = allTypes.Count;

                await db.AddAlternativeSpellingTypeAsync(null);
                allTypes = await FileIO.GetAlternativeSpellingTypesAsync("ChineseA");

                test = (await DatabaseAddAlternativeSpellingTypeValid()) && allTypes != null &&
                       db.AlternativeSpellingTypes != null && dbPreCount == db.AlternativeSpellingTypes.Count &&
                       testdbPreCount == allTypes.Count;
            }

            return test;
        }

        /* Tests that the AddAlternativeSpellingTypeAsync method does not
         * add a duplicate alternative spelling type to a database.
         * Note that this test also checks FileIO.QueryDatabaseAsync.
         */ 
        public static async Task<bool> DatabaseAddAlternativeSpellingTypeDuplicate()
        {
            Database db = await Database.CreateDatabase("ChineseA");
            bool test = false;

            List<string> allTypes = await FileIO.GetAlternativeSpellingTypesAsync("ChineseA");

            if (allTypes != null && db.AlternativeSpellingTypes != null &&
                db.AlternativeSpellingTypes.Contains("Pinyin"))
            {
                int dbPreCount = db.AlternativeSpellingTypes.Count;
                int testdbPreCount = allTypes.Count;

                await db.AddAlternativeSpellingTypeAsync("Pinyin");
                allTypes = await FileIO.GetAlternativeSpellingTypesAsync("ChineseA");

                test = (await DatabaseAddAlternativeSpellingTypeValid()) && allTypes != null &&
                       db.AlternativeSpellingTypes != null && dbPreCount == db.AlternativeSpellingTypes.Count &&
                       testdbPreCount == allTypes.Count;
            }

            return test;
        }

        /* Tests that the RemoveAlternativeSpellingTypeAsync method properly
         * removes an alternative spelling type from a database when passed
         * a valid type.
         * Note that this test also checks FileIO.EditDatabaseAsync and
         * FileIO.QueryDatabaseAsync.
         */ 
        public static async Task<bool> DatabaseRemoveAlternativeSpellingTypeValid()
        {
            Database db = await Database.CreateDatabase("ChineseA");
            bool test = false;
            string type = "new_type";

            if (await DatabaseAddAlternativeSpellingTypeValid())
            {
                await db.AddAlternativeSpellingTypeAsync(type);
                await db.RemoveAlternativeSpellingTypeAsync(type);

                List<string> allTypes = await FileIO.GetAlternativeSpellingTypesAsync("ChineseA");

                test = allTypes != null && db.AlternativeSpellingTypes != null &&
                       !allTypes.Contains(type) && !db.AlternativeSpellingTypes.Contains(type);
            }

            return test;
        }

        /* Tests that the RemoveAlternativeSpellingTypeAsync method does nothing
         * when passed a null alternative spelling type.
         */ 
        public static async Task<bool> DatabaseRemoveAlternativeSpellingTypeNull()
        {
            Database db = await Database.CreateDatabase("ChineseA");
            bool test = false;

            List<string> allTypes = await FileIO.GetAlternativeSpellingTypesAsync("ChineseA");

            if (allTypes != null && db.AlternativeSpellingTypes != null)
            {
                int testdbPreCount = allTypes.Count;
                int dbPreCount = db.AlternativeSpellingTypes.Count;

                await db.RemoveAlternativeSpellingTypeAsync(null);

                allTypes = await FileIO.GetAlternativeSpellingTypesAsync("ChineseA");

                test = (await DatabaseRemoveAlternativeSpellingTypeValid()) && testdbPreCount == allTypes.Count &&
                       dbPreCount == db.AlternativeSpellingTypes.Count;
            }

            return test;
        }

        /* Tests that the RemoveAlternativeSpellingTypeAsync method does nothing
         * when passed an alternative spelling type that does not exist
         * in the database.
         * Note that this test also checks FileIO.QueryDatabaseAsync.
         */ 
        public static async Task<bool> DatabaseRemoveAlternativeSpellingTypeNonExistent()
        {
            Database db = await Database.CreateDatabase("ChineseA");
            bool test = false;

            List<string> allTypes = await FileIO.GetAlternativeSpellingTypesAsync("ChineseA");

            if (allTypes != null && db.AlternativeSpellingTypes != null &&
                !db.AlternativeSpellingTypes.Contains("not_a_type"))
            {
                int testdbPreCount = allTypes.Count;
                int dbPreCount = db.AlternativeSpellingTypes.Count;

                await db.RemoveAlternativeSpellingTypeAsync("not_a_type");

                allTypes = await FileIO.GetAlternativeSpellingTypesAsync("ChineseA");

                test = (await DatabaseRemoveAlternativeSpellingTypeValid()) && testdbPreCount == allTypes.Count &&
                       dbPreCount == db.AlternativeSpellingTypes.Count;
            }

            return test;
        }
    }
}
