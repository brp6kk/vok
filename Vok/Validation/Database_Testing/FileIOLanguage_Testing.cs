﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vok.Src.Database;
using static Vok.Src.Common.Common;
using Vok.Src.Common;

namespace Vok.Validation.Database_Testing
{
    static class FileIOLanguage_Testing
    {
        /* Ensures that the ExistingLanguagesAsync method
         * properly returns all of the names of the languages
         * in the test database.
         */ 
        public static bool ExistingLanguagesValid()
        {
            List<string> languages = FileIO.ExistingLanguages();
            List<string> expected = new List<string>() { "ChineseA", "ChineseB", "ChineseC", "ChineseD" };
            bool test = false;

            if (languages != null)
            {
                test = languages.Count == expected.Count;

                foreach (string language in languages)
                {
                    test = test && expected.Contains(language);
                }
            }

            return test;
        }

        /* Ensures that the ExistingLanguageAsync method
         * properly returns an empty list if the language database
         * is empty.
         */ 
        public static bool ExistingLanguagesEmpty()
        {
            Common.connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=../../Validation/emptyDB.mdb;Jet OLEDB:Database Password=dr@gonwe11";
            List<string> languages = FileIO.ExistingLanguages();
            bool test = languages != null && languages.Count == 0;

            Common.connectionString = TEST_CONNECTION;

            return test;
        }

        /* Ensures that the ExistingLanguageAsync method
         * properly ignores a language that has a terms table
         * but is missing a characteristics table.
         */ 
        public static bool ExisingLanguagesMissingCharacteristics()
        {
            Common.connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=../../Validation/missingCharDB.mdb;Jet OLEDB:Database Password=dr@gonwe11";
            List<string> languages = FileIO.ExistingLanguages();
            List<string> expected = new List<string>() { "ChineseB", "ChineseC", "ChineseD" };
            bool test = false;

            if (languages != null)
            {
                test = languages.Count == expected.Count;

                foreach (string language in languages)
                {
                    test = test && expected.Contains(language);
                }
            }

            Common.connectionString = TEST_CONNECTION;

            return test;
        }

        /* Ensures that the ExistingLanguageAsync method
         * properly ignores a language that has a characteristics table
         * but is missing a terms table.
         */ 
        public static bool ExistingLanguageMissingTerms()
        {
            Common.connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=../../Validation/missingTermDB.mdb;Jet OLEDB:Database Password=dr@gonwe11";
            List<string> languages = FileIO.ExistingLanguages();
            List<string> expected = new List<string>() { "ChineseB", "ChineseC", "ChineseD" };
            bool test = false;

            if (languages != null)
            {
                test = languages.Count == expected.Count;

                foreach (string language in languages)
                {
                    test = test && expected.Contains(language);
                }
            }

            Common.connectionString = TEST_CONNECTION;

            return test;
        }

        /* Ensures that the NewLanguageAsync method
         * properly adds new tables to the database
         * when passed a valid language name.
         */ 
        public static async Task<bool> NewLanguageValid()
        {
            List<string> languages = FileIO.ExistingLanguages();
            bool test = languages != null && !languages.Contains("ChineseE");

            if (test)
            {
                await FileIO.NewLanguageAsync("ChineseE");
                languages = FileIO.ExistingLanguages();
                test = languages != null && languages.Contains("ChineseE");

                // Cleanup after test.
                await FileIO.EditDatabaseAsync("Drop table ChineseE_Terms");
                await FileIO.EditDatabaseAsync("Drop table ChineseE_Characteristics");
            }

            return test;
        }

        /* Ensures that the NewLanguageAsync method does nothing
         * when passed a null string.
         */ 
        public static async Task<bool> NewLanguageNull()
        {
            await FileIO.NewLanguageAsync(null);

            // Unable to check this programmatically,
            // but presumably it works if no exceptions are thrown.
            // Check testDB.mdb to be sure.
            return await NewLanguageValid();
        }

        /* Ensures that the NewLanguageAsync method does nothing
         * when passed a language that already exists in the database.
         */ 
        public static async Task<bool> NewLanguageDuplicate()
        {
            List<string> languages = FileIO.ExistingLanguages();
            bool test = languages != null && languages.Contains("ChineseD");

            if (test)
            {
                await FileIO.NewLanguageAsync("ChineseD");
                List<Word> words = await FileIO.QueryDatabaseAsync("Select * from ChineseD_Terms");
                test = test && await NewLanguageValid() && words.Count > 0;
            }

            return test;
        }

        /* Ensures that the NewLanguageAsync method creates new tables
         * when passed a language that is missing a terms/characteristics table
         * (and thus does not count as existing) but still has the other table.
         */ 
        public static async Task<bool> NewLanguageMissingValid()
        {
            await FileIO.EditDatabaseAsync("Drop table ChineseD_Characteristics");
            await FileIO.NewLanguageAsync("ChineseD");
            List<Word> words = await FileIO.QueryDatabaseAsync("Select * from ChineseD_Terms");
            bool test = words.Count == 0;

            if (test)
            {
                await FileIO.EditDatabaseAsync("Insert into ChineseD_Terms select * from ChineseC_Terms");
            }

            return test;
        }
    }
}
