﻿using Vok.Src.Database;
using static Vok.Src.Common.Common;

namespace Vok.Validation.Database_Testing
{
    /* Tests for the Vok.Src.Database.Word class.
     */
    static class Word_Testing
    {
        /* Sees whether or not CompareTo works with strings with non-Latin characters。
         */ 
        public static bool NonLatinCharactersTest()
        {
            bool test = "我".CompareTo("你") > 0;
            test = test && "språk".CompareTo("sprak") > 0;
            test = test && "我觉得你是很好的朋友".CompareTo("我觉得你是不好的朋友") > 0;
            test = test && "怡".CompareTo("以") > 0; // ??
            test = test && "д".CompareTo("ж") < 0;

            return test;
        }

        /* Ensures that the constructor properly creates a Word
         * when given valid parameters.
         * Also checks optional parameter usage.
         */ 
        public static bool WordConstructorTests()
        {
            Word[] words = { new Word("tea", PartOfSpeech.Noun, true, 1, 1),
                             new Word("cheese"),
                             new Word(term: "pretzel", part: PartOfSpeech.Noun, numTimesStudied: 2) };
            // Expected values of properties.
            string[] terms = { "tea", "cheese", "pretzel" };
            PartOfSpeech?[] parts = { PartOfSpeech.Noun, null, PartOfSpeech.Noun };
            bool[] specials = { true, false, false };
            int[] numTimesStudied = { 1, 0, 2 };
            int[] numTimesCorrect = { 1, 0, 0 };

            bool test = true;

            // Ensures constructor properly assigned properties.
            for (int index = 0; index < words.Length && test; index++)
            {
                if(!(words[index].Term.Equals(terms[index]) &&
                     words[index].Part == parts[index] &&
                     words[index].Special == specials[index] &&
                     words[index].NumTimesStudied == numTimesStudied[index] &&
                     words[index].NumTimesCorrect == numTimesCorrect[index]))
                {
                    test = false;
                }
            }

            return test;
        }

        /* Ensures that the AddAlternativeSpelling method properly
         * adds an alternative spelling to the Word.
         */ 
        public static bool WordAddAltSpellingValid()
        {
            Word word = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            word.AddAlternativeSpelling("type", "val");

            bool test = word.AlternativeSpellings != null && 
                        word.AlternativeSpellings.Count == 1 && word.AlternativeSpellings["type"].Equals("val");

            return test;
        }

        /* Ensures that the AddAlternativeSpelling method does not
         * add an alternative spelling when either type or val are null.
         */
        public static bool WordAddAltSpellingNull()
        {
            Word word = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            word.AddAlternativeSpelling(null, "val");
            word.AddAlternativeSpelling("type", null);
            word.AddAlternativeSpelling(null, null);

            bool test = WordAddAltSpellingValid() && word.AlternativeSpellings.Count == 0;

            return test;
        }

        /* Ensures that the AddAlternativeSpelling method replaces
         * the alternative spelling if the type already exists
         * in the Word.
         */
        public static bool WordAddAltSpellingDuplicate()
        {
            Word word = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            word.AddAlternativeSpelling("type", "val");
            word.AddAlternativeSpelling("type", "new");

            bool test = WordAddAltSpellingValid() && word.AlternativeSpellings.Count == 1 && 
                        word.AlternativeSpellings["type"].Equals("new");

            return test;
        }

        /* Ensures that the RemoveAlternativeSpelling method properly
         * removes an alternative spelling to the Word.
         */ 
        public static bool WordRemoveAltSpellingValid()
        {
            Word word = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            word.AddAlternativeSpelling("type", "val");
            bool test = word.AlternativeSpellings != null && word.AlternativeSpellings.Count == 1;

            word.RemoveAlternativeSpelling("type");
            test = test && word.AlternativeSpellings.Count == 0;

            return test;
        }

        /* Ensures that the RemoveAlternativeSpelling method does nothing
         * when type is null.
         */
        public static bool WordRemoveAltSpellingNull()
        {
            Word word = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            word.AddAlternativeSpelling("type", "val");
            bool test = word.AlternativeSpellings != null && word.AlternativeSpellings.Count == 1;

            word.RemoveAlternativeSpelling(null);
            test = WordRemoveAltSpellingValid() && test && word.AlternativeSpellings.Count == 1;

            return test;
        }

        /* Ensures that the RemoveAlternativeSpelling method does nothing
         * when asked to remove a non-existent type of alternative spelling.
         */
        public static bool WordRemoveAltSpellingNonExistent()
        {
            Word word = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            word.AddAlternativeSpelling("type", "val");
            bool test = word.AlternativeSpellings != null && word.AlternativeSpellings.Count == 1;

            word.RemoveAlternativeSpelling("not a type");
            test = WordRemoveAltSpellingValid() && test && word.AlternativeSpellings.Count == 1;

            return test;
        }

        /* Ensures that the AddDefinition method properly adds
         * a definition to the Word.
         */
        public static bool WordAddDefinitionValid()
        {
            Word word = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            word.AddDefinition("definition");
            bool test = word.Definitions != null && word.Definitions.Count == 1 && word.Definitions[0].Equals("definition");

            return test;
        }

        /* Ensures that the AddDefinition method does nothing
         * when passed a null parameter.
         */
        public static bool WordAddDefinitionNull()
        {
            Word word = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            word.AddDefinition(null);
            bool test = WordAddDefinitionValid() && word.Definitions.Count == 0;

            return test;
        }

        /* Ensures that the AddDefinition method does not add
         * a duplicate definition to a Word.
         */
        public static bool WordAddDefinitionDuplicate()
        {
            Word word = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            word.AddDefinition("definition");
            bool test = word.Definitions != null && word.Definitions.Count == 1;

            word.AddDefinition("definition");
            test = test && word.Definitions.Count == 1;

            return test;
        }

        /* Ensures that the AddDefinition method splits
         * a definition based on the definition delimiter
         * before adding to a Word.
         */
        public static bool WordAddDefinitionMultiple()
        {
            Word word = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            word.AddDefinition("definition1" + ENTRY_DELIM + "definition2");
            bool test = word.Definitions != null && word.Definitions.Count == 2 &&
                        word.Definitions[0].Equals("definition1") && word.Definitions[1].Equals("definition2");

            return test;
        }

        /* Ensures that the RemoveDefinition method properly
         * removes a definition from a Word.
         */
        public static bool WordRemoveDefinitionValid()
        {
            Word word = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            word.AddDefinition("definition");
            bool test = word.Definitions != null && word.Definitions.Count == 1;

            word.RemoveDefinition("definition");
            test = test && word.Definitions.Count == 0;

            return test;
        }

        /* Ensures that the RemoveDefinition method does nothing
         * when passed a null parameter.
         */
        public static bool WordRemoveDefinitionNull()
        {
            Word word = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            word.AddDefinition("definition");
            bool test = word.Definitions != null && word.Definitions.Count == 1;

            word.RemoveDefinition(null);
            test = WordRemoveDefinitionValid() && test && word.Definitions.Count == 1;

            return test;
        }

        /* Ensures that the RemoveDefinition method does nothing
         * when passed a definition not attached to the word.
         */
        public static bool WordRemoveDefinitionNonExistent()
        {
            Word word = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            word.AddDefinition("definition");
            bool test = word.Definitions != null && word.Definitions.Count == 1;

            word.RemoveDefinition("not a word");
            test = WordRemoveDefinitionValid() && test && word.Definitions.Count == 1;

            return test;
        }

        /* Ensures that the following related methods work as expected:
         *  - IncrementNumTimesStudied
         *  - IncrementNumTimesCorrect
         *  - PercentageCorrect
         *  - ResetScore
         */  
        public static bool WordNumTimesStudiedCorrectTests()
        {
            Word word = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            double initialPercentage = word.PercentageCorrect();
            word.IncrementNumTimesStudied();
            word.IncrementNumTimesCorrect();
            word.IncrementNumTimesStudied();
            bool test = initialPercentage == 1.0 && word.NumTimesStudied == 3 && 
                        word.NumTimesCorrect == 2 && word.PercentageCorrect() == ((double)2 / 3);

            word.ResetScoreStats();
            test = test && word.NumTimesStudied == 0 && word.NumTimesCorrect == 0;

            return test;
        }

        /* Ensures that the AddCharacteristic method properly
         * adds a characteristic to a word.
         */ 
        public static bool WordAddCharacteristicValid()
        {
            Word word = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            word.AddCharacteristic("characteristic");
            bool test = word.Characteristics != null && word.Characteristics.Count == 1 && word.Characteristics[0].Equals("characteristic");

            return test;
        }

        /* Ensures that the AddCharacteristic method does nothing
         * when passed a null parameter.
         */
        public static bool WordAddCharacteristicNull()
        {
            Word word = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            word.AddCharacteristic("characteristic");
            bool test = word.Characteristics != null && word.Characteristics.Count == 1;

            word.AddCharacteristic(null);
            test = test && word.Characteristics.Count == 1;

            return test;
        }

        /* Ensures that the AddCharacteristic method does nothing
         * when passed a characteristic already attached to the Word.
         */
        public static bool WordAddCharacteristicDuplicate()
        {
            Word word = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            word.AddCharacteristic("characteristic");
            bool test = word.Characteristics != null && word.Characteristics.Count == 1;

            word.AddCharacteristic("characteristic");
            test = test && word.Characteristics.Count == 1;

            return test;
        }

        /* Ensures that the ContainsCharacteristic method properly
         * returns true when a characterisic is attached to a Word.
         */
        public static bool WordContainsCharacteristicTrue()
        {
            Word word = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            word.AddCharacteristic("characteristic");
            bool test = word.ContainsCharacteristic("characteristic");

            return test;
        }

        /* Ensures that the ContainsCharacteristic method properly
         * returns false when a character is not attached to a Word.
         */
        public static bool WordContainsCharacterisicFalse()
        {
            Word word = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            word.AddCharacteristic("characteristic");
            bool test = !word.ContainsCharacteristic("not characteristic");

            return WordContainsCharacteristicTrue() && test;
        }

        /* Ensures that the ContainsCharacteristic method returns false
         * when passed null parameter.
         */
        public static bool WordContainsCharacteristicNull()
        {
            Word word = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            word.AddCharacteristic("characteristic");
            bool test = !word.ContainsCharacteristic(null);

            return WordContainsCharacteristicTrue() && test;
        }

        /* Ensures that the RemoveCharacteristic method properly
         * removes a characteristic from a word.
         */
        public static bool WordRemoveCharacteristicValid()
        {
            Word word = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            word.AddCharacteristic("characteristic");
            bool test = word.Characteristics != null && word.Characteristics.Count == 1;

            word.RemoveCharacteristic("characteristic");
            test = test && word.Characteristics.Count == 0;

            return test;
        }

        /* Ensures that the RemoveCharacteristic method does nothing
         * when passed a null parameter.
         */
        public static bool WordRemoveCharacteristicNull()
        {
            Word word = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            word.AddCharacteristic("characteristic");
            bool test = word.Characteristics != null && word.Characteristics.Count == 1;

            word.RemoveCharacteristic(null);
            test = WordRemoveCharacteristicValid() && test && word.Characteristics.Count == 1;

            return test;
        }

        /* Ensures that the RemoveCharacteristic method does nothing
         * when passed a characteristic not attached to the Word.
         */
        public static bool WordRemoveCharacteristicNonExistent()
        {
            Word word = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            word.AddCharacteristic("characteristic");
            bool test = word.Characteristics != null && word.Characteristics.Count == 1;

            word.RemoveCharacteristic("not a characteristic");
            test = WordRemoveCharacteristicValid() && test && word.Characteristics.Count == 1;

            return test;
        }

        /* Ensures that two words with identical terms are considered equal.
         */ 
        public static bool WordEqual()
        {
            Word word1 = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            Word word2 = new Word("word", PartOfSpeech.Noun, false, 1, 0);
            bool test = word1 == word2;

            return test;
        }

        /* Ensures that two words without identical terms are not considered equal.
         */
        public static bool WordsNotEqual()
        {
            Word word1 = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            Word word2 = new Word("not a word", PartOfSpeech.Noun, true, 1, 1);
            bool test = word1 != word2;

            return test;
        }

        /* Ensures that two words are properly compared with comparison operators.
         */ 
        public static bool WordInequalityTests()
        {
            Word word1 = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            Word word2 = new Word("word", PartOfSpeech.Noun, false, 1, 0);
            Word word3 = new Word("not a word", PartOfSpeech.Noun, true, 1, 1);
            Word word4 = new Word("maybe a word", PartOfSpeech.Noun, true, 1, 1);
            bool test = word4 < word3 && word1 > word3 &&
                        word1 <= word2 && word4 <= word2 &&
                        word1 >= word2 && word1 >= word3;

            return test;
        }

        /* Ensures that CompareTo properly compares two words based on terms.
         */ 
        public static bool WordCompareToTest()
        {
            Word word1 = new Word("word", PartOfSpeech.Noun, true, 1, 1);
            Word word2 = new Word("word", PartOfSpeech.Noun, false, 1, 0);
            Word word3 = new Word("not a word", PartOfSpeech.Noun, true, 1, 1);
            Word word4 = new Word("maybe a word", PartOfSpeech.Noun, true, 1, 1);
            bool test = word4.CompareTo(word3) < 0 && word1.CompareTo(word3) > 0 &&
                        word1.CompareTo(word2) == 0;

            return test;
        }
    }
}
