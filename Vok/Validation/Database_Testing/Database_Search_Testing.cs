﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vok.Src.Database;
using static Vok.Src.Common.Common;

namespace Vok.Validation.Database_Testing
{
    static class Database_Search_Testing
    {
        /* Ensures that running Search on a Database returns
         * the proper list of Words.
         */ 
        public static async Task<bool> SearchValid()
        {
            SearchCommand[] cmds = { SearchCommand.CreateSearchCommand("Part_of_Speech", "=", "Verb") };
            List<Word> results = await currentDB.SearchAsync(cmds);

            bool test = results != null && results.Count == 5;

            if (test)
            {
                foreach (Word res in results)
                {
                    test = test && res.Part == PartOfSpeech.Verb;
                }
            }

            return test;
        }

        /* Ensures that running Search on a Database with
         * SearchCommands that cause all values in the Database
         * to be returned will return all Words.
         */ 
        public static async Task<bool> SearchAll()
        {
            SearchCommand[] cmds = { SearchCommand.CreateSearchCommand("Number_Times_Studied", ">=", "0") };
            List<Word> results = await currentDB.SearchAsync(cmds);

            bool test = results != null && results.Count == 34;

            return test;
        }

        /* Ensures that running Search on a Database with
         * SearchCommands that conflict causes an empty list
         * to be returned.
         */ 
        public static async Task<bool> SearchConflictingCommands()
        {
            SearchCommand[] cmds = { SearchCommand.CreateSearchCommand("Part_of_Speech", "=", "Noun"),
                                     SearchCommand.CreateSearchCommand("Part_of_Speech", "=", "Verb")};
            List<Word> results = await currentDB.SearchAsync(cmds);

            bool test = results != null && results.Count == 0;

            return test;
        }

        /* Ensures that running Search on a Database where
         * SearchCommands contain an alternative spelling type
         * properly returns words.
         */ 
        public static async Task<bool> SearchAlternativeSpellingType()
        {
            SearchCommand[] cmds = { SearchCommand.CreateSearchCommand("Pinyin", ">", "wen4") };
            List<Word> results = await currentDB.SearchAsync(cmds);

            bool test = results != null && results.Count == 8;

            return test;
        }

        /* Ensures that running Search on a Database where
         * the container of SearchCommands is either null or
         * a SearchCommand within the container is null
         * returns null.
         */ 
        public static async Task<bool> SearchNull()
        {
            SearchCommand[] cmds = { null };
            List<Word> results = await currentDB.SearchAsync(cmds);
            bool test = await SearchValid() && results is null;

            results = await currentDB.SearchAsync(null);
            test = test && results is null;

            return test;
        }
        
        /* Ensures that running Search on a Database where
         * at least one of the SearchCommands is illegal
         * returns null.
         */ 
        public static async Task<bool> SearchInvalid()
        {
            SearchCommand[] cmds = { SearchCommand.CreateSearchCommand("Part_of_Speech", ">", "4") };
            List<Word> results = await currentDB.SearchAsync(cmds);
            bool test = await SearchValid() && results is null;

            return test;
        }
    }
}
