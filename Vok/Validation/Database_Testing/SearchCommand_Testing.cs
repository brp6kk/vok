﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vok.Src.Database;

namespace Vok.Validation.Database_Testing
{
    static class SearchCommand_Testing
    {
        /* Ensures that the CreateSearchCommand function properly
         * returns a SearchCommand when passed valid parameters.
         * Tests the version of this method that takes three parameters.
         */ 
        public static bool CreateSearchCommandValid()
        {
            SearchCommand cmd = SearchCommand.CreateSearchCommand("Term", "=", "value");

            bool test = cmd != null && cmd.FieldName.Equals("Term") && cmd.ComparisonType.Equals("=") && 
                        cmd.SearchValue.Equals("value") && cmd.SearchFunction is null;

            return test;
        }

        /* Ensures that the CreateSearchCommand function properly
         * returns a SearchCommand when passed valid parameters.
         * Tests the version of this method that takes four parameters.
         */ 
        public static bool CreateSearchCommandValidWithFunction()
        {
            SearchCommand cmd = SearchCommand.CreateSearchCommand("Len", "Term", "=", "2");

            bool test = cmd != null && cmd.FieldName.Equals("Term") && cmd.ComparisonType.Equals("=") &&
                        cmd.SearchValue.Equals("2") && cmd.SearchFunction.Equals("Len");

            return test;
        }

        /* Ensures that the CreateSearchCommand function properly
         * returns a SearchCommand when passed a valid 
         * alternative spelling type as the field name.
         */ 
        public static bool CreateSearchCommandValidAlternativeSpellingType()
        {
            SearchCommand cmd = SearchCommand.CreateSearchCommand("Pinyin", "=", "value");

            bool test = cmd != null && cmd.FieldName.Equals("Pinyin") && cmd.ComparisonType.Equals("=") &&
                        cmd.SearchValue.Equals("value") && cmd.SearchFunction is null;

            return test;
        }

        /* Ensures that the CreateSearchCommand function properly returns null
         * when any of the parameters are null (sans searchFunction).
         */ 
        public static bool CreateSearchCommandNull()
        {
            SearchCommand[] cmds = { SearchCommand.CreateSearchCommand(null, "<>", "value"),
                                     SearchCommand.CreateSearchCommand("Term", null, "value"),
                                     SearchCommand.CreateSearchCommand("Term", "<>", null) };

            bool test = CreateSearchCommandValid();

            foreach (SearchCommand cmd in cmds)
            {
                test = test && cmd is null;
            }

            return test;
        }

        /* Ensures that the CreateSearchCommand function properly returns null
         * when an illegal field name is passed as a parameter.
         */ 
        public static bool CreateSearchCommandIllegalFieldName()
        {
            SearchCommand cmd = SearchCommand.CreateSearchCommand("Bad", "=", "value");

            bool test = CreateSearchCommandValid() && cmd is null;

            return test;
        }

        /* Ensures that the CreateSearchCommand function properly returns null
         * when an illegal comparison type is passed as a parameter.
         */ 
        public static bool CreateSearchCommandIllegalComparison()
        {
            SearchCommand cmd = SearchCommand.CreateSearchCommand("Term", "*", "value");

            bool test = CreateSearchCommandValid() && cmd is null;

            return test;
        }

        /* Ensures that the CreateSearchCommand function properly returns null
         * when an illegal function is passed as a parameter.
         */ 
        public static bool CreateSearchCommandIllegalFunction()
        {
            SearchCommand cmd = SearchCommand.CreateSearchCommand("Bad", "Term", "=", "value");

            bool test = CreateSearchCommandValid() && cmd is null;

            return test;
        }

        /* Ensures that the CreateSearchCommand function properly return null
         * when the passed comparison type cannot be applied to the passed field name.
         */ 
        public static bool CreateSearchCommandInvalidComparisonForFieldName()
        {
            SearchCommand[] cmds = { SearchCommand.CreateSearchCommand("Number_Times_Studied", "~", "5"),
                                     SearchCommand.CreateSearchCommand("Special", "~", "true"),
                                     SearchCommand.CreateSearchCommand("Special", ">", "false"),
                                     SearchCommand.CreateSearchCommand("Definitions", "<", "value") };

            bool test = CreateSearchCommandValid();

            foreach (SearchCommand cmd in cmds)
            {
                test = test && cmd is null;
            }

            return test;
        }

        /* Ensures that the CreateSearchCommand function properly returns null
         * when the passed search value is a type incompatible with the passed field name.
         */ 
        public static bool CreateSearchCommandInvalidSearchValueForFieldName()
        {
            SearchCommand[] cmds = { SearchCommand.CreateSearchCommand("Part_of_Speech", "=", "not a part"),
                                     SearchCommand.CreateSearchCommand("Special", "=", "maybe"),
                                     SearchCommand.CreateSearchCommand("Number_Times_Studied", "=", "not a number"),
                                     SearchCommand.CreateSearchCommand("Number_Times_Correct", "=", "not a number") };

            bool test = CreateSearchCommandValid();

            foreach (SearchCommand cmd in cmds)
            {
                test = test && cmd is null;
            }

            return test;
        }

        /* Ensures that the CreateSearchCommand function properly returns null
         * when the passed search value is a type incompatible with the passed function.
         */ 
        public static bool CreateSearchCommandInvalidSearchValueForFunction()
        {
            SearchCommand[] cmds = { SearchCommand.CreateSearchCommand("Len", "Term", "=", "not a number"),
                                     SearchCommand.CreateSearchCommand("Count", "Definitions", "=", "not a number"),
                                     SearchCommand.CreateSearchCommand("Highest", "Number_Times_Studied", "=", "not a number"),
                                     SearchCommand.CreateSearchCommand("Lowest", "Number_Times_Correct", "=", "not a number") };

            bool test = CreateSearchCommandValid();

            foreach (SearchCommand cmd in cmds)
            {
                test = test && cmd is null;
            }

            return test;
        }

        /* Ensures that the CreateSearchCommand function properly returns null
         * when the passed function is a type incompatible with the passed field name.
         */ 
        public static bool CreateSearchCommandInvalidFunctionForFieldName()
        {
            SearchCommand[] cmds = { SearchCommand.CreateSearchCommand("Len", "Number_Times_Studied", "=", "5"),
                                     SearchCommand.CreateSearchCommand("Count", "Term", "=", "5"),
                                     SearchCommand.CreateSearchCommand("Highest", "Special", "=", "5"),
                                     SearchCommand.CreateSearchCommand("Lowest", "Definitions", "=", "5") };

            bool test = CreateSearchCommandValid();

            foreach (SearchCommand cmd in cmds)
            {
                test = test && cmd is null;
            }

            return test;
        }

        /* Ensures that the ToSQLWhere function returns a properly formatted string
         * based on a SearchCommand that has a null function.
         */ 
        public static bool ToSQLWhereValid()
        {
            SearchCommand cmd = SearchCommand.CreateSearchCommand("Special", "<>", "false");
            string expected = "Special<>false";

            bool test = cmd != null && expected.Equals(cmd.ToSQLWhere());

            return test;
        }

        /* Ensures that the ToSQLWhere function returns a properly formatted string
         * based on a SearchCommand with a function.
         */ 
        public static bool ToSQLWhereFunctionsValid()
        {
            SearchCommand[] cmds = { SearchCommand.CreateSearchCommand("Len", "Term", "=", "5"),
                                     SearchCommand.CreateSearchCommand("Count", "Definitions", "=", "5"),
                                     SearchCommand.CreateSearchCommand("Count", "Definitions", "=", "0") };
            string[] expected = { "Len(Term)=5", "(Len(Definitions) - Len(Replace(Definitions, ';', '')))=4",
                                  "Definitions=''" };

            bool test = true;

            for (int index = 0; index < cmds.Length; index++)
            {
                test = test && cmds[index] != null && expected[index].Equals(cmds[index].ToSQLWhere());
            }

            return test;
        }

        /* Ensures that the ToSQLWhere function returns a null string
         * if the SearchFunction of the SearchCommand is either "Highest" or "Lowest".
         */ 
        public static bool ToSQLWhereFunctionInvalid()
        {
            SearchCommand[] cmds = { SearchCommand.CreateSearchCommand("Highest", "Number_Times_Studied", "=", "5"),
                                     SearchCommand.CreateSearchCommand("Lowest", "Number_Times_Studied", "=", "5") };

            bool test = ToSQLWhereValid();

            foreach (SearchCommand cmd in cmds)
            {
                test = test && cmd.ToSQLWhere() is null;
            }

            return test;
        }

        /* Ensures that the ToSQLWhere function returns a properly formatted string
         * based on a SearchCommand that has a null function and a string search value.
         */
        public static bool ToSQLWhereStringSearchValueValid()
        {
            SearchCommand cmd = SearchCommand.CreateSearchCommand("Term", "=", "value");
            string expected = "Term='value'";

            bool test = cmd != null && expected.Equals(cmd.ToSQLWhere());

            return test;
        }

        /* Ensures that the ToSQLWhere function returns a properly formatted string
         * based on a SearchCommand where comparison type == ~ (contains).
         */
        public static bool ToSQLWhereContainsValid()
        {
            SearchCommand cmd = SearchCommand.CreateSearchCommand("Term", "~", "value");
            string expected = "Term like '%value%'";

            bool test = cmd != null && expected.Equals(cmd.ToSQLWhere());

            return test;
        }

        /* Ensures that the ToSQLWhere function returns a properly formatted string
         * based on a SearchCommand where field name == Percentage_Correct.
         */ 
        public static bool ToSQLWherePercentageCorrectValid()
        {
            SearchCommand cmd = SearchCommand.CreateSearchCommand("Percentage_Correct", "=", "0.5");
            string expected = "(Iif(Number_Times_Studied=0, 0, Number_Times_Correct/Number_Times_Studied))=0.5";

            bool test = cmd != null && expected.Equals(cmd.ToSQLWhere());

            return test;
        }

        /* Ensures that the ToSQL function returns a properly formatted SQL statement
         * based on a collection of SearchCommands.
         */ 
        public static bool ToSQLValid()
        {
            SearchCommand[] cmds = { SearchCommand.CreateSearchCommand("Special", "=", "true"),
                                     SearchCommand.CreateSearchCommand("Part_of_Speech", "=", "Verb"),
                                     SearchCommand.CreateSearchCommand("Len", "Term", ">", "3") };
            string expected = "Select * from ChineseA_Terms where Special=true and Part_of_Speech='Verb' and Len(Term)>3";
            string actual = SearchCommand.ToSQL(cmds);

            bool test = actual != null && actual.Equals(expected);

            return test;
        }

        /* Ensures that the ToSQL function returns a properly formatted SQL statement
         * based on a collection of SearchCommands which includes one with the Highest function.
         */ 
        public static bool ToSQLHighestValid()
        {
            SearchCommand[] cmds = { SearchCommand.CreateSearchCommand("Special", "=", "true"),
                                     SearchCommand.CreateSearchCommand("Part_of_Speech", "=", "Verb"),
                                     SearchCommand.CreateSearchCommand("Highest", "Number_Times_Studied", "=", "3") };
            string expected = "Select top 3 * from ChineseA_Terms where Special=true and Part_of_Speech='Verb' order by Number_Times_Studied desc";
            string actual = SearchCommand.ToSQL(cmds);

            bool test = actual != null && actual.Equals(expected);

            return test;
        }

        /* Ensures that the ToSQL function returns a properly formatted SQL statement
         * based on a collection of SearchCommands which includes one with the Lowest function.
         */ 
        public static bool ToSQLLowestValid()
        {
            SearchCommand[] cmds = { SearchCommand.CreateSearchCommand("Special", "=", "true"),
                                     SearchCommand.CreateSearchCommand("Part_of_Speech", "=", "Verb"),
                                     SearchCommand.CreateSearchCommand("Lowest", "Number_Times_Studied", "=", "3") };
            string expected = "Select top 3 * from ChineseA_Terms where Special=true and Part_of_Speech='Verb' order by Number_Times_Studied";
            string actual = SearchCommand.ToSQL(cmds);

            bool test = actual != null && actual.Equals(expected);

            return test;
        }

        /* Ensures that the ToSQL function returns a properly formatted SQL statement
         * based on a collection of SearchCommands which includes multiple Highest/Lowest function.
         */
        public static bool ToSQLMultipleHighestLowest()
        {
            SearchCommand[] cmds = { SearchCommand.CreateSearchCommand("Special", "=", "true"),
                                     SearchCommand.CreateSearchCommand("Lowest", "Number_Times_Correct", "=", "3"),
                                     SearchCommand.CreateSearchCommand("Part_of_Speech", "=", "Verb"),
                                     SearchCommand.CreateSearchCommand("Highest", "Number_Times_Studied", "=", "3") };
            string expected = "Select top 3 * from ChineseA_Terms where Special=true and Part_of_Speech='Verb' order by Number_Times_Correct";
            string actual = SearchCommand.ToSQL(cmds);

            bool test = actual != null && actual.Equals(expected);

            return test;
        }

        /* Ensures that the ToSQL function returns a properly formatted SQL statement
         * based on a collection of SearchCommands which does not include any components
         * that would be part of the where clause.
         */ 
        public static bool ToSQLEmptyList()
        {
            SearchCommand[][] cmds = { new SearchCommand[1], new SearchCommand[0] };
            cmds[0][0] = SearchCommand.CreateSearchCommand("Highest", "Number_Times_Studied", "=", "3");
            string[] expected = { "Select top 3 * from ChineseA_Terms order by Number_Times_Studied desc",
                                  "Select * from ChineseA_Terms"};
            bool test = ToSQLValid();

            for (int index = 0; index < cmds.Length; index++)
            {
                string actual = SearchCommand.ToSQL(cmds[index]);
                test = test && expected[index].Equals(actual);
            }

            return test;
        }

        /* Ensures that the ToSQL function returns null when either the collection is null
         * or an element of the collection is null.
         */ 
        public static bool ToSQLNull()
        {
            SearchCommand[] cmds = { SearchCommand.CreateSearchCommand("Special", "=", "true"), null };

            bool test = ToSQLValid() && SearchCommand.ToSQL(null) is null && SearchCommand.ToSQL(cmds) is null;

            return test;
        }

        /* Ensures that the ToSQL function returns a properly formatted SQL statement
         * based on a collection of SearchCommands which includes one with the Random function.
         */ 
        public static bool ToSQLRandom()
        {
            SearchCommand[] cmds = { SearchCommand.CreateSearchCommand("Special", "=", "true"),
                                     SearchCommand.CreateSearchCommand("Part_of_Speech", "=", "Verb"),
                                     SearchCommand.CreateSearchCommand("Random", "Number_Times_Studied", "=", "3") };
            string expected = "Select top 3 * from ChineseA_Terms where Special=true and Part_of_Speech='Verb' order by rnd(int(now*ID)-now*ID)";
            string actual = SearchCommand.ToSQL(cmds);

            bool test = actual != null && actual.Equals(expected);

            return test;
        }


    }
}
