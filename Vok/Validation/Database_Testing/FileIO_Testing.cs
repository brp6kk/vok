﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vok.Src.Database;
using static Vok.Src.Common.Common;
using Vok.Src.Common;

namespace Vok.Validation.Database_Testing
{
    static class FileIO_Testing
    {
        /* Tests that the QueryDatabaseAsync method runs the SQL query and returns
         * the requested list of words.
         * Note that this test also checks FileIO.RowToWord.
         */
        public static async Task<bool> QueryDatabaseValid()
        {
            string sql = "Select * from ChineseA_Terms where Part_of_Speech='Adjective'";
            List<Word> words = await FileIO.QueryDatabaseAsync(sql);

            bool test = words != null && words.Count == 4 &&
                        words[0].Term.Equals("好") && words[1].Term.Equals("贵") &&
                        words[2].Term.Equals("女") && words[3].Term.Equals("男");

            return test;
        }

        /* Tests that the QueryDatabaseAsync method returns null when passed 
         * a string that is not proper sql.
         */
        public static async Task<bool> QueryDatabaseSQLInvalid()
        {
            string sql = "not sql";
            List<Word> words = await FileIO.QueryDatabaseAsync(sql);

            bool test = (await QueryDatabaseValid()) && words is null;

            return test;
        }

        /* Tests that the QueryDatabaseAsync method returns null when passed
         * non-existent field names.
         */
        public static async Task<bool> QueryDatabaseSQLNonApplicable()
        {
            string sql = "Select * from ChineseA_Terms where Field='thing'";
            List<Word> words = await FileIO.QueryDatabaseAsync(sql);

            bool test = (await QueryDatabaseValid()) && words is null;

            return test;
        }

        /* Tests that the QueryDatabaseAsync method returns an empty list
         * when passed a legal query that returns no words.
         */
        public static async Task<bool> QueryDatabaseSQLNoResults()
        {
            string sql = "Select * from ChineseA_Terms where Number_Times_Studied=3";
            List<Word> words = await FileIO.QueryDatabaseAsync(sql);

            bool test = words != null && words.Count == 0;

            return test;
        }

        /* Tests that the EditDatabaseAsync method runs the SQL command.
         * Note that this test is checked using the QueryDatabaseAsync method.
         */
        public static async Task<bool> EditDatabaseValid()
        {
            string sql = "Insert into ChineseA_Terms (Term, Definitions, Part_of_Speech, Characteristics, Pinyin)" +
                         "Values ('妹妹', 'younger sister', 'Noun', 'ic/level01/lesson02;family', 'mei4mei')";
            await FileIO.EditDatabaseAsync(sql);

            sql = "Select * from ChineseA_Terms where Term='妹妹'";
            List<Word> words = await FileIO.QueryDatabaseAsync(sql);
            bool test = words != null && words.Count == 1 && words[0].Term.Equals("妹妹");

            // Adding term to table was successful; now try removing term from table.
            if (test)
            {
                sql = "Delete from ChineseA_Terms where Term='妹妹'";
                await FileIO.EditDatabaseAsync(sql);

                sql = "Select * from ChineseA_Terms where Term='妹妹'";
                words = await FileIO.QueryDatabaseAsync(sql);
                test = words != null && words.Count == 0;
            }

            return test;
        }

        /* Tests that the EditDatabaseAsync method does nothing when passed
         * a string that is not proper SQL.
         */
        public static async Task<bool> EditDatabaseSQLInvalid()
        {
            string sql = "not sql";
            await FileIO.EditDatabaseAsync(sql);

            // Unable to check this programmatically,
            // but presumably it works if no exceptions are thrown.
            return (await EditDatabaseValid());
        }

        /* Tests that the EditDatabaseAsync method does nothing when passed
         * a legal SQL string that attempts to do illegal edits.
         * Note that this test is checked using the QueryDatabaseAsync method.
         */
        public static async Task<bool> EditDatabaseSQLNonApplicable()
        {
            // Field name Zhuyin does not exist, making this command invalid.
            string sql = "Insert into ChineseA_Terms (Term, Definitions, Part_of_Speech, Characteristics, Zhuyin)" +
                         "Values ('妹妹', 'younger sister', 'Noun', 'ic/level01/lesson02;family', 'mei4mei')";
            await FileIO.EditDatabaseAsync(sql);

            sql = "Select * from ChineseA_Terms where Term='妹妹'";
            List<Word> words = await FileIO.QueryDatabaseAsync(sql);
            bool test = words != null && words.Count == 0;

            return test;
        }

        /* Tests that the BackupDatabaseAsync method creates new tables in the database
         * representing the current state of the language database.
         * Note that this test is checked using the QueryDatabaseAsync and
         * GetCharacteristicsAsync methods.
         */
        public static async Task<bool> BackupDatabaseValid()
        {
            await FileIO.BackupDatabaseAsync("ChineseA");

            string sql = "Select * from ChineseA_Backup_Terms";
            List<Word> words = await FileIO.QueryDatabaseAsync(sql);
            bool test = words != null && words.Count == 34;

            Forest<string> characteristics = await FileIO.GetCharacteristicsAsync("ChineseA_Backup");
            test = test && characteristics != null && characteristics.Trees.Count == 3;

            // Removes backup database if successful backup.
            if (test)
            {
                sql = "Drop table ChineseA_Backup_Terms";
                await FileIO.EditDatabaseAsync(sql);
                sql = "Drop table ChineseA_Backup_Characteristics";
                await FileIO.EditDatabaseAsync(sql);
            }

            return test;
        }

        /* Tests that the BackupDatabaseAsync method does nothing when
         * passed a null string.
         */
        public static async Task<bool> BackupDatabaseNull()
        {
            await FileIO.BackupDatabaseAsync(null);

            // Unable to check this programmatically,
            // but presumably it works if no exceptions are thrown.
            // Check testDB.mdb to be sure.
            return (await BackupDatabaseValid());
        }

        /* Tests that the BackupDatabaseAsync method does nothing when
         * passed a nonexistent language.
         */
        public static async Task<bool> BackupDatabaseNonExistentLanguage()
        {
            await FileIO.BackupDatabaseAsync("ChineseE");

            // Unable to check this programmatically,
            // but presumably it works if no exceptions are thrown.
            // Check testDB.mdb to be sure.
            return (await BackupDatabaseValid());
        }

        /* Tests that the BackupDatabaseAsync method writes over 
         * a backup database if one already exists.
         * Note that this test is checked using the QueryDatabaseAsync method.
         */
        public static async Task<bool> BackupDatabaseAlreadyExists()
        {
            await FileIO.BackupDatabaseAsync("ChineseC");

            string sql = "Select * from ChineseC_Backup_Terms";
            List<Word> words = await FileIO.QueryDatabaseAsync(sql);
            bool test = words != null && words.Count == 34;

            // Continues testing if successful backup.
            if (test)
            {
                sql = "Insert into ChineseC_Terms (Term, Definitions, Part_of_Speech, Characteristics)" +
                      "Values ('妹妹', 'younger sister', 'Noun', 'ic/level01/lesson02;family')";
                await FileIO.EditDatabaseAsync(sql);

                // Try additional backup.
                await FileIO.BackupDatabaseAsync("ChineseC");
                sql = "Select * from ChineseC_Backup_Terms";
                words = await FileIO.QueryDatabaseAsync(sql);
                test = words != null && words.Count == 35;

                // Cleanup.
                sql = "Delete from ChineseC_Terms where Term='妹妹'";
                await FileIO.EditDatabaseAsync(sql);
                sql = "Drop table ChineseC_Backup_Terms";
                await FileIO.EditDatabaseAsync(sql);
                sql = "Drop table ChineseC_Backup_Characteristics";
                await FileIO.EditDatabaseAsync(sql);
            }

            return test;
        }

        /* Tests that the ResetDatabaseAsync method changes a language's tables
         * to the state of the backup database.
         * Note that this test is checked using the QueryDatabaseAsync and
         * GetCharacteristicsAsync methods.
         */
        public static async Task<bool> ResetDatabaseValid()
        {
            await FileIO.BackupDatabaseAsync("ChineseA");

            // Adds to database.
            string sql = "Insert into ChineseA_Terms (Term, Definitions, Part_of_Speech, Characteristics)" +
                         "Values ('妹妹', 'younger sister', 'Noun', 'ic/level01/lesson02;family')";
            await FileIO.EditDatabaseAsync(sql);
            sql = "Select * from ChineseA_Terms";
            List<Word> words = await FileIO.QueryDatabaseAsync(sql);
            int preCount = words.Count;

            await FileIO.ResetDatabaseAsync("ChineseA");
            words = await FileIO.QueryDatabaseAsync(sql);
            int resetCount = words.Count;
            
            sql = "Select * from ChineseA_Terms where Term='妹妹'";
            words = await FileIO.QueryDatabaseAsync(sql);
            // After reseting database, the added word should be gone.
            bool test = preCount > resetCount && words != null && words.Count == 0;

            // Cleanup.
            if (!test)
            {
                sql = "Delete from ChineseA_Terms where Term='妹妹'";
                await FileIO.EditDatabaseAsync(sql);
            }
            sql = "Drop table ChineseA_Backup_Terms";
            await FileIO.EditDatabaseAsync(sql);
            sql = "Drop table ChineseA_Backup_Characteristics";
            await FileIO.EditDatabaseAsync(sql);

            return test;
        }

        /* Tests that the ResetDatabaseAsync method does nothing when
         * passed a null string.
         */
        public static async Task<bool> ResetDatabaseNull()
        {
            await FileIO.ResetDatabaseAsync(null);

            // Unable to check this programmatically,
            // but presumably it works if no exceptions are thrown.
            // Check testDB.mdb to be sure.
            return (await ResetDatabaseValid());
        }

        /* Tests that the ResetDatabaseAsync method does nothing when
         * passed a nonexistent language.
         */
        public static async Task<bool> ResetDatabaseNonExistentLanguage()
        {
            await FileIO.ResetDatabaseAsync("ChineseE");

            // Unable to check this programmatically,
            // but presumably it works if no exceptions are thrown.
            // Check testDB.mdb to be sure.
            return (await ResetDatabaseValid());
        }

        /* Tests that the ResetDatabaseAsync method does nothing when
         * there exists no backup database for a language.
         */ 
        public static async Task<bool> ResetDatabaseNonExistentBackup()
        {
            // Count of database terms.
            string sql = "Select * from ChineseC_Terms";
            List<Word> words = await FileIO.QueryDatabaseAsync(sql);
            int preCount = words.Count;

            await FileIO.ResetDatabaseAsync("ChineseC");

            words = await FileIO.QueryDatabaseAsync(sql);
            int postCount = words.Count;

            bool test = (await ResetDatabaseValid()) && preCount == postCount;

            return test;
        }

        /* Tests that the ResetDatabaseAsync method creates new tables
         * when passed a language with an existing backup, but 
         * nonexistent original database.
         */ 
        public static async Task<bool> ResetDatabaseNonExistentBase()
        {
            bool test = false;

            // Don't run this test if ResetDatabaseAsync doesn't work on expected input.
            // This is done to prevent dropping tables unnecessarily.
            if (await ResetDatabaseValid())
            {
                await FileIO.BackupDatabaseAsync("ChineseD");

                string sql = "Drop table ChineseD_Terms";
                await FileIO.EditDatabaseAsync(sql);
                sql = "Drop table ChineseD_Characteristics";
                await FileIO.EditDatabaseAsync(sql);

                await FileIO.ResetDatabaseAsync("ChineseD");

                sql = "Select * from ChineseD_Terms";
                List<Word> words = await FileIO.QueryDatabaseAsync(sql);
                Forest<string> chars = await FileIO.GetCharacteristicsAsync("ChineseD");

                test = words != null && chars != null && words.Count > 0 && chars.Trees.Count == 0;

                // Cleanup.
                sql = "Drop table ChineseD_Backup_Terms";
                await FileIO.EditDatabaseAsync(sql);
                sql = "Drop table ChineseD_Backup_Characteristics";
                await FileIO.EditDatabaseAsync(sql);
            }

            return test;
        }

        /* Tests that the GetAlternativeSpellingTypesAsync method properly
         * obtains a list of alternative spelling types from a language database.
         */
        public static async Task<bool> GetAlternativeSpellingTypesValid()
        {
            // Expected: Pinyin, Traditional
            List<string> types = await FileIO.GetAlternativeSpellingTypesAsync("ChineseA");

            bool test = types != null && types.Count == 2 &&
                        types.Contains("Pinyin") && types.Contains("Traditional");

            return test;
        }

        /* Tests that the GetAlternativeSpellingTypesAsync method returns null when
         * passed a null string.
         */
        public static async Task<bool> GetAlternativeSpellingTypesNull()
        {
            List<string> types = await FileIO.GetAlternativeSpellingTypesAsync(null);

            bool test = (await GetAlternativeSpellingTypesValid()) && types is null;

            return test;
        }

        /* Tests that the GetAlternativeSpellingTypesAsync method returns null
         * when passed a language name that does not exist in the full database.
         */
        public static async Task<bool> GetAlternativeSpellingTypesNonExistentLanguage()
        {
            List<string> types = await FileIO.GetAlternativeSpellingTypesAsync("ChineseE");

            bool test = (await GetAlternativeSpellingTypesValid()) && types is null;

            return test;
        }

        /* Tests that the GetAlternativeSpellingTypesAsync method returns
         * an empty list when passed a language name that exists but has no
         * alternative spelling types.
         */
        public static async Task<bool> GetAlternativeSpellingTypesNonExistentTypes()
        {
            List<string> types = await FileIO.GetAlternativeSpellingTypesAsync("ChineseC");

            bool test = types != null && types.Count == 0;

            return test;
        }

        /* Tests that the GetCharacteristicsAsync method properly obtains
         * a Forest of characteristics from a language database.
         */
        public static async Task<bool> GetCharacteristicsValid()
        {
            // Expected: title, family, ic/level01/lesson01, ic/level01/lesson02
            Forest<string> characteristics = await FileIO.GetCharacteristicsAsync("ChineseA");

            // 3 root trees; all expected characteristics exist; individual directories separated properly
            bool test = characteristics != null && characteristics.Trees.Count == 3 &&
                        !(characteristics.Search("title") is null) && !(characteristics.Search("family") is null) &&
                        characteristics.ToListWithDirectories().Count == 6 &&
                        characteristics.ToListWithDirectories().Contains("/ic/level01/lesson01") &&
                        characteristics.ToListWithDirectories().Contains("/ic/level01/lesson02");

            return test;
        }

        /* Tests that the GetCharacteristicsAsync method returns null when
         * passed a null string.
         */
        public static async Task<bool> GetCharacteristicsNull()
        {
            Forest<string> characteristics = await FileIO.GetCharacteristicsAsync(null);

            bool test = (await GetCharacteristicsValid()) && characteristics is null;

            return test;
        }

        /* Tests that the GetCharacteristicsAsync method returns null when
         * passed a language name that does not exist in the full database.
         */
        public static async Task<bool> GetCharacteristicsNonExistentLanguage()
        {
            Forest<string> characteristics = await FileIO.GetCharacteristicsAsync("ChineseE");

            bool test = (await GetCharacteristicsValid()) && characteristics is null;

            return test;
        }

        /* Tests that the GetCharacteristicsAsync method returns an empty forest
         * when passed a language name that exists but has no characteristics.
         */
        public static async Task<bool> GetCharacteristicsNonExistentCharacteristics()
        {
            Forest<string> characteristics = await FileIO.GetCharacteristicsAsync("ChineseB");

            bool test = characteristics != null && characteristics.Trees.Count == 0;

            return test;
        }
    }
}
