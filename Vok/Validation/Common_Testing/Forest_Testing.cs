﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vok.Src.Common;

namespace Vok.Validation.Common_Testing
{
    /* Tests for the Vok.Src.Common.Tree class.
     */
    class Forest_Testing
    {
        /* Ensures that the constructor properly creates a Forest 
         * when given valid parameters.
         */
        public static bool ForestConstructorValid()
        {
            Forest<int> forest = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                 new Tree<int>(3),
                                                 new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));

            bool test = forest.Trees.Count == 3 && forest.Trees[0] == new Tree<int>(1, new Tree<int>(2)) &&
                        forest.Trees[1] == new Tree<int>(3) && forest.Trees[2] == new Tree<int>(4, new Tree<int>(5), new Tree<int>(6));

            return test;
        }

        /* Ensures that the AddTree method properly adds a child to a Tree.
         */ 
        public static bool ForestAddTreeValid()
        {
            Forest<int> forest = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                 new Tree<int>(3),
                                                 new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));
            Tree<int> tree = new Tree<int>(7);
            forest.AddTree(tree);
            Forest<int> expected = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                   new Tree<int>(3),
                                                   new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)),
                                                   new Tree<int>(7));

            bool test = forest == expected;

            return test;
        }

        /* Ensures that the AddTree method does not add an empty Tree to a Forest.
         */ 
        public static bool ForestAddTreeEmpty()
        {
            Forest<int> forest = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                 new Tree<int>(3),
                                                 new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));
            Tree<int> tree = null;
            forest.AddTree(tree);
            Forest<int> expected = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                   new Tree<int>(3),
                                                   new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));

            bool test = ForestAddTreeValid() && forest == expected;

            return test;
        }

        /* Ensures that the AddTree method does not add a Tree
         * that is identical to a Tree already in the Forest.
         */ 
        public static bool ForestAddTreeDuplicateTree()
        {
            Forest<int> forest = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                 new Tree<int>(3),
                                                 new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));
            Tree<int> tree = new Tree<int>(3);
            forest.AddTree(tree);
            Forest<int> expected = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                   new Tree<int>(3),
                                                   new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));

            bool test = ForestAddTreeValid() && forest == expected;

            return test;
        }

        /* Ensures that the AddTree method does not add a Tree
         * that contains a node that exists in one of the Trees
         * already in the Forest.
         */ 
        public static bool ForestAddTreeDuplicateNode()
        {
            Forest<int> forest = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                 new Tree<int>(3),
                                                 new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));
            Tree<int> tree = new Tree<int>(6, new Tree<int>(7));
            forest.AddTree(tree);
            Forest<int> expected = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                   new Tree<int>(3),
                                                   new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));

            bool test = ForestAddTreeValid() && forest == expected;

            return test;
        }

        /* Ensures that the RemoveTree method removes a Tree from a Forest.
         */ 
        public static bool ForestRemoveTreeValid()
        {
            Forest<int> forest = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                 new Tree<int>(3),
                                                 new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));
            Tree<int> tree = new Tree<int>(3);
            forest.RemoveTree(tree);
            Forest<int> expected = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                   new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));

            bool test = forest == expected;

            return test;
        }

        /* Ensures that the RemoveTree method does nothing
         * when passed an empty parameter.
         */ 
        public static bool ForestRemoveTreeEmpty()
        {
            Forest<int> forest = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                 new Tree<int>(3),
                                                 new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));
            forest.RemoveTree(null);
            Forest<int> expected = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                   new Tree<int>(3),
                                                   new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));

            bool test = ForestRemoveTreeValid() && forest == expected;

            return test;
        }

        /* Ensures that the RemoveTree method does nothing
         * when passed a Tree not in the Forest.
         */ 
        public static bool ForestRemoveTreeNonExistent()
        {
            Forest<int> forest = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                 new Tree<int>(3),
                                                 new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));
            Tree<int> tree = new Tree<int>(7);
            forest.RemoveTree(tree);
            Forest<int> expected = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                   new Tree<int>(3),
                                                   new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));

            bool test = ForestRemoveTreeValid() && forest == expected;

            return test;
        }

        /* Ensures that the RemoveTree method does nothing
         * when passed a Tree that does not match a Tree in the
         * Forest exactly.
         */ 
        public static bool ForestRemoveTreeNonExact()
        {
            Forest<int> forest = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                 new Tree<int>(3),
                                                 new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));

            Tree<int> tree = new Tree<int>(3, new Tree<int>(7));
            forest.RemoveTree(tree);
            Forest<int> expected = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                   new Tree<int>(3),
                                                   new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));

            bool test = ForestRemoveTreeValid() && forest == expected;

            return test;
        }
        
        /* Ensures that the Search method returns
         * a full Tree in the Forest when passed the value
         * of that Tree's node.
         */ 
        public static bool ForestSearchValidNode()
        {
            Forest<int> forest = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                 new Tree<int>(3),
                                                 new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));
            int val = 1;
            Tree<int> expected = new Tree<int>(1, new Tree<int>(2));

            bool test = forest.Search(val) == expected;

            return test;
        }

        /* Ensures that the Seach method returns
         * the subtree with the node matching the search value.
         */ 
        public static bool ForestSearchValidChild()
        {
            Forest<int> forest = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                 new Tree<int>(3),
                                                 new Tree<int>(4, new Tree<int>(5, new Tree<int>(7)), new Tree<int>(6)));
            int val = 5;
            Tree<int> expected = new Tree<int>(5, new Tree<int>(7));

            bool test = forest.Search(val) == expected;

            return test;
        }

        /* Ensures that the Search method returns null
         * when passed an invalid search value.
         */ 
        public static bool ForestSeachNonExistent()
        {
            Forest<int> forest = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                 new Tree<int>(3),
                                                 new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));
            int val = 10;

            bool test = ForestSearchValidNode() && forest.Search(val) == null;

            return test;
        }

        /* Ensures that the ToList method returns a list in the proper order
         * based on the passed Forest.
         */ 
        public static bool ForestToListValid()
        {
            Forest<int> forest = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                 new Tree<int>(3),
                                                 new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));
            List<int> realList = forest.ToList();
            List<int> expected = new List<int>() { 1, 2, 3, 4, 5, 6 };

            if (realList == null || realList.Count != expected.Count)
            {
                return false;
            }

            for (int index = 0; index < realList.Count && index < expected.Count; index++)
            {
                if (realList[index] != expected[index])
                {
                    return false;
                }
            }

            return true;
        }

        /* Ensures that the RootsToList method returns a list consisting only
         * of the roots of the Trees in the proper order
         * based on the passed Forest.
         */ 
        public static bool ForestRootsToListValid()
        {
            Forest<int> forest = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                 new Tree<int>(3),
                                                 new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));
            List<int> realList = forest.RootsToList();
            List<int> expected = new List<int>() { 1, 3, 4 };

            if (realList == null || realList.Count != expected.Count)
            {
                return false;
            }

            for (int index = 0; index < realList.Count && index < expected.Count; index++)
            {
                if (realList[index] != expected[index])
                {
                    return false;
                }
            }

            return true;
        }

        /* Ensures that the ToListWithDirectories method returns a list in the proper order
         * based on the passed Forest.
         */ 
        public static bool ForestToListWithDirectoriesValid()
        {
            Forest<int> forest = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                 new Tree<int>(3),
                                                 new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));
            List<string> realList = forest.ToListWithDirectories();
            List<string> expected = new List<string>() { "/1", "/1/2", "/3", "/4", "/4/5", "/4/6" };

            if (realList == null || realList.Count != expected.Count)
            {
                return false;
            }

            for (int index = 0; index < realList.Count && index < expected.Count; index++)
            {
                if (!(realList[index].Equals(expected[index])))
                {
                    return false;
                }
            }

            return true;
        }

        /* Ensures that two Forests with identical Trees in the same order are equal.
         */ 
        public static bool ForestEqualsValid()
        {
            Forest<int> forest = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                 new Tree<int>(3),
                                                 new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));
            Forest<int> other = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                new Tree<int>(3),
                                                new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));

            bool test = forest == other;

            return test;
        }

        /* Ensures that two Forests with identical Trees stored in a different
         * order are equal.
         */ 
        public static bool ForestOrderEqualsValid()
        {
            Forest<int> forest = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                 new Tree<int>(3),
                                                 new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));
            Forest<int> other = new Forest<int>(new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)),
                                                new Tree<int>(1, new Tree<int>(2)),
                                                new Tree<int>(3));

            bool test = forest == other;

            return test;
        }

        /* Ensures that two Forests with non-matching Trees
         * are not equal.
         */ 
        public static bool ForestEqualsImproperTree()
        {
            Forest<int> forest = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                 new Tree<int>(3),
                                                 new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));
            Forest<int> other = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                new Tree<int>(3),
                                                new Tree<int>(10, new Tree<int>(5), new Tree<int>(6)));

            bool test = forest != other;

            return test;
        }

        /* Ensures that a null Forest is not equal to a non-null Forest.
         */ 
        public static bool ForestEqualsNull()
        {
            Forest<int> forest = new Forest<int>(new Tree<int>(1, new Tree<int>(2)),
                                                 new Tree<int>(3),
                                                 new Tree<int>(4, new Tree<int>(5), new Tree<int>(6)));

            bool test = forest != null;

            return test;
        }
    }
}
