﻿using System.Collections.Generic;
using Vok.Src.Common;

namespace Vok.Validation.Common_Testing
{
    /* Tests for the Vok.Src.Common.Tree class.
     */ 
    static class Tree_Testing
    {
        /* Ensures that the constructor properly creates a Tree 
         * when given valid parameters.
         */
        public static bool TreeConstructorValid()
        {
            Tree<int> tree = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));

            // Structure of Tree is as expected.
            bool test = tree.Node == 1 && tree.Children.Count == 2 && tree.Children[0].Node == 2 &&
                        tree.Children[0].Children.Count == 0 && tree.Children[1].Node == 3 &&
                        tree.Children[1].Children.Count == 1 && tree.Children[1].Children[0].Node == 4 &&
                        tree.Children[1].Children[0].Children.Count == 0;

            return test;
        }

        /* Ensures that the AddChild method properly adds a child to a Tree.
         */ 
        public static bool TreeAddChildValid()
        {
            Tree<int> tree = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));
            Tree<int> child = new Tree<int>(5, new Tree<int>(6));
            tree.AddChild(child);
            Tree<int> expected = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)), new Tree<int>(5, new Tree<int>(6)));

            bool test = tree == expected;

            return test;
        }

        /* Ensures that the AddChild method does not add an empty child to a Tree.
         */ 
        public static bool TreeAddChildEmpty()
        {
            Tree<int> tree = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));
            Tree<int> child = null;
            tree.AddChild(child);
            Tree<int> expected = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));

            bool test = TreeAddChildValid() && tree == expected;

            return test;
        }

        /* Ensures that the AddChild method does not add a child 
         * containing a duplicate node to a Tree.
         */
        public static bool TreeAddChildDuplicateNode()
        {
            Tree<int> tree = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));
            Tree<int> child = new Tree<int>(3, new Tree<int>(7));
            tree.AddChild(child);
            Tree<int> expected = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));

            bool test = TreeAddChildValid() && tree == expected;

            return test;
        }

        /* Ensures that the AddChild method does not add a child
         * where the node is unique 
         * but a child contains a duplicate node.
         */ 
        public static bool TreeAddChildDuplicateChildNode()
        {
            Tree<int> tree = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));
            Tree<int> child = new Tree<int>(7, new Tree<int>(3));
            tree.AddChild(child);
            Tree<int> expected = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));

            bool test = TreeAddChildValid() && tree == expected;

            return test;
        }

        /* Ensures that the RemoveChild method removes a child from a Tree.
         */ 
        public static bool TreeRemoveChildValid()
        {
            Tree<int> tree = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));
            Tree<int> child = new Tree<int>(3, new Tree<int>(4));
            tree.RemoveChild(child);
            Tree<int> expected = new Tree<int>(1, new Tree<int>(2));

            bool test = tree == expected;

            return test;
        }

        /* Ensures that the RemoveChild method does nothing 
         * when passed an empty parameter.
         */ 
        public static bool TreeRemoveChildEmpty()
        {
            Tree<int> tree = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));
            tree.RemoveChild(null);
            Tree<int> expected = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));

            bool test = TreeRemoveChildValid() && tree == expected;

            return test;
        }

        /* Ensures that the RemoveChild method does nothing
         * when passed a nonexistant child.
         */ 
        public static bool TreeRemoveChildNonExistent()
        {
            Tree<int> tree = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));
            Tree<int> child = new Tree<int>(5);
            tree.RemoveChild(child);
            Tree<int> expected = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));

            bool test = TreeRemoveChildValid() && tree == expected;

            return test;
        }

        /* Ensures that the RemoveChild method does nothing
         * when passed a child that does not match a child in the Tree exactly.
         */ 
        public static bool TreeRemoveChildNonExact()
        {
            Tree<int> tree = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));
            Tree<int> child = new Tree<int>(3);
            tree.RemoveChild(child);
            Tree<int> expected = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));

            bool test = TreeRemoveChildValid() && tree == expected;

            return test;
        }

        /* Ensures that the Search method returns
         * the full tree when passed the value of the Tree's node.
         */ 
        public static bool TreeSearchValidNode()
        {
            Tree<int> tree = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));
            int val = 1;
            Tree<int> child = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));

            bool test = tree.Search(val) == child;

            return test;
        }

        /* Ensures that the Search method returns 
         * the subtree with the node matching the search value.
         */ 
        public static bool TreeSearchValidChild()
        {
            Tree<int> tree = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));
            int val = 3;
            Tree<int> child = new Tree<int>(3, new Tree<int>(4));

            bool test = tree.Search(val) == child;

            return test;
        }

        /* Ensures that the Search method returns null
         * when passed an invalid search value.
         */ 
        public static bool TreeSearchNonExistent()
        {
            Tree<int> tree = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));
            int val = 5;

            bool test = TreeSearchValidNode() && tree.Search(val) == null;

            return test;
        }

        /* Ensures that the ToList method returns a list in the proper order
         * based on the passed Tree.
         */ 
        public static bool TreeToListValid()
        {
            Tree<int> tree = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));
            List<int> realList = tree.ToList();
            List<int> expectedList = new List<int>() { 1, 2, 3, 4 };

            if (realList == null)
            {
                return false;
            }

            for (int index = 0; index < realList.Count && index < expectedList.Count; index++)
            {
                if (realList[index] != expectedList[index])
                {
                    return false;
                }
            }

            return true;
        }

        /* Ensures that the ToListWithDirectories method returns a list in the proper order
         * based on the passed Tree.
         */ 
        public static bool TreeToListWithDirectoriesValid()
        {
            Tree<int> tree = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));
            List<string> realList = tree.ToListWithDirectories();
            List<string> expectedList = new List<string>() { "/1", "/1/2", "/1/3", "/1/3/4" };
            
            if (realList == null || realList.Count != expectedList.Count)
            {
                return false;
            }

            for (int index = 0; index < realList.Count && index < expectedList.Count; index++)
            {
                if (!(realList[index].Equals(expectedList[index])))
                {
                    return false;
                }
            }

            return true;
        }

        /* Ensures that two Trees with identical nodes & children are equal.
         */ 
        public static bool TreeEqualsValid()
        {
            Tree<int> tree = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));
            Tree<int> other = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));
            bool test = tree == other;

            return test;
        }

        /* Ensures that two Trees with identical children but
         * different nodes are not equal.
         */ 
        public static bool TreeEqualsImproperNode()
        {
            Tree<int> tree = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));
            Tree<int> other = new Tree<int>(0, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));
            bool test = tree != other;

            return test;
        }

        /* Ensures that two Trees with identical nodes but
         * different children are not equal.
         */ 
        public static bool TreeEqualsImproperChild()
        {
            Tree<int> tree = new Tree<int>(1, new Tree<int>(2), new Tree<int>(5, new Tree<int>(4)));
            Tree<int> other = new Tree<int>(1, new Tree<int>(2), new Tree<int>(3, new Tree<int>(4)));
            bool test = tree != other;

            return test;
        }

        /* Ensures that a null Tree is not equal to a non-null Tree.
         */ 
        public static bool TreeEqualsNull()
        {
            Tree<int> tree = new Tree<int>(1, new Tree<int>(2), new Tree<int>(5, new Tree<int>(4)));
            bool test = tree != null;

            return test;
        }
    }
}
