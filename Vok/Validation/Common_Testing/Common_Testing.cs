﻿using static Vok.Src.Common.Common;

namespace Vok.Validation.Common_Testing
{
    /* Tests for the Vok.Src.Common.Common.PartOfSpeech enum and related methods.
     */
    static class Common_Testing
    {
        /* Ensures that the ToPartOfSpeech method returns the appropriate
         * PartOfSpeech value when passed a valid string.
         */ 
        public static bool StringToPartOfSpeechValid()
        {
            PartOfSpeech[] parts = { PartOfSpeech.Adjective, PartOfSpeech.MeasureWord, PartOfSpeech.VerbPlusComplement };
            string[] strings = { "Adjective", "Measure Word", "Verb Plus Complement" };

            bool test = true;

            for (int index = 0; index < parts.Length && index < strings.Length; index++)
            {
                PartOfSpeech? part = strings[index].ToPartOfSpeech();
                if (!(part.Equals(parts[index])))
                {
                    test = false;
                }
            }

            return test;
        }

        /* Ensures that the ToPartOfSpeech method returns null
         * when passed a string that does not have a corresponding PartOfSpeech value.
         */
        public static bool StringToPartOfSpeechInvalid()
        {
            string[] strings = { "Conjuction", "Word", "Thing of Speech" };

            bool test = true;

            for (int index = 0; index < strings.Length; index++)
            {
                PartOfSpeech? part = strings[index].ToPartOfSpeech();
                if (!(part.Equals(null)))
                {
                    test = false;
                }
            }

            return StringToPartOfSpeechValid() && test;
        }

        /* Ensures that the ToStringName method returns the appropriate
         * string value when passed a PartOfSpeech.
         */
        public static bool PartOfSpeechToStringTest()
        {
            PartOfSpeech?[] parts = { PartOfSpeech.Numeral, PartOfSpeech.ProperNoun, PartOfSpeech.VerbPlusComplement };
            string[] strings = { "Numeral", "Proper Noun", "Verb Plus Complement" };

            bool test = true;

            for (int index = 0; index < parts.Length && index < strings.Length; index++)
            {
                string str = parts[index].ToStringName();
                if (str == null || !(str.Equals(strings[index])))
                {
                    test = false;
                }
            }

            return test;
        }

        /* Ensures that the ToTitleCase method properly converts a string
         * to title case.
         */ 
        public static bool ToTitleCaseTest()
        {
            string[] toConvert = { "TEXT TEXT", "text text", "TeXt tExT", "Text Text" };
            string expected = "Text Text";

            bool test = true;

            foreach (string word in toConvert)
            {
                if (!(word.ToTitleCase().Equals(expected)))
                {
                    test = false;
                }
            }

            return test;
        }
    }
}
