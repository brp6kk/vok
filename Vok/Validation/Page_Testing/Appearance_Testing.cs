﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using static Vok.Src.Common.Common;
using Vok.Src.UI;

namespace Vok.Validation.Page_Testing
{
    static class Appearance_Testing
    {
        private static int fontCount = 4;

        public static void LoadTestPage()
        {
            //if (Application.OpenForms.Count == 1) { UpdateFrmTesting(); (new FrmTesting()).Show(); };

            Page page = new Page();
            for (int i = 0; i < fontCount; i++)
            {
                page.AddControl(new Label()
                {
                    Name = "label" + i,
                    //Text = "The quick brown fox jumped over the lazy dog\n我觉得你是很好的朋友\nфывапролдячсмитьб\nプログラムに入力できる単語をいくつか紹介します。",
                    //      Latin        Cyrillic   Greek             Armenian             Hangeul       Hanzi Zhuyin   Japanese Arabic        Hebrew     Thai        Hindi
                    Text = "Latin script\nкириллица\nελληνικό σενάριο\nհայկական գրություն\n한글 스크립트\n汉字\nㄅㄆㄇㄈ\nかな文字\nالنص العربي\nתסריט עברי\nสคริปต์ไทย\nहिंदी लिपि\n",
                    AutoSize = false,
                    Size = new Size(200, 300),
                    BorderStyle = BorderStyle.FixedSingle
                });
            }

            Button btn = new Button()
            {
                Name = "button",
                Text = "Press me",
                Size = new Size(100, 100)
            };
            btn.Click += UpdateButton;
            page.AddControl(btn);

            //LightModeGreen(page);
            //DarkModeGreen(page);
            //LightModeBlue(page);
            //DarkModeBlue(page);
            //DarkMode = false;
            //FontChange(page);
            //UpdateFrmTesting();
            
            //page.Panel.SetDefaultAppearance();
            foreach (Control c in page.Controls)
            {
                c.SetDefaultAppearance();
            }

            Page.UpdatePage(page);
            //if (Application.OpenForms.Count == 1) (new FrmTesting()).Show();
        }

        public static void UpdateButton(object sender, EventArgs e)
        {
            //DarkMode = !DarkMode;
        }

        public static void UpdateFrmTesting()
        {
            currentForm.BackColor = Color.FromArgb(20, 20, 25);
            currentForm.ActiveControl = null;

            // Need recursion to get all controls in groupbox, etc
            foreach (Control c in currentForm.Controls)
            {
                //currentForm.Controls.Find("label1", true);
                c.SetDefaultAppearance();
                //c.BackColor = Color.FromArgb(20, 20, 25);
                //c.ForeColor = Color.FromArgb(225, 225, 235);
                //c.Font = new Font("DengXian", 14);

                //c.ContainsFocus = false;
            }

            Control[] output = currentForm.Controls.Find("listBox1", true);
            output[0].BackColor = Color.FromArgb(20, 20, 25);
        }

        public static void LightModeGreen(Page p)
        {
            Color backLight = Color.FromArgb(245, 245, 235);
            Color backMid = Color.FromArgb(235, 235, 225);
            Color backDark = Color.FromArgb(225, 225, 215);

            Color foreDark = Color.FromArgb(15, 15, 10);
            Color foreMid = Color.FromArgb(25, 25, 20);
            Color foreLight = Color.FromArgb(35, 35, 30);

            p.Panel.BackColor = backLight;
            p.Panel.ForeColor = foreLight;
        }

        public static void DarkModeGreen(Page p)
        {
            Color backDark = Color.FromArgb(15, 15, 10);
            Color backMid = Color.FromArgb(25, 25, 20);
            Color backLight = Color.FromArgb(35, 35, 30);

            Color foreLight = Color.FromArgb(245, 245, 235);
            Color foreMid = Color.FromArgb(235, 235, 225);
            Color foreDark = Color.FromArgb(225, 225, 215);

            p.Panel.BackColor = backLight;
            p.Panel.ForeColor = foreLight;
        }

        public static void LightModeBlue(Page p)
        {
            Color foreDark = Color.FromArgb(10, 10, 15);
            Color foreMid = Color.FromArgb(20, 20, 25);
            Color foreLight = Color.FromArgb(30, 30, 35);

            Color backLight = Color.FromArgb(235, 235, 245);
            Color backMid = Color.FromArgb(225, 225, 235);
            Color backDark = Color.FromArgb(215, 215, 225);

            p.Panel.BackColor = backLight;
            p.Panel.ForeColor = foreLight;
        }

        public static void DarkModeBlue(Page p)
        {
            Color backDark = Color.FromArgb(10, 10, 15);
            Color backMid = Color.FromArgb(20, 20, 25);
            Color backLight = Color.FromArgb(30, 30, 35);

            Color foreLight = Color.FromArgb(235, 235, 245);
            Color foreMid = Color.FromArgb(225, 225, 235);
            Color foreDark = Color.FromArgb(215, 215, 225);

            p.Panel.BackColor = backLight;
            p.Panel.ForeColor = foreLight;
            
        }

        public static void FontChange(Page p)
        {
            Font[] f = { //new Font("Bahnschrift", 16), // bad for characters
                         //new Font("Century Gothic", 16), // bad for characters
                         //new Font("Consolas", 16), // bad for characters
                         //new Font("Fangsong", 16), // not a fan of latin
                         //new Font("DFKai-SB", 16), // not a fan of latin
                         new Font("DengXian", 14), // 非常好！
                         //new Font("Microsoft JhengHei", 16), // not bad, but DengXian is better
                         //new Font("NSimSun", 16) // not a fan of latin
            };

            for (int i = 0; i < fontCount; i++)
            {
                Control c = p.Controls[i];
                c.Font = f[0];
                c.Padding = new Padding(10);
                c.Margin = new Padding(20);
            }
        }
    }
}
