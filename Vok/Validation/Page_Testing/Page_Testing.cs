﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using static Vok.Src.Common.Common;
using Vok.Src.UI;

namespace Vok.Validation.Page_Testing
{
    static class Page_Testing
    {
        public static void LoadTestPage()
        {
            TestPage01();
        }

        public static void TestPage01()
        {
            Page page = new Page();
            page.Panel.BackColor = Color.Blue;
            Button btn = new Button();
            btn.Click += btnClick01;
            btn.Resize += resize;
            page.AddControl(btn);
            Label lbl = new Label()
            {
                Name = "label",
                Text = "test test\ntest\n test",
                AutoSize = false,
                Height = 50,
                Width = 50,
                BorderStyle = BorderStyle.FixedSingle
            };
            lbl.Resize += resize;
            page.AddControl(lbl);
            Page.UpdatePage(page);
        }

        public static void TestPage02()
        {
            Page page = new Page();
            page.Panel.BackColor = Color.Yellow;
            Button btn = new Button();
            btn.Click += btnClick02;
            page.AddControl(btn);
            page.AddControl(new ListBox() { Name = "name" });
            Page.UpdatePage(page);
        }

        public static void resize(object sender, EventArgs e)
        {
            Control ctr = (Control)sender;
            ctr.Width = currentPage.Panel.Width / 3;
            ctr.Height = currentPage.Panel.Width / 4;
        }

        public static void btnClick01(object sender, EventArgs e)
        {
            TestPage02();
        }

        public static void btnClick02(object sender, EventArgs e)
        {
            TestPage01();
        }
    }
}
