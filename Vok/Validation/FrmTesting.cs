﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Vok.Src.Common;
using Vok.Src.Database;

namespace Vok.Validation
{
    public partial class FrmTesting : Form
    {
        public FrmTesting()
        {
            InitializeComponent();
        }

        private async void FrmTesting_Load(object sender, EventArgs e)
        {
            //RunTreeTests();
            //RunForestTests();
            //RunCommonTests();
            //RunWordTests();
            //await RunFileIOTests();
            //await RunDatabaseTests();
            //RunTests.RunTestsFor("Vok.Validation.Database_Testing.Database_Testing");
            await RunSearchCommandTests();
            //await RunDatabaseSearchTests();
            //RunPageTests();
            //await RunFileIOLanguageTests();
            //RunTests.RunTestsFor("Vok.Validation.Database_Testing.FileIOLanguage_Testing");
            //RunAppearanceTests();
            //await RunTestTests();
        }

        private void RunTreeTests()
        {
            string str = RunTests.RunTestsFor("Vok.Validation.Common_Testing.Tree_Testing");
            label1.Text = str;
        }

        private void RunForestTests()
        {
            string str = RunTests.RunTestsFor("Vok.Validation.Common_Testing.Forest_Testing");
            label1.Text = str;
        }

        private void RunCommonTests()
        {
            string str = RunTests.RunTestsFor("Vok.Validation.Common_Testing.Common_Testing");
            label1.Text = str;
        }

        private void RunWordTests()
        {
            string str = RunTests.RunTestsFor("Vok.Validation.Database_Testing.Word_Testing");
            label1.Text = str;
        }

        private async Task RunFileIOTests()
        {
            string str = await RunTests.RunTestsForAsync("Vok.Validation.Database_Testing.FileIO_Testing");
            label1.Text = str;
        }

        private async Task RunDatabaseTests()
        {
            string str = await RunTests.RunTestsForAsync("Vok.Validation.Database_Testing.Database_Testing");
            label1.Text = str;
        }

        private async Task RunSearchCommandTests()
        {
            Common.currentDB = await Database.CreateDatabase("ChineseA");
            string str = RunTests.RunTestsFor("Vok.Validation.Database_Testing.SearchCommand_Testing");
            label1.Text = str;
        }

        private async Task RunDatabaseSearchTests()
        {
            Common.currentDB = await Database.CreateDatabase("ChineseA");
            string str = await RunTests.RunTestsForAsync("Vok.Validation.Database_Testing.Database_Search_Testing");
            label1.Text = str;
        }

        private void RunPageTests()
        {
            Common.currentForm = this;
            Page_Testing.Page_Testing.LoadTestPage();
        }

        private async Task RunFileIOLanguageTests()
        {
            label1.Text = "";
            string str = RunTests.RunTestsFor("Vok.Validation.Database_Testing.FileIOLanguage_Testing");
            if (!str.Equals("All tests passed.\n"))
            {
                label1.Text = str;
            }

            str = await RunTests.RunTestsForAsync("Vok.Validation.Database_Testing.FileIOLanguage_Testing");
            label1.Text += str;
        }

        private void RunAppearanceTests()
        {
            Common.currentForm = this;
            Page_Testing.Appearance_Testing.LoadTestPage();
        }

        private async Task RunTestTests()
        {
            Common.currentDB = await Database.CreateDatabase("ChineseA");
            string str = RunTests.RunTestsFor("Vok.Validation.Test_Testing.Test_Testing");
            label1.Text = str;
        }
    }
}
