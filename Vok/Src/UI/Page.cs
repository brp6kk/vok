﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using static Vok.Src.Common.Common;
using Vok.Src.Database;

namespace Vok.Src.UI
{
    /* Represents a page of Vok.
     * Controls are added to the page in a flow layout.
     * Resizing the page raises each control's resize event - 
     * define how a control should be resized upon window's resize
     * within the event handler.
     * Static method UpdatePage changes the page shown on the
     * current form to the passed page.
     */ 
    class Page
    {
        public List<Control> Controls { get; private set; }
        public FlowLayoutPanel Panel { get; private set; }
        public List<Word> WordList { get; private set; }
        public List<Word> OriginalWordList { get; private set; }
        public int? WordListIndex { get; set; }

        /* Constructor.
         */
        public Page(List<Word> wordList = null)
        {
            Controls = new List<Control>();
            Panel = new FlowLayoutPanel()
            {
                Name = "pnlPage",
                Dock = DockStyle.Fill
            };
            Panel.SetDefaultAppearance();
            Panel.Resize += ResizeControls;

            WordList = wordList;
            OriginalWordList = new List<Word>();
            if (wordList != null)
            {
                wordList.ForEach((Word w) => OriginalWordList.Add((Word)w.Clone()));
            }
            
            WordListIndex = wordList is null ? (int?)null : 0;
        }

        /* Adds a Control to the Page.
         */ 
        public void AddControl(Control control)
        {
            if (control != null && !ControlExists(control.Name))
            {
                Controls.Add(control);
            }
        }

        /* Places all controls on the page, 
         * in the order they were inserted
         * into this page.
         */ 
        public void LoadPage()
        {
            // Removes previous controls to ensure
            // no double addition of controls.
            Panel.Controls.Clear();

            foreach (Control control in Controls)
            {
                Panel.Controls.Add(control);
            }
        }

        /* Resizes the controls on a page appropriately
         * based on the size of the page.
         * Note that for resize to occur, a control's
         * resize event must have a handler.
         */ 
        public void ResizeControls(object sender, EventArgs e)
        {
            // Prevents bad resize when minimizing page
            if (!Panel.Size.Equals(new Size(0, 0)))
            {
                foreach (Control control in Controls)
                {
                    // Dummy code to raise control's Resize event.
                    // If no Resize event exists, then the control
                    // will remain the same size.
                    control.Scale(new SizeF(0.8f, 0.8f));
                    control.Scale(new SizeF(1.25f, 1.25f));
                }
            }
        }

        /* Determines if a control with the passed name
         * already exists in this page.
         */ 
        public bool ControlExists(string name)
        {
            return Controls.Exists((Control c) => c.Name.Equals(name));
        }

        /* Changes the current page on the current form to newPage.
         */
        public static void UpdatePage(Page newPage)
        {
            if (currentForm != null && newPage != null)
            {
                currentForm.Controls.Remove(currentPage?.Panel);
                currentPage = newPage;
                newPage.LoadPage();
                currentForm.Controls.Add(newPage.Panel);
                newPage.Panel.BringToFront();
            }
        }
    }
}
