﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using Vok.Src.Database;
using static Vok.Src.Common.Common;

namespace Vok.Src.UI.Predefined_Pages
{
    /* Sets up various Pages for Vok.
     * These pages are used to edit a database:
     *  - WordInputPage(List<Word>)
     *  - SearchFiltersPage(NextPageCode)
     *  - SearchResultsPage(List<Word>)
     *  - RemoveTermsPage(List<Word>)
     */
    static class EditDatabasePages
    {
        #region Word input page creation - WordInputPage(List<Word>)

        /* Creates a new page used to input words into the dictionary.
         * Parameter wordList consists of list of words being added/edited.
         * This page is used both for adding new words and editing existing words.
         * Usage: 
         *  adding words: wordList = new List<Word>()
         *  editing words: wordList = [list of Words read in from Database.Search]
         */
        public static Page WordInputPage(List<Word> wordList = null)
        {
            // Default parameter wordList = new List<Word>() is not allowed -
            // this is my janky work-around.
            if (wordList is null)
            {
                wordList = new List<Word>();
            }
            Page page = new Page(wordList);

            // Groupbox + textbox - term input
            TextBox term = new TextBox()
            {
                Name = "txtTermInput",
                RightToLeft = LeftToRight ? RightToLeft.No : RightToLeft.Yes
            };
            GroupBox grpTerm = new GroupBox()
            {
                Name = "grpTermInput",
                Text = "Term"
            };
            grpTerm.SetDefaultAppearance();
            grpTerm.Controls.Add(term);
            grpTerm.BackColor = currentPalette.BackAccent;
            term.SetDefaultAppearance();
            grpTerm.Resize += GroupBoxShortResize;
            page.AddControl(grpTerm);

            // Checkbox - special yes/no
            CheckBox special = new CheckBox()
            {
                Name = "chkSpecial",
                Text = "Special",
                AutoSize = false,
                TextAlign = ContentAlignment.MiddleRight,
                CheckAlign = ContentAlignment.MiddleRight
            };
            special.SetDefaultAppearance();
            special.Resize += CheckBoxResize;
            page.AddControl(special);

            // Groupbox + multi-prompt - alternative spelling input
            Controls.MultiPrompt alternativeSpellings = new Controls.MultiPrompt()
            {
                Name = "prmAlternativeSpellings",
                Prompts = currentDB.AlternativeSpellingTypes.ToArray()
            };
            GroupBox grpAlternativeSpellings = new GroupBox()
            {
                Name = "grpAlternativeSpellings",
                Text = "Alternative Spellings"
            };
            grpAlternativeSpellings.SetDefaultAppearance();
            grpAlternativeSpellings.Controls.Add(alternativeSpellings);
            grpAlternativeSpellings.BackColor = currentPalette.BackAccent;
            foreach (Control c in alternativeSpellings.Controls)
            {
                c.SetDefaultAppearance();
            }
            grpAlternativeSpellings.Resize += GroupBoxShortResize;
            page.AddControl(grpAlternativeSpellings);

            // Groupbox + drop-down list combobox - part of speech input
            ComboBox partOfSpeech = new ComboBox()
            {
                Name = "cboPartOfSpeech",
                DropDownStyle = ComboBoxStyle.DropDownList
            };
            foreach (PartOfSpeech? part in Enum.GetValues(typeof(PartOfSpeech)))
            {
                partOfSpeech.Items.Add(part.ToStringName());
            }
            GroupBox grpPartOfSpeech = new GroupBox()
            {
                Name = "grpPartOfSpeech",
                Text = "Part of Speech"
            };
            grpPartOfSpeech.SetDefaultAppearance();
            grpPartOfSpeech.Controls.Add(partOfSpeech);
            grpPartOfSpeech.BackColor = currentPalette.BackAccent;
            partOfSpeech.SetDefaultAppearance();
            grpPartOfSpeech.Resize += GroupBoxShortResize;
            page.AddControl(grpPartOfSpeech);

            // Groupbox + multiline textbox - definition input
            TextBox definition = new TextBox()
            {
                Name = "txtDefinitions",
                Multiline = true,
                ScrollBars = ScrollBars.Vertical,
                BorderStyle = BorderStyle.FixedSingle
            };
            GroupBox grpDefinition = new GroupBox()
            {
                Name = "grpDefinitions",
                Text = "Definitions"
            };
            grpDefinition.SetDefaultAppearance();
            grpDefinition.Controls.Add(definition);
            grpDefinition.BackColor = currentPalette.BackAccent;
            definition.SetDefaultAppearance();
            grpDefinition.Resize += GroupBoxFullResize;
            page.AddControl(grpDefinition);

            // Groupbox + multi-choice - characteristics input
            Controls.MultiChoice characteristics = new Controls.MultiChoice()
            {
                Name = "choCharacteristics",
                Choices = currentDB.Characteristics.ToListWithDirectories().ToArray()
            };
            GroupBox grpCharacteristics = new GroupBox()
            {
                Name = "grpCharacteristics",
                Text = "Characteristics"
            };
            grpCharacteristics.SetDefaultAppearance();
            grpCharacteristics.Controls.Add(characteristics);
            grpCharacteristics.BackColor = currentPalette.BackAccent;
            foreach (Control c in characteristics.Controls)
            {
                c.SetDefaultAppearance();
            }
            grpCharacteristics.Resize += GroupBoxFullResize;
            page.AddControl(grpCharacteristics);

            // I know this is hideous 
            // but this is the only way I can think of
            // to load a word onto the page without 
            // forcing the calling code to also call LoadWordInputPage
            characteristics.Load += LoadWordInputPage;

            // Buttons - previous term, next term, edit default characteristics, finish
            string[] buttonTexts = { "Previous Term", "Next Term", "Edit Default Characteristics", "Finish" };
            string[] buttonNames = { "btnPreviousTerm", "btnNextTerm", "btnCharacteristics", "btnFinish" };
            for (int index = 0; index < 4; index++)
            {
                Button button = new Button()
                {
                    Name = buttonNames[index],
                    Text = buttonTexts[index]
                };
                button.SetDefaultAppearance();
                button.BackColor = currentPalette.BackAccent;
                button.Resize += ButtonResize;
                button.Click += ButtonClick;
                page.AddControl(button);
            }

            // Non-visible listbox - Items contains default characteristics
            ListBox defaultCharacteristics = new ListBox()
            {
                Name = "lstDefaultCharacteristics",
                Visible = false
            };
            page.AddControl(defaultCharacteristics);

            return page;
        }

        /* Loads the first word of the provided word list (if it exists)
         * onto the WordInputPage.
         */ 
        public static void LoadWordInputPage(object sender, EventArgs e)
        {
            if (currentPage.WordList != null && currentPage.WordList.Count > 0)
            {
                ShowWordOnWordInputPage(currentPage.WordList[0]);
            }
        }

        /* Event handler to resize a groupbox 
         * to half the size of the panel.
         */ 
        public static void GroupBoxShortResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Groupbox size change.
            int width = panel.Width / 2 - (int)(DEFAULT_MARGIN.All * 2.5);
            int height = (int)(panel.Height / 6.75);
            control.Size = new Size(width, height);

            // Groupbox font size change.
            int fontSizeWidth = width / 40;
            int fontSizeHeight = height / 8;
            control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);

            // Control resize.
            GroupBoxControlResize((GroupBox)control);
        }

        /* Event handler to resize a groupbox 
         * to the full size of the panel.
         */ 
        public static void GroupBoxFullResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Groupbox size change.
            int width = panel.Width / 2 - (int)(DEFAULT_MARGIN.All * 2.5);
            int height = (int)(panel.Height / 6.75) * 2;
            control.Size = new Size(width, height);

            // Groupbox font size change.
            control.Font = control.FindForm().Controls.Find("grpTermInput", true)[0].Font;

            // Control resize.
            GroupBoxControlResize((GroupBox)control);
        }

        /* Helper function to resize the control within a groupbox.
         */ 
        public static void GroupBoxControlResize(GroupBox control)
        {
            // Control size & position change.
            int innerWidth = (int)(control.Font.Size * 25);
            int innerHeight = (int)(control.Font.Size * 8);
            int innerX = DEFAULT_MARGIN.All;
            int innerY = DEFAULT_MARGIN.All + (int)control.Font.Size;
            control.Controls[0].Size = new Size(innerWidth, innerHeight);
            control.Controls[0].Location = new Point(innerX, innerY);

            // Control font size change.
            control.Controls[0].Font = new Font(control.Font.Name, (int)(control.Font.Size * 1.25));
        }

        /* Event handler to resize a checkbox.
         */ 
        public static void CheckBoxResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Checkbox size change.
            int width = panel.Width / 2 - (int)(DEFAULT_MARGIN.All * 2.5);
            int height = (int)(panel.Height / 6.75);
            control.Size = new Size(width, height);

            // Font size change.
            int fontSizeWidth = width / 32;
            int fontSizeHeight = height / 10;
            control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);
        }

        /* Event handler to resize a button.
         */
        public static void ButtonResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Button size change.
            int width = panel.Width / 4 - (int)(DEFAULT_MARGIN.All * 2.25);
            int height = (int)(panel.Height / 10);
            control.Size = new Size(width, height);

            // Font size change.
            int fontSizeWidth = width / 22;
            int fontSizeHeight = height / 7;
            control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);
        }

        /* Event handler to call the appropriate method 
         * upon button click based on the name of the button.
         */
        public static void ButtonClick(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            // not sure yet if sender & e are necessary, potentially delete
            switch (button.Name)
            {
                case "btnPreviousTerm":
                    ButtonPreviousTermClick();
                    break;
                case "btnNextTerm":
                    ButtonNextTermClick();
                    break;
                case "btnCharacteristics":
                    ButtonCharacteristicsClick();
                    break;
                case "btnFinish":
                    ButtonFinishClick();
                    break;
                default:
                    break;
            }
        }

        /* Event handler to go to the previous term (if it exists)
         * upon a button click.
         */
        public static async void ButtonPreviousTermClick()
        {
            if (currentPage.WordListIndex <= 0)
            {
                return;
            }

            UpdateWordList();
            currentPage.WordListIndex--;
            await UpdateCharacteristics();

            ShowWordOnWordInputPage(currentPage.WordList[(int)currentPage.WordListIndex]);
        }

        /* Event handler to go to:
         *  - the next term (if it exists)
         *  - a new, blank term (if no next term exists)
         * upon a button click.
         */ 
        public static async void ButtonNextTermClick()
        {
            // Do not go to a new blank term if this term is still blank.
            if (!UpdateWordList())
            {
                return;
            }

            await UpdateCharacteristics();
            currentPage.WordListIndex++;

            // Either show a blank WordInputPage
            // or show the next word.
            if (currentPage.WordListIndex >= currentPage.WordList.Count)
            {
                ShowWordOnWordInputPage(new Word(""));
            }
            else
            {
                ShowWordOnWordInputPage(currentPage.WordList[(int)currentPage.WordListIndex]);
            }
        }

        /* Event handler to open an EditDefaultCharacteristicsPage
         * in a popup window.
         */ 
        public static void ButtonCharacteristicsClick()
        {
            Controls.PopupWindow popup = new Controls.PopupWindow();
            popup.Load += PopupPages.CharacteristicsPageLoad;
            popup.FormClosing += PopupPages.CharacteristicsPageClose;

            Page page = PopupPages.EditDefaultCharacteristicsPage();
            page.LoadPage();
            popup.Controls.Add(page.Panel);
            page.Panel.BringToFront();

            popup.Show();
        }

        /* Event handler to add the new words to the database
         * and going back to the language home page.
         */ 
        public static async void ButtonFinishClick()
        {
            UpdateWordList();
            await UpdateCharacteristics();

            int index = 0;

            // Edit words if necessary.
            for (; index < currentPage.OriginalWordList.Count; index++)
            {
                await currentDB.EditWordAsync(currentPage.OriginalWordList[index], 
                    currentPage.WordList[index]);
            }

            // Add additional words.
            for (; index < currentPage.WordList.Count; index++)
            {
                await currentDB.AddWordAsync(currentPage.WordList[index]);
            }

            Page.UpdatePage(StartupPages.LanguageHomePage());
        }

        #region Helpers - GetWordFromWordInputPage; ShowWordOnWordInputPage(Word); UpdateWordList; UpdateCharacteristics

        /* Helper function to get the values 
         * from a WordInputPage and use them 
         * to create a Word.
         */
        private static Word GetWordFromWordInputPage()
        {
            FlowLayoutPanel panel = currentPage.Panel;

            // Returns null if control could not be found 
            // (indicating this method was called on a non-WordInputPage)
            // or if the term was blank.
            Control[] term = panel.Controls.Find("txtTermInput", true);
            if (term.Length == 0 || term[0].Text.Trim().Equals(""))
            {
                return null;
            }

            PartOfSpeech? part = panel.Controls.Find("cboPartOfSpeech", true)[0].Text.ToPartOfSpeech();
            bool special = ((CheckBox)panel.Controls.Find("chkSpecial", true)[0]).Checked;
            Word word = new Word(term[0].Text, part, special);

            foreach (string def in ((TextBox)panel.Controls.
                Find("txtDefinitions", true)[0]).Lines)
            {
                word.AddDefinition(def);
            }

            // Returns null if word does not contain definitions.
            if (word.Definitions.Count == 0)
            {
                return null;
            }

            foreach (string characteristic in ((Controls.MultiChoice)panel.Controls.
                Find("choCharacteristics", true)[0]).ChosenValues)
            {
                word.AddCharacteristic(characteristic);
            }

            string[] types = ((Controls.MultiPrompt)panel.Controls.
                Find("prmAlternativeSpellings", true)[0]).Prompts;
            string[] values = ((Controls.MultiPrompt)panel.Controls.
                Find("prmAlternativeSpellings", true)[0]).Answers;
            for (int index = 0; index < types.Length && index < values.Length; index++)
            {
                word.AddAlternativeSpelling(types[index], values[index]);
            }

            return word;
        }

        /* Helper function to put the passed word
         * onto a WordInputPage.
         */
        private static void ShowWordOnWordInputPage(Word word)
        {
            FlowLayoutPanel panel = currentPage.Panel;

            // Returns early if control could not be found 
            // (indicating this method was called on a non-WordInputPage)
            // or if a null Word was passed.
            Control[] term = panel.Controls.Find("txtTermInput", true);
            if (term.Length == 0 || word is null)
            {
                return;
            }

            term[0].Text = word.Term;
            ((ComboBox)panel.Controls.Find("cboPartOfSpeech", true)[0]).Text = word.Part.ToStringName();
            ((CheckBox)panel.Controls.Find("chkSpecial", true)[0]).Checked = word.Special;

            TextBox definitions = ((TextBox)panel.Controls.Find("txtDefinitions", true)[0]);
            definitions.Text = "";
            foreach (string def in word.Definitions)
            {
                definitions.Text += def + Environment.NewLine;
            }

            Controls.MultiChoice characteristics = ((Controls.MultiChoice)panel.Controls.
                Find("choCharacteristics", true)[0]);
            // Reset control.
            for (int index = 0; index < characteristics.ChosenValues.Count; index++)
            {
                characteristics.RemoveChosenValue(index--);
            }
            
            foreach (string str in word.Characteristics)
            {
                characteristics.AddChosenValue(str);
            }
            foreach (string str in
                ((ListBox)panel.Controls.Find("lstDefaultCharacteristics", true)[0]).Items)
            {
                if (!characteristics.ChosenValues.Contains(str))
                {
                    characteristics.AddChosenValue(str);
                }
            }

            Controls.MultiPrompt altSpellings = ((Controls.MultiPrompt)panel.Controls.
                Find("prmAlternativeSpellings", true)[0]);
            altSpellings.Controls.Find("txtAnswer", true)[0].Text = "";
            for (int index = 0; index < altSpellings.Prompts.Count(); index++)
            {
                // Word may not have all alternative spellings.
                // If the alternative spelling exists and 
                // prompt displayed on screen matches this spelling type,
                // update the corresponding answer textbox.
                try
                {
                    altSpellings.Answers[index] = word.AlternativeSpellings[altSpellings.Prompts[index]];
                    if (altSpellings.Prompts[index].Equals(altSpellings.Controls.
                        Find("ddlPrompts", true)[0].Text))
                    {
                        altSpellings.Controls.
                            Find("txtAnswer", true)[0].Text = altSpellings.Answers[index];
                    }
                }
                catch (KeyNotFoundException)
                {
                    altSpellings.Answers[index] = "";
                }
            }
        }

        /* Helper function to update the Page's WordList.
         * Returns false if the Page returns null Word.
         */
        private static bool UpdateWordList()
        {
            Word word = GetWordFromWordInputPage();

            if (word is null)
            {
                return false;
            }

            // Either add word to end of list
            // or update current item in list to word.
            if (currentPage.WordListIndex >= currentPage.WordList.Count)
            {
                currentPage.WordList.Add(word);
            }
            else
            {
                currentPage.WordList[(int)currentPage.WordListIndex] = word;
            }

            return true;
        }

        /* Helper function to add new characteristics added by the user
         * to the database's list of characteristics, also updating
         * the choices in the characteristics multichoice.
         */ 
        private static async Task UpdateCharacteristics()
        {
            Controls.MultiChoice characteristics = ((Controls.MultiChoice)currentPage.
                Panel.Controls.Find("choCharacteristics", true)[0]);
            int count = 0;

            // If this word contains any characteristics not in database,
            // add them to database.
            foreach (string val in characteristics.ChosenValues)
            {
                if (!characteristics.Choices.Contains(val))
                {
                    await currentDB.AddCharacteristicAsync(val);
                    count++;
                }
            }

            // If any characteristics were added, 
            // update multichoice to include them as choices.
            if (count > 0)
            {
                characteristics.Choices = currentDB.Characteristics.
                    ToListWithDirectories().ToArray();
                characteristics.Invalidate();
            }
        }

        #endregion

        #endregion

        #region Search filters for dictionary page creation - SearchFiltersPage(NextPageCode)

        /* Creates a new page used to get search filters from a user
         * prior to searching a dictionary.
         * Parameter code determines what page to load after the search.
         * After searching, a user might want to show search results,
         * edit words, or remove words.
         */
        public static Page SearchFiltersPage(NextPageCode code)
        {
            Page page = new Page();

            // Label - prompt
            Label prompt = new Label()
            {
                Name = "lblPrompt",
                Text = "Search Filters",
                AutoSize = false,
                TextAlign = ContentAlignment.MiddleCenter
            };
            prompt.SetDefaultAppearance();
            prompt.Resize += SearchPromptResize;
            page.AddControl(prompt);

            /* Helper to set colors, 
             * add resize event handler,
             * and add control to page.
             */ 
            void SetCommandInput(Control c)
            {
                c.SetDefaultAppearance();
                c.BackColor = currentPalette.BackAccent;
                c.Resize += CommandInputResize;
                page.AddControl(c);
            }

            // Combobox - search function selection
            ComboBox searchFunction = new ComboBox()
            {
                Name = "cboSearchFunction",
                DropDownStyle = ComboBoxStyle.DropDownList
            };
            searchFunction.Items.Add("");
            foreach (string str in SearchCommand.LegalSearchFunctions)
            {
                searchFunction.Items.Add(str);
            }
            SetCommandInput(searchFunction);

            // Combobox - field name selection
            ComboBox fieldName = new ComboBox()
            {
                Name = "cboFieldName",
                DropDownStyle = ComboBoxStyle.DropDownList
            };
            for (int index = 1; index < SEARCH_COMMAND_FIELDS.Count; index++)
            {
                fieldName.Items.Add(SEARCH_COMMAND_FIELDS[index]);
            }
            foreach (string str in currentDB.AlternativeSpellingTypes)
            {
                fieldName.Items.Add(str);
            }
            SetCommandInput(fieldName);

            // Combobox - comparison type selection
            ComboBox comparisonType = new ComboBox()
            {
                Name = "cboComparisonType",
                DropDownStyle = ComboBoxStyle.DropDownList
            };
            foreach (string str in SearchCommand.LegalComparisonTypes)
            {
                comparisonType.Items.Add(str);
            }
            SetCommandInput(comparisonType);

            // Textbox - search value selection
            TextBox searchValue = new TextBox()
            {
                Name = "txtSearchValue"
            };
            SetCommandInput(searchValue);
            searchValue.KeyUp += TextBoxEnter;

            // Button - enter search command
            Button submitCommand = new Button()
            {
                Name = "btnSubmitCommand",
                Text = "Submit",
                FlatStyle = FlatStyle.Flat
            };
            SetCommandInput(submitCommand);
            submitCommand.Padding = new Padding(0); // Makes button text visible
            submitCommand.Click += EnterSearchCommand;

            // Listbox - search commands list
            ListBox searchCommands = new ListBox()
            {
                Name = "lstSearchCommands",
                ScrollAlwaysVisible = true
            };
            searchCommands.SetDefaultAppearance();
            searchCommands.Resize += CommandStorageResize;
            searchCommands.DoubleClick += RemoveSearchCommand;
            page.AddControl(searchCommands);

            // Button - next page
            Button nextPage = new Button()
            {
                Name = "btnNextPage"
            };
            switch (code)
            {
                case NextPageCode.EditDictionary:
                    nextPage.Text = "Edit Terms";
                    break;
                case NextPageCode.RemoveFromDictionary:
                    nextPage.Text = "View Terms";
                    break;
                case NextPageCode.SearchDictionary:
                    nextPage.Text = "Search Database";
                    break;
            }
            nextPage.SetDefaultAppearance();
            nextPage.BackColor = currentPalette.BackAccent;
            nextPage.Resize += SearchButtonResize;
            nextPage.Click += SearchButtonClick;
            page.AddControl(nextPage);

            return page;
        }

        /* Event handler to resize
         * label that displays name of SearchFiltersPage.
         */ 
        public static void SearchPromptResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Label size change.
            int width = panel.Width - (DEFAULT_MARGIN.All * 3);
            int height = (panel.Height / 5) - (DEFAULT_MARGIN.All * 4);
            control.Size = new Size(width, height);

            // Font size change.
            int fontSizeWidth = width / 40;
            int fontSizeHeight = height / 2;
            control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);
        }

        /* Event handler to resize input controls.
         */ 
        public static void CommandInputResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel;

            // Not sure why this try/catch block is necessary,
            // especially since this seems to be the only instance
            // of resizing causing issues.
            try
            {
                panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

                // Label size change.
                int width = (panel.Width / 5) - (DEFAULT_MARGIN.All * 2);
                int height = panel.Height / 25;
                control.Size = new Size(width, height);

                // Font size change.
                int fontSizeWidth = width / 15;
                int fontSizeHeight = (int)(height / 2.5);
                control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);
            }
            catch (Exception ex) when (ex is NullReferenceException || ex is ArgumentException)
            {
                return;
            }
        }

        /* Event handler to resize listbox 
         * used to store search commands.
         */ 
        public static void CommandStorageResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Label size change.
            int width = panel.Width - (DEFAULT_MARGIN.All * 3);
            int height = (int)(panel.Height / 1.875);
            control.Size = new Size(width, height);

            // Font size change.
            int fontSizeWidth = width / 33;
            int fontSizeHeight = height / 12;
            control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);
        }

        /* Event handler to resize search button.
         */ 
        public static void SearchButtonResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Label size change.
            int width = panel.Width - (DEFAULT_MARGIN.All * 3);
            int height = (panel.Height / 5) - (DEFAULT_MARGIN.All * 3);
            control.Size = new Size(width, height);

            // Font size change.
            int fontSizeWidth = width / 50;
            int fontSizeHeight = (int)(height / 2.5);
            control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);
        }

        /* Event handler to execute when a key is entered
         * in the search value input textbox.
         * If the key is the enter key, the search command is submitted.
         */ 
        public static void TextBoxEnter(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                EnterSearchCommand(sender, e);
            }
        }

        /* Event handler to execute when a search command is entered.
         * Adds search command to listbox, if it is a valid search command.
         * Meant to be called either by a button click 
         * or hitting enter on textbox.
         */ 
        public static void EnterSearchCommand(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];
            string findControlText(string name)
            {
                return panel.Controls.Find(name, true)?[0]?.Text;
            }
            
            // Input values
            string searchFunction = findControlText("cboSearchFunction");
            string fieldName = findControlText("cboFieldName");
            string comparisonType = findControlText("cboComparisonType");
            string searchValue = findControlText("txtSearchValue");

            // Add search command to box, 
            // if it is a valid command & it doesn't already exist.
            if (SearchCommand.IsLegalSearchCommand(
                "".Equals(searchFunction) ? null : searchFunction, 
                fieldName, comparisonType, searchValue))
            {
                ListBox lstSearchCommand = (ListBox)panel.Controls.Find("lstSearchCommands", true)[0];
                string newCommand = searchFunction + COMMAND_DELIM + fieldName + COMMAND_DELIM +
                                           comparisonType + COMMAND_DELIM + searchValue;
                if (!lstSearchCommand.Items.Contains(newCommand))
                {
                    lstSearchCommand.Items.Add(newCommand);
                }

            }
        }

        /* Event handler to execute when a search command is removed.
         * Removes search command from listbox.
         * Meant to be called by double click on listbox item.
         */
        public static void RemoveSearchCommand(object sender, EventArgs e)
        {
            ListBox control = (ListBox)sender;
            try
            {
                control.Items.RemoveAt(control.SelectedIndex);
            }
            catch (ArgumentOutOfRangeException) { }
        }

        /* Event handler to execute when search button is clicked.
         * Outcome depends on button text:
         *  - "Search" -> show search results screen
         *  - "Edit" -> show word input page with search results
         *  - "Remove" -> show remove results screen
         */ 
        public static async void SearchButtonClick(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Get listbox items, convert to search commands, and run query.
            ListBox lstSearchCommand = (ListBox)panel.Controls.Find("lstSearchCommands", true)[0];
            List<SearchCommand> commands = new List<SearchCommand>();
            foreach (string item in lstSearchCommand.Items)
            {
                List<string> vals = item.Split(COMMAND_DELIM).ToList();

                if (vals[0].Equals(""))
                {
                    vals[0] = null;
                }

                commands.Add(SearchCommand.CreateSearchCommand(vals[0], vals[1], vals[2], vals[3]));
            }

            // Don't go to search results page if there are no search commands.
            if (commands.Count == 0)
            {
                return;
            }

            List<Word> words = await currentDB.SearchAsync(commands);

            switch (control.Text)
            {
                case "Edit Terms": // Edit
                    Page.UpdatePage(WordInputPage(words));
                    break;
                case "View Terms": // Remove From
                    Page.UpdatePage(RemoveTermsPage(words));
                    break;
                case "Search Database": // Search
                    Page.UpdatePage(SearchResultsPage(words));
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region Shows results of a database search - SearchResultsPage(List<Word>)

        /* Creates a new page to show the words returned from a search query.
         * Parameter words consists of the query results.
         */ 
        public static Page SearchResultsPage(List<Word> words)
        {
            Page page = new Page();

            // Textbox - displays search results
            TextBox results = new TextBox()
            {
                Name = "txtResults",
                Multiline = true,
                ScrollBars = ScrollBars.Vertical,
                ReadOnly = true
            };
            AddWordsToTextbox(results, words);
            results.SetDefaultAppearance();
            results.BackColor = currentPalette.BackAccent;
            results.Resize += SearchResultsResize;
            page.AddControl(results);

            // Button - goes to search filters page
            Button newQuery = new Button()
            {
                Name = "btnNewQuery",
                Text = "New Search Query",
                FlatStyle = FlatStyle.Flat,
            };
            newQuery.SetDefaultAppearance();
            newQuery.BackColor = currentPalette.BackAccent;
            newQuery.Resize += ReturnButtonResize;
            newQuery.Click += NewQueryButtonClick;
            page.AddControl(newQuery);

            // Button - goes back to language home page
            Button homeButton = new Button()
            {
                Name = "btnHome",
                Text = "Return Home",
                FlatStyle = FlatStyle.Flat
            };
            homeButton.SetDefaultAppearance();
            homeButton.BackColor = currentPalette.BackAccent;
            homeButton.Resize += ReturnButtonResize;
            homeButton.Click += HomeButtonClick;
            page.AddControl(homeButton);

            return page;
        }

        /* Helper function to convert each word in words to a string
         * and add them to box, properly formatting everything.
         */ 
        private static void AddWordsToTextbox(TextBox box, IEnumerable<Word> words)
        {
            if (box is null || words is null)
            {
                return;
            }

            box.Text = "";

            // Add all non-null words to the textbox.
            foreach (Word word in words)
            {
                string term = FormatSearchResult(word);

                if (term != null)
                {
                    box.Text += term + Environment.NewLine + Environment.NewLine;
                }
            }
        }

        /* Event handler to resize the textbox that displays search results.
         */ 
        public static void SearchResultsResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Textbox size change.
            int width = panel.Width - (DEFAULT_MARGIN.All * 3);
            int height = (panel.Height ) - (DEFAULT_MARGIN.All * 10);
            control.Size = new Size(width, height);

            // Font size change.
            int fontSizeWidth = width / 67;
            int fontSizeHeight = height / 30;
            control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);
        }

        /* Event handler to resize the buttons on the search results page.
         * Resizes one button to half the page width.
         */ 
        public static void ReturnButtonResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Button size change.
            int width = panel.Width / 2 - (int)(DEFAULT_MARGIN.All * 2.5);
            int height = (panel.Height / 5) - (DEFAULT_MARGIN.All * 3);
            control.Size = new Size(width, height);

            // Font size change.
            int fontSizeWidth = width / 32;
            int fontSizeHeight = height / 5;
            control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);
        }

        /* Event handler to execute when "New Search Query" button is clicked.
         * Takes user to a new SearchFiltersPage to create a new query.
         */ 
        public static void NewQueryButtonClick(object sender, EventArgs e)
        {
            Page.UpdatePage(SearchFiltersPage(NextPageCode.SearchDictionary));
        }

        /* Event handler to execute when "Return Home" button is clicked.
         * Takes user back to langauge home page.
         */
        public static void HomeButtonClick(object sender, EventArgs e)
        {
            Page.UpdatePage(StartupPages.LanguageHomePage());
        }

        #endregion

        #region Shows words with option to remove any/all words from database - RemoveTermsPage(List<Word>)

        /* Creates a new page to show words that can be removed
         * and execute a removal if requested.
         * Parameter words consists of the possible words to be removed.
         */
        public static Page RemoveTermsPage(List<Word> words)
        {
            Page page = new Page(words);

            // Listbox - displays words that match search query,
            // which are meant to be options to be removed from database.
            ListBox wordsBox = new ListBox()
            {
                Name = "lstWords",
                ScrollAlwaysVisible = true,
                SelectionMode = SelectionMode.MultiExtended
            };
            AddWordsToListbox(wordsBox, words);
            wordsBox.SetDefaultAppearance();
            wordsBox.BackColor = currentPalette.BackAccent;
            wordsBox.Resize += SearchResultsResize;
            page.AddControl(wordsBox);

            // Button - Return to language home page without any deletion.
            Button cancel = new Button()
            {
                Name = "btnCancel",
                Text = "Cancel Removal",
                FlatStyle = FlatStyle.Flat
            };
            cancel.SetDefaultAppearance();
            cancel.BackColor = currentPalette.BackAccent;
            cancel.Resize += ReturnButtonResize;
            cancel.Click += CancelButtonClick;
            page.AddControl(cancel);

            // Button - Display messagebox confirming deletion, 
            // perform delection, then return to language home page.
            Button removeTerms = new Button()
            {
                Name = "btnRemove",
                Text = "Remove Selected Terms",
                FlatStyle = FlatStyle.Flat
            };
            removeTerms.SetDefaultAppearance();
            removeTerms.BackColor = currentPalette.BackAccent;
            removeTerms.Resize += ReturnButtonResize;
            removeTerms.Click += RemoveButtonClick;
            page.AddControl(removeTerms);

            return page;
        }

        /* Helper function to convert each word in words to a string
         * and add them to box, properly formatting everything.
         */
        private static void AddWordsToListbox(ListBox box, IEnumerable<Word> words)
        {
            if (box is null || words is null)
            {
                return;
            }

            box.Items.Clear();

            // Add all non-null words to the textbox.
            foreach (Word word in words)
            {
                string term = FormatSearchResult(word);

                if (term != null)
                {
                    // Yes this is ugly. No I don't care.
                    string[] parts = term.Split('\n');
                    int main = 0;
                    int definitions = word.AlternativeSpellings.Count > 0 ? 2 : 1;
                    string formatted = parts[main].Substring(0, parts[main].Length - 1) + " - " + 
                                       parts[definitions].Substring(0, parts[definitions].Length - 1);
                    box.Items.Add(formatted);
                }
            }
        }

        /* Event handler to return to language home page
         * without deleting terms.
         */
        public static void CancelButtonClick(object sender, EventArgs e)
        {
            Page.UpdatePage(StartupPages.LanguageHomePage());
        }

        /* Event handler to remove terms from database.
         * First confirms that user wants to delete selected terms,
         * then deletes terms from database,
         * finally returns to language home page.
         */ 
        public static async void RemoveButtonClick(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to delete the selected terms?", "Vok",
                                                  MessageBoxButtons.YesNo, MessageBoxIcon.Warning,
                                                  MessageBoxDefaultButton.Button2);
            if (result.Equals(DialogResult.No))
            {
                return;
            }

            Control panel = ((Control)sender).FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];
            ListBox lstWords = (ListBox)panel.Controls.Find("lstWords", true)[0];

            // Get words to remove -
            // indices in page's WordList should correspond
            // with order they were placed in listbox's items.
            List<Word> remove = new List<Word>();
            foreach (int index in lstWords.SelectedIndices)
            {
                remove.Add(currentPage.WordList[index]);
            }

            await currentDB.RemoveWordsAsync(remove);

            Page.UpdatePage(StartupPages.LanguageHomePage());
        }

        #endregion
    }
}
