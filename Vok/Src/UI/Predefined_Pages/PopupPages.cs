﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using Vok.Src.Database;
using static Vok.Src.Common.Common;

namespace Vok.Src.UI.Predefined_Pages
{
    /* Sets up various Pages for Vok.
     * These pages are meant to be displayed on a popup window:
     *  - HelpPage()
     *  - OptionsPage()
     *  - EditDefaultCharacteristicsPage()
     */
    static class PopupPages
    {
        #region Help page creation - HelpPage()

        /* Creates a new page to display help topics.
         */ 
        public static Page HelpPage()
        {
            Page page = new Page();

            // Non-visible multiprompt used to store 
            // help topics and corresponding information.
            Controls.MultiPrompt storage = new Controls.MultiPrompt()
            {
                Name = "prmHelpStorage",
                Visible = false
            };
            HelpTopics.SetUpHelpTopics(storage);
            page.AddControl(storage);

            // Combobox - select help topic.
            ComboBox topics = new ComboBox()
            {
                Name = "cboTopicChoice",
                DropDownStyle = ComboBoxStyle.DropDownList,
                FlatStyle = FlatStyle.Flat,
            };
            foreach (string str in storage.Prompts)
            {
                topics.Items.Add(str);
            }
            topics.SelectedIndex = 0;
            topics.SetDefaultAppearance();
            topics.BackColor = currentPalette.BackAccent;
            topics.Resize += ResizeTopicsBox;
            topics.SelectedIndexChanged += ChangeHelpTopic;
            page.AddControl(topics);

            // Textbox - read information corresponding to 
            // selected help topic.
            TextBox help = new TextBox()
            {
                Name = "txtTopicDetails",
                Multiline = true,
                WordWrap = true,
                ScrollBars = ScrollBars.Vertical,
                ReadOnly = true,
                Text = storage.Answers[0]
            };
            help.SetDefaultAppearance();
            help.BackColor = currentPalette.BackAccent;
            help.Resize += ResizeTextBox;
            page.AddControl(help);

            return page;
        }

        /* Event handler to resize the help page's combobox,
         * used to select a help topic.
         */ 
        public static void ResizeTopicsBox(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel;
            // Don't know why this is necessary,
            // but Vok doesn't seem happy without it.
            try
            {
                panel = control.FindForm().Controls[0];
            }
            catch (NullReferenceException)
            {
                return;
            }

            // Label size change.
            int width = panel.Width - (DEFAULT_MARGIN.All * 4);
            int height = panel.Height / 7 - (DEFAULT_MARGIN.All * 4);
            control.Size = new Size(width, height);

            // Font size change.
            int fontSizeWidth = width / 40;
            int fontSizeHeight = height / 2;
            control.Font = new Font(control.Font.Name,
                fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);
        }

        /* Event handler to resize the help page's textbox,
         * used to display the information corresponding to a topic.
         */ 
        public static void ResizeTextBox(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls[0];

            // Label size change.
            int width = panel.Width - (DEFAULT_MARGIN.All * 4);
            int height = (int)(panel.Height / 1.5);
            control.Size = new Size(width, height);

            // Font size change.
            int fontSizeWidth = width / 50;
            int fontSizeHeight = height / 24;
            control.Font = new Font(control.Font.Name,
                fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);
        }

        /* Event handler to change the information displayed in the textbox
         * when the topic in the combobox is changed.
         */ 
        public static void ChangeHelpTopic(object sender, EventArgs e)
        {
            ComboBox topics = (ComboBox)sender;
            Control panel = topics.FindForm().Controls[0];

            Controls.MultiPrompt storage = (Controls.MultiPrompt)panel.Controls.Find("prmHelpStorage", true)[0];
            TextBox help = (TextBox)panel.Controls.Find("txtTopicDetails", true)[0];

            int index = topics.SelectedIndex;
            help.Text = storage.Answers[index];
        }

        #endregion

        #region Options page creation - OptionsPage()

        /* Creates a Page that serves as an options page.
         */ 
        public static Page OptionsPage()
        {
            Page page = new Page();

            // CheckBox - Switch between light and dark mode.
            CheckBox darkMode = new CheckBox()
            {
                Name = "chkDarkMode",
                Text = "Dark Mode Enabled",
                AutoSize = false,
                Checked = currentPalette.Equals(DARK_MODE)
            };
            darkMode.CheckedChanged += CheckDarkMode;
            darkMode.Resize += CheckBoxResize;
            page.AddControl(darkMode);

            // CheckBox - Switch between left-to-right
            // and right-to-left input.
            CheckBox rightToLeft = new CheckBox()
            {
                Name = "chkRightToLeft",
                Text = "Right-to-Left Input",
                AutoSize = false,
                Checked = !LeftToRight
            };
            rightToLeft.CheckedChanged += CheckRightToLeft;
            rightToLeft.Resize += CheckBoxResize;
            page.AddControl(rightToLeft);

            return page;
        }

        /* Event handler to resize the Option page's checkbox.
         */ 
        public static void CheckBoxResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control optionsPanel = control.FindForm().Controls[0];

            // Checkbox size change.
            int width = optionsPanel.Width - (DEFAULT_MARGIN.All * 3);
            int height = (optionsPanel.Height / 6);
            control.Size = new Size(width, height);

            // Font size change.
            int fontSizeWidth = width / 25;
            int fontSizeHeight = height / 3;
            control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);
        }

        /* Event handler to change color palette to/from dark mode
         * based on checkbox status.
         */ 
        public static void CheckDarkMode(object sender, EventArgs e)
        {
            Common.ColorPalette.UpdateColorPalette(currentPalette.Equals(DARK_MODE) ? LIGHT_MODE : DARK_MODE);
        }

        /* Event handler to change default text input 
         * to/from right-to-left input based on checkbox status.
         */ 
        public static void CheckRightToLeft(object sender, EventArgs e)
        {
            LeftToRight = !((CheckBox)sender).Checked;
        }

        #endregion

        #region Edit default characteristics - EditDefaultCharacteristicsPage()

        /* Creates a new page used to edit the default characteristics
         * added to a word in a word-input page.
         */
        public static Page EditDefaultCharacteristicsPage()
        {
            Page page = new Page();

            // Label - prompt
            Label prompt = new Label()
            {
                Name = "lblPrompt",
                Text = "Default Characteristics",
                AutoSize = false,
                TextAlign = ContentAlignment.MiddleCenter
            };
            prompt.SetDefaultAppearance();
            prompt.Resize += PromptResize;
            page.AddControl(prompt);

            // Multi-choice - characteristics input
            Controls.MultiChoice characteristics = new Controls.MultiChoice()
            {
                Name = "choCharacteristics",
                Choices = currentDB.Characteristics.ToListWithDirectories().ToArray()
            };
            foreach (Control c in characteristics.Controls)
            {
                c.SetDefaultAppearance();
            }
            characteristics.Resize += CharacteristicsResize;
            page.AddControl(characteristics);

            return page;
        }

        /* Event handler to add current default characteristics
         * to the multichoice box's chosen values.
         */
        public static void CharacteristicsPageLoad(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Controls.MultiChoice characteristics = (Controls.MultiChoice)control.FindForm().
                Controls[0].Controls.Find("choCharacteristics", true)[0];

            // Ensures this code only works when WordInputPage is loaded.
            ListBox currentDefault;
            try
            {
                currentDefault = (ListBox)currentPage.Panel.Controls.
                    Find("lstDefaultCharacteristics", true)[0];
            }
            catch (IndexOutOfRangeException)
            {
                return;
            }

            foreach (string str in currentDefault.Items)
            {
                characteristics.AddChosenValue(str);
            }
        }

        /* Event handler to resize the prompt label.
         */
        public static void PromptResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls[0];

            // Label size change.
            int width = panel.Width - (DEFAULT_MARGIN.All * 3);
            int height = panel.Height / 7;
            control.Size = new Size(width, height);

            // Font size change.
            int fontSizeWidth = width / 32;
            int fontSizeHeight = height / 3;
            control.Font = new Font(control.Font.Name,
                fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);
        }

        /* Event handler to resize the characteristics input multichoice box.
         */
        public static void CharacteristicsResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls[0];

            // Label size change.
            int width = panel.Width - (DEFAULT_MARGIN.All * 3);
            int height = panel.Height / 2 + DEFAULT_MARGIN.All;
            control.Size = new Size(width, height);

            // Font size change.
            int fontSizeWidth = width / 25;
            int fontSizeHeight = height / 9;
            Font resizedFont = new Font(control.Font.Name,
                fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);
            foreach (Control c in control.Controls)
            {
                c.Font = resizedFont;
            }
        }

        /* Event handler to update default characteristics stored
         * in WordInputPage listbox.
         * For the current term being edited on WordInputPage:
         *  - remove any characteristics removed from default list
         *  - add new characteristics added to default list
         */
        public static void CharacteristicsPageClose(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Controls.MultiChoice newCharacteristics = (Controls.MultiChoice)control.FindForm().
                Controls[0].Controls.Find("choCharacteristics", true)[0];

            // Ensures this code only works when WordInputPage is loaded.
            ListBox currentDefault;
            Controls.MultiChoice currentCharacteristics;
            try
            {
                currentDefault = (ListBox)currentPage.Panel.Controls.
                    Find("lstDefaultCharacteristics", true)[0];
                currentCharacteristics = (Controls.MultiChoice)currentPage.
                    Panel.Controls.Find("choCharacteristics", true)[0];
            }
            catch (IndexOutOfRangeException)
            {
                return;
            }

            // Gets previous values of default characteristics.
            string[] previousItems = currentDefault.Items.Cast<string>().ToArray();

            // Reset list of default characteristics in the page.
            currentDefault.Items.Clear();
            foreach (string item in newCharacteristics.ChosenValues)
            {
                currentDefault.Items.Add(item);
            }

            // Remove characteristics that formerly were default characteristics
            // but no longer are.           
            for (int index = 0; index < currentCharacteristics.ChosenValues.Count; index++)
            {
                bool isPreviousDefault = previousItems.
                    Contains(currentCharacteristics.ChosenValues[index]);
                bool isNewDefault = newCharacteristics.ChosenValues.
                    Contains(currentCharacteristics.ChosenValues[index]);
                if (isPreviousDefault && !isNewDefault)
                {
                    currentCharacteristics.RemoveChosenValue(index);
                    index--;
                }
            }

            // Add characteristics that are new additions to default characteristics.
            for (int index = 0; index < newCharacteristics.ChosenValues.Count; index++)
            {
                bool isPreviousDefault = previousItems.
                    Contains(newCharacteristics.ChosenValues[index]);
                bool isCurrentDefault = currentCharacteristics.ChosenValues.
                    Contains(newCharacteristics.ChosenValues[index]);
                if (!isPreviousDefault && !isCurrentDefault)
                {
                    currentCharacteristics.AddChosenValue(newCharacteristics.ChosenValues[index]);
                }
            }
        }

        #endregion
    }
}
