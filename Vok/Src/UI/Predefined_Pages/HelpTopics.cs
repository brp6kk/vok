﻿using System.Collections.Generic;
using static Vok.Src.Common.Common;

namespace Vok.Src.UI.Predefined_Pages
{
    /* Used by PopupPages.HelpPage to set up help topics.
     * Separated into a separate file due to large quantity of information.
     */ 
    static class HelpTopics
    {
        /* Helper function to add help topics and 
         * corresponding information to a MultiPrompt.
         * Used in PopupPages.cs, refactored into separate file 
         * because of large amount of information.
         */ 
        public static void SetUpHelpTopics(Controls.MultiPrompt storage)
        {
            storage.Prompts = new string[helpTopics.Count];
            storage.Answers = new string[helpTopics.Count];
            int index = 0;

            foreach (KeyValuePair<string, string> entry in helpTopics)
            {
                storage.Prompts[index] = entry.Key;
                storage.Answers[index] = entry.Value;
                index++;
            }
        }

        /* Represents topics and corresponding information;
         * used to simplify SetUpHelpTopics implementation.
         */ 
        private static Dictionary<string, string> helpTopics = new Dictionary<string, string>
        {
            ["Vok Basics"] =
                "Vok is a vocabulary studying aid meant to be used to assist in learning the terms of a foreign language. " +
                "This software can be used to view and edit a database of vocabulary terms. " +
                "Additionally, you can take tests to review vocabulary." + ENTER + ENTER + ENTER +
                "At the top of Vok's window is a menu consisting of four buttons:" + ENTER + ENTER +
                "* Home - return to the home page seen at program startup" + ENTER + ENTER +
                "* Options - open a window to edit options" + ENTER +
                "  * More information can be found under the \"Options\" topic" + ENTER + ENTER +
                "* Help - opens a window to view help topics" + ENTER +
                "  * Change help topics by selecting a new one via the drop - down list" + ENTER + ENTER +
                "* Quit - closes Vok",
            ["Home Page"] = 
                "When first starting Vok, you are greeted by the home page. There, " +
                "you can choose which language to study. Any languages that you have " +
                "already set up will appear at the top of the language selection box. " +
                "Simply double-click on the language that you wish to study to move on " +
                "to its language home page." + ENTER + ENTER +
                "Additionally, there is a \"New Language\" option for if you wish " +
                "to begin studying vocabulary for a new language. " +
                "Double-click on this item to be taken to the new language page.",
            ["New Language Page"] =
                "This page sets up a new language with no vocabulary terms." +
                ENTER + ENTER +
                "The textbox labeled \"Language\" is simply used to input the name of the language." +
                ENTER + ENTER +
                "The textbox labeled \"Alternative Spelling Types\" is used " +
                "to store the names of any alternative spelling types associated " +
                "with the language. If you do not want to have any " +
                "alternative spelling types associated with this language, " +
                "simply leave the box blank. If you want to include more " +
                "than one type, separate each type onto different lines." +
                ENTER + ENTER + 
                "An example of page usage: you decide to set up the language Chinese." +
                " You type \"Chinese\" into the Language textbox. You type " +
                "\"Pinyin\", \"Traditional\", and \"Zhuyin\" into the " +
                "Alternative Spelling Types textbox, pressing the Enter key in between each entry."
                + ENTER + ENTER +
                "After setup, click the \"Add Terms\" button to be taken" +
                " to a new page to begin adding vocabulary.",
            ["Language Home Page"] =
                "This page is a gateway to different mechanisms of interacting " +
                "with the vocabulary of a language. The language name is listed " +
                "at the top of the page. Underneath are two groups of buttons: " +
                "one that contains buttons to edit the database and " +
                "one to test knowledge of vocabulary." + ENTER + ENTER + ENTER +
                "There are four options regarding editing the vocabulary database:" + ENTER + ENTER +
                "* \"Add To\" - takes you to a page where you can enter new " +
                "vocabulary terms; more information can be found under the " +
                "\"Add To Database\" topic" + ENTER + ENTER + 
                "* \"Search\" - takes you to a page where you can specify " +
                "search parameters. After entering some, you can view the results " +
                "of the search; more information can be found under the " +
                "\"Search Database\" topic" + ENTER + ENTER + 
                "* \"Edit\" - takes you to a page where you can specify " +
                "search parameters. After entering some, you can edit the " +
                "results of the search; more information can be found under the " +
                "\"Edit Database\" topic" + ENTER + ENTER + 
                "* \"Remove From\" - takes you to a page where you can " +
                "specify search parameters. After entering some, you can view " +
                "the results and select any that you wish to delete from " +
                "the vocabulary set; more information can be found under the " +
                "\"Remove From Database\" topic" + ENTER + ENTER + ENTER +
                "There are three options regarding tests to study vocabulary:" + ENTER + ENTER +
                "* \"Flashcards\"" + ENTER + ENTER +
                "* \"Flashcards Draw\"" + ENTER + ENTER +
                "* \"Fill in the Blanks\"" + ENTER + ENTER +
                "All three options take you to a page where you can set up the tests. " +
                "More information on each test type can be found under the \"Flashcards Tests,\" " +
                "\"Flashcards Draw Tests,\" and \"Fill in the Blanks Tests\" topics. " +
                "More information on test setup can be found under the \"Test Setup\" topic.",
        /* Editing a database */
            ["Add To Database"] =
                "This page allows you to define a word. A word consists of several components:" + ENTER + ENTER +
                "* Term - the actual word" + ENTER + ENTER +
                "* Special - a value that indicates whether or not this word " +
                "should have extra focus when studied" + ENTER + ENTER +
                "* If the language has at least one alternative spelling type, " +
                "the word itself can contain alternative spellings" + ENTER + ENTER +
                "* Part of Speech - the syntactic category the word falls under" + ENTER + ENTER +
                "* Definitions - the word's meanings" + ENTER +
                "  * If a word has multiple definitions, these should be separated " +
                "onto different lines" + ENTER + ENTER +
                "* Characteristics - special tags used to group related words." + ENTER + ENTER + ENTER +
                "At the bottom of the page are four buttons:" + ENTER + ENTER + 
                "* Previous Term - If the current word you are adding is not the first, " +
                "this button returns to the word you were previously editing" + ENTER + ENTER +
                "* Next Term - will either: " + ENTER +
                "  * create a new blank term, or" + ENTER +
                "  * go to the next term if the Previous Term button " +
                "had prior returned to a different word" + ENTER + ENTER +
                "* Edit Default Characteristics - opens the Edit Default Characteristics Window" + ENTER +
                "  * More information can be found under the " +
                "\"Edit Default Characteristics Window\" topic" + ENTER + ENTER +
                "* Finish - saves all of the terms you added to the language's database " +
                "and returns you to the language home page",
            ["Search Database"] =
                "You begin a search by specifying search filters. " +
                "More information can be found under the \"Editing Search Filters\" topic." + ENTER + ENTER +
                "After adding search filters and clicking \"Search Database\", " +
                "the words that match the specified criteria are displayed on screen. " +
                "All of the information about each word is displayed." + ENTER + ENTER +
                "At the bottom of the screen are two buttons:" + ENTER +
                "* New Search Query - takes you to a new screen to add search criteria for a new search" + ENTER +
                "* Return Home - takes you back to the language home page.",
            ["Edit Database"] =
                "Before editing terms, you have to specify what types of terms you wish to edit. " +
                "This is done by specifying search filters. More information can be found " +
                "under the \"Editing Search Filters\" topic." + ENTER + ENTER +
                "After adding search filters and clicking \"Edit Terms\", " +
                "one word at a time will display on screen. From there, " +
                "you may edit any aspect of the word as you wish. Note that the interface " +
                "is identical to that used to add terms to a database. For more information " +
                "about the options, see the \"Add To Database\" topic. Additionally, after " +
                "editing all words, clicking the Next Term button allows you to add new terms to the database.",
            ["Remove From Database"] =
                "Before removing terms, you have to specify what types of terms you wish to remove. " +
                "This is done by specifying search filters. More information can be found " +
                "under the \"Editing Search Filters\" topic." + ENTER + ENTER +
                "After adding search filters and clicking \"View Terms\", a list " +
                "of terms will display on the screen. Select the terms that you wish to delete." + ENTER +
                "* Clicking on a term will select it and deselect any previously-selected terms" + ENTER +
                "* Holding down the ctrl key while clicking on terms allows you to select multiple terms" + ENTER +
                "* Holding down the shift key while dragging the mouse over multiple terms allows you " +
                "to select multiple consecutive terms" + ENTER + ENTER +
                "Once you have selected all of the terms you wish to delete, click the " +
                "Remove Selected Terms button.You will be prompted to confirm this deletion. " +
                "If you go through with deleting the terms, they will be permanently removed " +
                "from the language's database and you will be returned to the language home screen." + ENTER + ENTER +
                "There is also a Cancel Removal button on this page. Clicking this button will " +
                "return you to the language home page without removing any words, regardless of " +
                "whether or not you have selected any terms.",
            ["Editing Search Filters"] =
                "This page is used to specify criteria that are then used to search a language's " +
                "vocabulary terms." + ENTER + ENTER + ENTER +
                "At the top of the screen are three drop - down tabs and a textbox. From left to right, " +
                "the controls are used to input the following:" + ENTER + ENTER +
                "* Function - An optional parameter that is applied to the field" + ENTER +
                "  1. Count - Counts how many of the value is stored in the field; " +
                "can only be used on definitions and characteristics fields" + ENTER +
                "  2. Len - Calculates the length of the value; " +
                "can only be used on term, part of speech, and alternative spelling types fields" + ENTER +
                "  3. Highest - Finds the highest number of terms of the value; " +
                "can only be used on terms, alternative spelling types, number times studied, " +
                "number times correct, and percentage correct fields" + ENTER + 
                "    * Term, alternative spelling types - finds the values that would come later in the dictionary" + ENTER +
                "    * Numbers - finds largest numbers" + ENTER +
                "  4. Lowest - Finds the lowest number of terms of the value; " +
                "can only be used on terms, alternative spelling types, number times studied, " +
                "number times correct, and percentage correct fields" + ENTER +
                "    * Term, alternative spelling types - finds the values that would come sooner in the dictionary" + ENTER +
                "    * Numbers - finds smallest numbers" + ENTER + 
                "  5. Random - Selects terms randomly; can be used with all fields**" + ENTER +
                "** Note that the random function does not actually act on a field, " +
                "but a field name is required for it to be a valid search command" + ENTER + ENTER +
                "* Field - An aspect of a word, such as definitions, special, etc." + ENTER + ENTER +
                "* Comparison type - The method applied to the field to compare it to the value" + ENTER + 
                "  1. ~ (contains)" + ENTER +
                "    * Can only be used on terms, definitions, part of speech, " +
                "characteristics, and alternative spelling types fields" + ENTER +
                "    * Cannot be used with any functions" + ENTER +
                "  2. = (equals)" + ENTER +
                "    * Can be used on all fields" + ENTER + 
                "    * Can be used with all functions**" + ENTER +
                "  3. <> (does not equal)" + ENTER + 
                "    * Can be used on all fields" + ENTER + 
                "    * Can be used with all functions" + ENTER +
                "  4. > (greater than)" + ENTER + 
                "    * Can only be used on terms, alternative spelling types, and number fields" + ENTER +
                "    * Can be used with all functions" + ENTER +
                "  5. < (less than)" + ENTER + 
                "    * Can only be used on terms, alternative spelling types, and number fields" + ENTER +
                "    * Can be used with all functions" + ENTER + 
                "  6. >= (greater than or equal)" + ENTER + 
                "    * Can only be used on terms, alternative spelling types, and number fields" + ENTER +
                "    * Can be used with all functions" + ENTER + 
                "  7. <= (less than or equal)" + ENTER + 
                "    * Can only be used on terms, alternative spelling types, and number fields" + ENTER + 
                "    * Can be used with all functions" + ENTER +
                "** Note that comparison type does not actually influence the highest and " +
                "lowest functions(ex: Highest Term = 3 and Highest Term< 3 both return the " +
                "three terms alphabetically last)" + ENTER + ENTER +
                "* Value - Checked against the actual value of each word under the specified " +
                "field to determine if the word matches the criterion" + ENTER + ENTER + ENTER +
                "Some examples of valid search criteria:" + ENTER + 
                "1. Len Term = 5" + ENTER + 
                "2. [blank] Special = false" + ENTER + 
                "3. Highest Number_Times_Studied = 10" + ENTER + 
                "4. [blank] Percentage_Correct > 0.5" + ENTER + ENTER +
                "Once you have entered a search criterion, click the Submit button. " +
                "It will then be shown in the box below the input controls. " +
                "If you wish to remove a search criterion, double-click the corresponding text." + ENTER + ENTER + ENTER +
                "Once you have finished entering search criteria, click the button " +
                "at the bottom of the screen to run the search query. The words matching " +
                "all of the criteria are obtained and a new page will be displayed. " +
                "Depending on which button you used to reach this screen, the next page could " +
                "allow you to view, edit, or remove the search results.",
            ["Edit Default Characteristics Window"] =
                "This window can be accessed when adding or editing terms by clicking the " +
                "Edit Default Characteristics button. Default characteristics are characteristics " +
                "that are applied to a term automatically. Note that default characteristics " +
                "are NOT applied to terms you have already created/edited unless " +
                "you return to them via the Previous Term button." + ENTER + ENTER +
                "To add a default characteristic, either:" + ENTER +
                "1. Select a characteristic from the drop-down list, or" + ENTER +
                "2. Type a characteristic and press the enter key" + ENTER + ENTER +
                "Default characteristics will display below the characteristic input control. " +
                "To remove a characteristic from the list, double-click it." + ENTER + ENTER +
                "When you are done editing default characteristics, simply close the window.",
        /* Testing vocabulary */
            ["Test Setup"] =
                "This page is used to set up a vocabulary test. Regardless of the test type " +
                "(Flashcards, Flashcards Draw, Fill in the Blanks), the Test Setup page is identical." + ENTER + ENTER + ENTER +
                "This page allows the user to customize several parts of a test:" + ENTER + ENTER +
                "* Prompts - the components of a vocabulary word to display as the question" + ENTER + ENTER +
                "* Answers - the components of a vocabulary word to expect as an answer" + ENTER + ENTER +
                "* Number of terms" + ENTER + ENTER +
                "* Terms type - currently, three types are supported:" + ENTER + 
                "  * Random" + ENTER +
                "  * Least studied" + ENTER +
                "  * Most missed" + ENTER + ENTER +
                "* Update Score checkbox - if checked, the \"number times studied\" and " +
                "\"number times correct\" components of words will be updated" + ENTER + ENTER +
                "* Special Only checkbox - if checked, only words marked as special will be used in the test" + ENTER + ENTER + ENTER +
                "Once you have filled out all of the necessary information, click the " +
                "\"Start Test\" button at the bottom of the screen. Note that if the page isn't " +
                "completely filled out or is filled out incorrectly, clicking this button will do nothing.",
            ["Test Progression"] =
                "While the different test types have slightly different mechanisms, " +
                "the basic test progression is identical." + ENTER + ENTER +
                "* \"Prompt\" box (on the left) - displays the \"test question\" components of the term" + ENTER + ENTER +
                "* \"Answer\" box (on the right) - used to input the information corresponding to the prompt" + ENTER +
                "  * Specifics about the answer type can be found under the \"Flashcards Tests,\" " +
                "\"Flashcards Draw Tests,\" and \"Fill in the Blanks Tests\" topics" + ENTER + ENTER +
                "* Special checkbox (bottom left) - allows you to change the term's \"Special\" " +
                "designation during a test" + ENTER + ENTER +
                "* Show / Submit Answer button (bottom middle) - specifics can be found under the " +
                "\"Flashcards Tests,\" \"Flashcards Draw Tests,\" and \"Fill in the Blanks Tests\" topics" + ENTER + ENTER +
                "* Next Question button (bottom right) - Progresses to the next term",
            ["Test Results"] =
                "After completing a test, you will see a page describing how many terms you got correct. " +
                "Additionally, there will be a percentage representing the percent of terms answered correctly. " +
                "Click the \"Return Home\" button to return to the language home page.",
            ["Flashcards Tests"] =
                "In a Flashcards test, the \"Answer\" box consists of three components:" + ENTER +
                "* Textbox - optionally used to write the answer" + ENTER + 
                "* \"Correct\" and \"Incorrect\" buttons" + ENTER + ENTER +
                "After clicking the \"Show Answer\" button at the bottom of the screen, " +
                "the answer will be displayed at the bottom of the \"Prompt\" box. " +
                "Compare this answer to your answer and self-score it using the \"Correct\" or \"Incorrect\" buttons " +
                "in the \"Answer\" box. You will be unable to progress to the next question until after self-scoring.",
            ["Flashcards Draw Tests"] =
                "In a Flashcards Draw test, the \"Answer\" box consists of three components:" + ENTER +
                "* Drawing surface - optionally used to draw the answer" + ENTER +
                "* \"Correct\" and \"Incorrect\" buttons" + ENTER + ENTER +
                "After clicking the \"Show Answer\" button at the bottom of the screen, " +
                "the answer will be displayed at the bottom of the \"Prompt\" box. " +
                "Compare this answer to your answer and self-score it using the " +
                "\"Correct\" or \"Incorrect\" buttons in the \"Answer\" box. " +
                "You will be unable to progress to the next question until after self-scoring.",
            ["Fill in the Blanks Tests"] =
                "In a Fill in the Blanks test, the \"Answer\" box consists of three components:" + ENTER +
                "* Drop-down list displaying which portion of the answer to enter" + ENTER +
                "* Textbox (to the right of the drop-down list) to input the requested portion of the answer" + ENTER +
                "* Box at the bottom" + ENTER + ENTER +
                "After clicking the \"Submit Answer\" button at the bottom of the screen, " +
                "the box at the bottom of the \"Answer\" box will display whether or not you got the answer correct. " +
                "If the answer is incorrect, the correct answer will also be displayed. " +
                "You will be unable to progress to the next question until after submitting an answer.",
            /* Miscellaneous */
            ["Options"] =
                "This window allows you to change some of Vok's settings." + ENTER + ENTER +
                "* Dark Mode Enabled - when selected, changes the color palette to be " +
                "a dark background with a light foreground" + ENTER + ENTER + 
                "* Right-to-Left Input - when selected, terms are inputted by the user " +
                "right-to-left rather than left-to-right"
        };
    }
}
