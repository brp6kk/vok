﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;
using Vok.Src.Database;
using static Vok.Src.Common.Common;

namespace Vok.Src.UI.Predefined_Pages
{
    /* Sets up various Pages for Vok.
     * These pages are used to study vocabulary:
     *  - TestSetupPage(NextPageCode)
     *  - FlashcardsPage
     *  - FlashcardsDrawPage
     *  - FillInTheBlanksPage
     *  - ResultsPage
     */
    static class TestPages
    {
        #region Test setup page creation - TestSetupPage(NextPageCode)

        /* Creates a Page that allows a user
         * to set up a test.
         */ 
        public static Page TestSetupPage(NextPageCode code)
        {
            Page page = new Page();

            // Prompts can be: Term, Definitions, Alternative spelling types
            List<string> validInput = new List<string>()
                { nameof(DBFieldNames.Term), nameof(DBFieldNames.Definitions) };
            validInput.AddRange(currentDB?.AlternativeSpellingTypes);

            // Groupbox + multichoice - prompt choice input
            Controls.MultiChoice prompt = new Controls.MultiChoice()
            {
                Name = "choPrompt",
                DropDownStyle = ComboBoxStyle.DropDownList,
                Choices = validInput.ToArray()
            };
            GroupBox grpPrompt = new GroupBox()
            {
                Name = "grpPrompt",
                Text = "Prompts"
            };
            grpPrompt.SetDefaultAppearance();
            grpPrompt.Controls.Add(prompt);
            grpPrompt.BackColor = currentPalette.BackAccent;
            foreach (Control c in prompt.Controls)
            {
                c.SetDefaultAppearance();
            }
            grpPrompt.Resize += SetupGroupboxFullResize;
            page.AddControl(grpPrompt);

            // Answers can be: Term, Definitions, Alternative spelling types,
            //                 Part of Speech, Characteristics
            validInput.AddRange(new string[] { nameof(DBFieldNames.Part_of_Speech),
                nameof(DBFieldNames.Characteristics) });

            // Groupbox + multichoice - answer choice input
            Controls.MultiChoice answer = new Controls.MultiChoice()
            {
                Name = "choAnswer",
                DropDownStyle = ComboBoxStyle.DropDownList,
                Choices = validInput.ToArray()
            };
            GroupBox grpAnswer = new GroupBox()
            {
                Name = "grpAnswer",
                Text = "Answers"
            };
            grpAnswer.SetDefaultAppearance();
            grpAnswer.Controls.Add(answer);
            grpAnswer.BackColor = currentPalette.BackAccent;
            foreach (Control c in answer.Controls)
            {
                c.SetDefaultAppearance();
            }
            grpAnswer.Resize += SetupGroupboxFullResize;
            page.AddControl(grpAnswer);

            // Groupbox + textbox - number of terms input
            TextBox numberTerms = new TextBox()
            {
                Name = "txtNumberTerms",
                BorderStyle = BorderStyle.FixedSingle
            };
            GroupBox grpNumberTerms = new GroupBox()
            {
                Name = "grpNumberTerms",
                Text = "Number of Terms"
            };
            grpNumberTerms.SetDefaultAppearance();
            grpNumberTerms.Controls.Add(numberTerms);
            grpNumberTerms.BackColor = currentPalette.BackAccent;
            numberTerms.SetDefaultAppearance();
            grpNumberTerms.Resize += SetupGroupboxShortResize;
            page.AddControl(grpNumberTerms);

            // Groupbox + combobox - types of terms to study
            // (random, least studied, [most missed])
            ComboBox termsType = new ComboBox()
            {
                Name = "cboTermsType",
                DropDownStyle = ComboBoxStyle.DropDownList,
            };
            termsType.Items.AddRange(TEST_TERMS_OPTIONS.ToArray());
            GroupBox grpTermsType = new GroupBox()
            {
                Name = "grpTermsType",
                Text = "Terms Type"
            };
            grpTermsType.SetDefaultAppearance();
            grpTermsType.Controls.Add(termsType);
            grpTermsType.BackColor = currentPalette.BackAccent;
            termsType.SetDefaultAppearance();
            grpTermsType.Resize += SetupGroupboxShortResize;
            page.AddControl(grpTermsType);

            // Checkbox - update word score
            CheckBox updateScore = new CheckBox()
            {
                Name = "chkUpdateScore",
                Text = "Update Score",
                AutoSize = false,
                TextAlign = ContentAlignment.MiddleLeft,
                CheckAlign = ContentAlignment.MiddleLeft
            };
            updateScore.SetDefaultAppearance();
            updateScore.Resize += SetupCheckboxResize;
            page.AddControl(updateScore);

            // Checkbox - special only
            CheckBox specialOnly = new CheckBox()
            {
                Name = "chkSpecialOnly",
                Text = "Special Only",
                AutoSize = false,
                TextAlign = ContentAlignment.MiddleLeft,
                CheckAlign = ContentAlignment.MiddleLeft,
                //Enabled = false
            };
            specialOnly.SetDefaultAppearance();
            specialOnly.Resize += SetupCheckboxResize;
            page.AddControl(specialOnly);

            // Determine appropriate name for test based on NextPageCode.
            string testName;
            switch (code)
            {
                case NextPageCode.Flashcards:
                    testName = TEST_OPTIONS[0]; // "Flashcards"
                    break;
                case NextPageCode.FlashcardsDraw: 
                    testName = TEST_OPTIONS[1]; // "Flashcards Draw"
                    break;
                case NextPageCode.FillInTheBlanks: 
                    testName = TEST_OPTIONS[2]; // "Fill in the Blanks"
                    break;
                default:
                    testName = "";
                    break;
            }

            // Button - start test
            Button nextPage = new Button()
            {
                Name = "btnNextPage",
                Text = "Start New " + testName + " Test",
                FlatStyle = FlatStyle.Flat
            };
            nextPage.SetDefaultAppearance();
            nextPage.BackColor = currentPalette.BackAccent;
            nextPage.Resize += SetupButtonResize;
            nextPage.Click += SetupStartClick;
            page.AddControl(nextPage);

            return page;
        }

        /* Event handler to resize a groupbox
         * to be shorter than other groupboxes.
         * Takes up half of the panel.
         */ 
        public static void SetupGroupboxShortResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Groupbox size change.
            int width = (panel.Width / 3) - (int)(DEFAULT_MARGIN.All * 2.5);
            int height = (int)(panel.Height / 6.75);
            control.Size = new Size(width, height);

            // Groupbox font size change.
            int fontSizeWidth = width / 25;
            int fontSizeHeight = height / 8;
            control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);

            // Control resize.
            SetupGroupBoxControlResize((GroupBox)control);
        }

        /* Event handler to resize a groupbox
         * to the full size.
         * Takes up half of the panel.
         */ 
        public static void SetupGroupboxFullResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Groupbox size change.
            int width = (panel.Width / 3) - (int)(DEFAULT_MARGIN.All * 2.5);
            int height = (int)(panel.Height / 3);
            control.Size = new Size(width, height);

            // Groupbox font size change.
            int fontSizeWidth = width / 25;
            int fontSizeHeight = height / 18;
            control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);

            // Control resize.
            SetupGroupBoxControlResize((GroupBox)control);
        }

        /* Helper function to resize the control within a groupbox.
         */
        public static void SetupGroupBoxControlResize(GroupBox control)
        {
            // Control size & position change.
            int innerWidth = (int)(control.Font.Size * 17);
            int innerHeight = (int)(control.Font.Size * 8);
            int innerX = DEFAULT_MARGIN.All;
            int innerY = DEFAULT_MARGIN.All + (int)control.Font.Size;
            control.Controls[0].Size = new Size(innerWidth, innerHeight);
            control.Controls[0].Location = new Point(innerX, innerY);

            // Control font size change.
            Font newFont = new Font(control.Font.Name, (int)(control.Font.Size * 1.25));
            control.Controls[0].Font = newFont;
            foreach (Control c in control.Controls[0].Controls)
            {
                c.Font = newFont;
            }
        }

        /* Event handler to resize checkboxes
         * on the test setup page.
         */
        public static void SetupCheckboxResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Groupbox size change.
            int width = (panel.Width / 3) - (int)(DEFAULT_MARGIN.All * 2.5);
            int height = (int)(panel.Height / 6.75);
            control.Size = new Size(width, height);

            // Groupbox font size change.
            int fontSizeWidth = width / 25;
            int fontSizeHeight = height / 8;
            control.Font = new Font(control.Font.Name, 
                (int)((fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight) * 1.5));
        }

        /* Event handler to resize the button
         * on the test setup page.
         */ 
        public static void SetupButtonResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Button size change.
            int width = panel.Width - (DEFAULT_MARGIN.All * 3);
            int height = (panel.Height / 4) - (DEFAULT_MARGIN.All * 4);
            control.Size = new Size(width, height);

            // Font size change.
            int fontSizeWidth = width / 40;
            int fontSizeHeight = height / 3;
            control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);
        }

        /* Event handler for clicking the button
         * on the test setup page.
         * If the page input is valid,
         * creates a new test and shows the test page.
         */ 
        public static async void SetupStartClick(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Try converting number of terms to integer
            // Failed conversion means improper input,
            // so test cannot be created.
            int num;
            try
            {
                Control txtNumberTerms = panel.Controls.Find("txtNumberTerms", true)[0];
                num = Convert.ToInt32(txtNumberTerms.Text);
            }
            catch (FormatException)
            {
                return; 
            }

            List<SearchCommand> commands = new List<SearchCommand>();
            List<Word> terms = new List<Word>();
            Control cboTermsType = panel.Controls.Find("cboTermsType", true)[0];

            if (cboTermsType.Text.Equals(TEST_TERMS_OPTIONS[0])) // Random
            {
                commands.Add(SearchCommand.CreateSearchCommand("Random", "Term", "=", num.ToString()));
            }
            else if (cboTermsType.Text.Equals(TEST_TERMS_OPTIONS[1])) // Least studied
            {
                commands.Add(SearchCommand.CreateSearchCommand("Lowest", "Number_Times_Studied", "=", num.ToString()));
            }
            else if (cboTermsType.Text.Equals(TEST_TERMS_OPTIONS[2])) // Most missed
            {
                commands.Add(SearchCommand.CreateSearchCommand("Lowest", "Percentage_Correct", "=", num.ToString()));
            }

            // Special only
            CheckBox chkSpecialOnly = (CheckBox)panel.Controls.Find("chkSpecialOnly", true)[0];
            if (chkSpecialOnly.Checked)
            {
                commands.Add(SearchCommand.CreateSearchCommand("Special", "=", "true"));
            }

            // Get words for the test.
            terms = await currentDB.SearchAsync(commands);
            terms = terms.Take(num).ToList();

            Controls.MultiChoice prompts = (Controls.MultiChoice)panel.Controls.Find("choPrompt", true)[0];
            Controls.MultiChoice answers = (Controls.MultiChoice)panel.Controls.Find("choAnswer", true)[0];
            CheckBox chkUpdateScore = (CheckBox)panel.Controls.Find("chkUpdateScore", true)[0];

            // Create test & ensure that it's a valid test.
            Test.Test test = Test.Test.CreateTest(terms, prompts.ChosenValues, 
                answers.ChosenValues, chkUpdateScore.Checked);
            if (test is null)
            {
                return;
            }

            currentTest = test;

            // Load proper test type based on button text.
            if (control.Text.Contains(TEST_OPTIONS[1])) // "Flashcards Draw"
            {
                Page.UpdatePage(FlashcardsDrawPage());
            }
            else if (control.Text.Contains(TEST_OPTIONS[0])) // "Flashcards"
            {
                Page.UpdatePage(FlashcardsPage());
            } 
            else if (control.Text.Contains(TEST_OPTIONS[2])) // "Fill in the Blanks"
            {
                Page.UpdatePage(FillInTheBlanksPage());
            }
        }

        #endregion

        #region Common code for all test types.

        /* Event handler to resize prompt groupbox and textbox.
         */
        private static void TestPromptResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Groupbox size change.
            int width = panel.Width / 2 - (int)(DEFAULT_MARGIN.All * 2.5);
            int height = (int)(panel.Height / 1.4);
            control.Size = new Size(width, height);

            // Groupbox font size change.
            int fontSizeWidth = width / 32;
            int fontSizeHeight = height / 31;
            control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);

            // Control size, position change, & font size change
            int innerWidth = (int)(control.Font.Size * 22.5);
            int innerHeight = (int)(control.Font.Size * 17.5);
            int innerX = DEFAULT_MARGIN.All;
            int innerY = DEFAULT_MARGIN.All + (int)control.Font.Size;
            control.Controls[0].Size = new Size(innerWidth, innerHeight);
            control.Controls[0].Location = new Point(innerX, innerY);
            control.Controls[0].Font = new Font(control.Font.Name, (int)(control.Font.Size * 1.25));
        }

        /* Event handler to resize checkbox & buttons at bottom of test screen.
         */
        private static void TestButtonsResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Button size change.
            int width = (panel.Width / 3) - (int)(DEFAULT_MARGIN.All * 2.5);
            int height = panel.Height / 8;
            control.Size = new Size(width, height);

            // Button font size change.
            int fontSizeWidth = width / 20;
            int fontSizeHeight = height / 5;
            control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);
        }

        /* Helper function to turn a prompt or answer to a readable string.
         */ 
        private static string FormatDictionary(Dictionary<string, string> dict)
        {
            string output = "";

            foreach (KeyValuePair<string, string> entry in dict)
            {
                output += entry.Key + ":" + ENTER;
                output += entry.Value + ENTER + ENTER;
            }

            return output;
        }

        #endregion

        #region Private common code for Flashcards & FlashcardsDraw

        /* Private page to make flashcards page:
         * takes control which is placed in answer area.
         * Neither should need any events besides resize,
         * so that should be fine.
         */
        private static Page FlashcardsBasePage(Control answerInput)
        {
            Page page = new Page();

            // Prompt textbox + groupbox
            TextBox prompt = new TextBox()
            {
                Name = "txtPrompt",
                Multiline = true,
                ScrollBars = ScrollBars.Vertical,
                ReadOnly = true,
                WordWrap = true,
                BorderStyle = BorderStyle.FixedSingle,
                Text = FormatDictionary(currentTest.GetCurrentPrompt())
            };
            GroupBox grpPrompt = new GroupBox()
            {
                Name = "grpPrompt",
                Text = "Prompt"
            };
            grpPrompt.SetDefaultAppearance();
            grpPrompt.Controls.Add(prompt);
            grpPrompt.BackColor = currentPalette.BackAccent;
            prompt.SetDefaultAppearance();
            grpPrompt.Resize += TestPromptResize;
            page.AddControl(grpPrompt);

            // Answer control, two radio buttons (correct/incorrect), & groupbox
            RadioButton correct = new RadioButton()
            {
                Name = "radCorrect",
                Text = "Correct",
                Enabled = false
            };
            RadioButton incorrect = new RadioButton()
            {
                Name = "radIncorrect",
                Text = "Incorrect",
                Enabled = false
            };
            GroupBox grpAnswer = new GroupBox()
            {
                Name = "grpAnswer",
                Text = "Answer"
            };
            grpAnswer.SetDefaultAppearance();
            grpAnswer.Controls.Add(answerInput);
            grpAnswer.Controls.Add(correct);
            grpAnswer.Controls.Add(incorrect);
            grpAnswer.BackColor = currentPalette.BackAccent;
            answerInput.SetDefaultAppearance();
            grpAnswer.Resize += FlashcardsAnswerResize;
            page.AddControl(grpAnswer);

            // Checkbox - special
            CheckBox special = new CheckBox()
            {
                Name = "chkSpecial",
                Text = "Special",
                AutoSize = false,
                TextAlign = ContentAlignment.MiddleLeft,
                CheckAlign = ContentAlignment.MiddleLeft,
                Checked = currentTest.GetCurrentWord().Special
            };
            special.SetDefaultAppearance();
            special.Resize += TestButtonsResize;
            page.AddControl(special);

            // Button - show answer
            Button showAnswer = new Button()
            {
                Name = "btnShowAnswer",
                Text = "Show Answer",
                FlatStyle = FlatStyle.Flat
            };
            showAnswer.SetDefaultAppearance();
            showAnswer.BackColor = currentPalette.BackAccent;
            showAnswer.Resize += TestButtonsResize;
            showAnswer.Click += FlashcardsShowClick;
            page.AddControl(showAnswer);

            // Button - next question
            Button nextQuestion = new Button()
            {
                Name = "btnNextQuestion",
                Text = "Next Question",
                FlatStyle = FlatStyle.Flat
            };
            nextQuestion.SetDefaultAppearance();
            nextQuestion.BackColor = currentPalette.BackAccent;
            nextQuestion.Resize += TestButtonsResize;
            nextQuestion.Click += FlashcardsNextClick;
            page.AddControl(nextQuestion);

            return page;
        }

        /* Event handler to resize answer groupbox and controls.
         */ 
        private static void FlashcardsAnswerResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Groupbox size change.
            int width = panel.Width / 2 - (int)(DEFAULT_MARGIN.All * 2.5);
            int height = (int)(panel.Height / 1.4);
            control.Size = new Size(width, height);

            // Groupbox font size change.
            int fontSizeWidth = width / 32;
            int fontSizeHeight = height / 31;
            control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);

            // Main answer input resize, position change, & font size change
            int innerWidth = (int)(control.Font.Size * 22.5);
            int innerHeight = (int)(control.Font.Size * 15);
            int innerX = DEFAULT_MARGIN.All;
            int innerY = DEFAULT_MARGIN.All + (int)control.Font.Size;
            control.Controls[0].Size = new Size(innerWidth, innerHeight);
            control.Controls[0].Location = new Point(innerX, innerY);
            control.Controls[0].Font = new Font(control.Font.Name, (int)(control.Font.Size * 1.25));

            // "Correct" radiobutton resize, position change, & font size change
            innerWidth = innerWidth / 2 - (int)(DEFAULT_MARGIN.All * 2.5);
            innerHeight = (int)(control.Font.Size * 2);
            innerY = innerY + control.Controls[0].Height + DEFAULT_MARGIN.All;
            control.Controls[1].Size = new Size(innerWidth, innerHeight);
            control.Controls[1].Location = new Point(innerX, innerY);
            control.Controls[1].Font = new Font(control.Font.Name, (int)(control.Font.Size * 0.8));

            // "Incorrect" radiobutton resize, position change, & font size change
            innerX = innerX + innerWidth + DEFAULT_MARGIN.All;
            control.Controls[2].Size = new Size(innerWidth, innerHeight);
            control.Controls[2].Location = new Point(innerX, innerY);
            control.Controls[2].Font = new Font(control.Font.Name, (int)(control.Font.Size * 0.8));
        }

        /* Event handler to execute when "show" button is clicked.
         * Adds answer text to bottom of prompt textbox.
         * Enables "correct" and "incorrect" radiobuttons.
         */
        private static void FlashcardsShowClick(object sender, EventArgs e)
        {
            Control panel = ((Control)sender).FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Adds correct answer to prompt.
            Control txtPrompt = panel.Controls.Find("txtPrompt", true)[0];
            txtPrompt.Text += FormatDictionary(currentTest.GetCurrentAnswer());

            // Enables radiobuttons.
            string[] buttonNames = { "radCorrect", "radIncorrect" };
            foreach (string name in buttonNames)
            {
                Control button = panel.Controls.Find(name, true)[0];
                button.Enabled = true;
            }

            // Disables "show" button.
            ((Control)sender).Enabled = false;
        }

        /* Event handler to execute when "next" button is clicked.
         * Updates test based on whether use was correct or incorrect
         * and moves on to next question.
         * Additionally, edits "special" value if checkbox is checked.
         */ 
        private static async void FlashcardsNextClick(object sender, EventArgs e)
        {
            Control panel = ((Control)sender).FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Get radiobuttons.
            RadioButton radCorrect = (RadioButton)panel.Controls.Find("radCorrect", true)[0];
            RadioButton radIncorrect = (RadioButton)panel.Controls.Find("radIncorrect", true)[0];

            // Ensures either correct or incorrect is selected.
            if (!radCorrect.Checked && !radIncorrect.Checked)
            {
                return;
            }

            // Get remainder of necessary objects.
            CheckBox chkSpecial = (CheckBox)panel.Controls.Find("chkSpecial", true)[0];
            TextBox prompt = (TextBox)panel.Controls.Find("txtPrompt", true)[0];
            Control answer = panel.Controls.Find("grpAnswer", true)[0].Controls[0];
            Control btnShow = panel.Controls.Find("btnShowAnswer", true)[0];
            Word oldWord = (Word)currentTest.GetCurrentWord().Clone();

            // Updates word in memory.
            Word newWord = currentTest.GetCurrentWord();
            newWord.Special = chkSpecial.Checked;
            currentTest.UpdateScore(radCorrect.Checked);

            // Updates word in external database if test is graded
            // (number times studied & correct would increase)
            // or if special value is changed.
            if (currentTest.Graded || oldWord.Special != newWord.Special)
            {
                await currentDB.EditWordAsync(oldWord, newWord);
            }

            // Goes to next question, if it exists,
            // or ends the test.
            Dictionary<string, string> newPrompt = currentTest.NextPrompt();
            if (newPrompt is null)
            {
                Page.UpdatePage(ResultsPage());
                return;
            }
            prompt.Text = FormatDictionary(newPrompt);

            // Reset test page if there is a next question.
            answer.ResetText();
            answer.Invalidate();
            btnShow.Enabled = true;
            radCorrect.Checked = radCorrect.Enabled = false;
            radIncorrect.Checked = radIncorrect.Enabled = false;
            chkSpecial.Checked = currentTest.GetCurrentWord().Special;
        }

        #endregion

        #region Flashcards test page creation - FlashcardsPage(); FlashcardsDrawPage()

        /* Creates a Page where a user can take
         * a Flashcards test.
         */ 
        public static Page FlashcardsPage()
        {
            // Multiline textbox - used to input answer.
            TextBox answer = new TextBox()
            {
                Name = "txtAnswer",
                Multiline = true,
                ScrollBars = ScrollBars.Vertical,
                WordWrap = true,
                BorderStyle = BorderStyle.FixedSingle
            };

            return FlashcardsBasePage(answer);
        }

        /* Creates a Page where a user can take
         * a Flashcards Draw test.
         */ 
        public static Page FlashcardsDrawPage()
        {
            // DrawingSurface - used to input answer.
            Controls.DrawingSurface answer = new Controls.DrawingSurface()
            {
                Name = "drwAnswer"
            };

            return FlashcardsBasePage(answer);
        }

        #endregion

        #region Fill-in-the-Blanks test page creation - FillInTheBlankPage()

        /* Creates a Page where a user can take
         * a Fill in the Blanks test.
         */ 
        public static Page FillInTheBlanksPage()
        {
            Page page = new Page();

            // Prompt textbox + groupbox
            TextBox prompt = new TextBox()
            {
                Name = "txtPrompt",
                Multiline = true,
                ScrollBars = ScrollBars.Vertical,
                ReadOnly = true,
                WordWrap = true,
                BorderStyle = BorderStyle.FixedSingle,
                Text = FormatDictionary(currentTest.GetCurrentPrompt())
            };
            GroupBox grpPrompt = new GroupBox()
            {
                Name = "grpPrompt",
                Text = "Prompt"
            };
            grpPrompt.SetDefaultAppearance();
            grpPrompt.Controls.Add(prompt);
            grpPrompt.BackColor = currentPalette.BackAccent;
            prompt.SetDefaultAppearance();
            grpPrompt.Resize += TestPromptResize;
            page.AddControl(grpPrompt);

            // Multiprompt answer input, textbox answer output, & groupbox
            Controls.MultiPrompt answerInput = new Controls.MultiPrompt()
            {
                Name = "prmAnswerInput",
                Prompts = currentTest.GetCurrentAnswer().Keys.ToArray()
            };
            TextBox answerOutput = new TextBox()
            {
                Name = "txtAnswerOutput",
                Multiline = true,
                ScrollBars = ScrollBars.Vertical,
                ReadOnly = true,
                WordWrap = true,
                BorderStyle = BorderStyle.FixedSingle
            };
            GroupBox grpAnswer = new GroupBox()
            {
                Name = "grpAnswer",
                Text = "Answer"
            };
            grpAnswer.SetDefaultAppearance();
            grpAnswer.Controls.Add(answerInput);
            grpAnswer.Controls.Add(answerOutput);
            grpAnswer.BackColor = currentPalette.BackAccent;
            foreach (Control c in answerInput.Controls)
            {
                c.SetDefaultAppearance();
            }
            answerOutput.SetDefaultAppearance();
            grpAnswer.Resize += FillInTheBlanksAnswerResize;
            page.AddControl(grpAnswer);

            // Checkbox - special
            CheckBox special = new CheckBox()
            {
                Name = "chkSpecial",
                Text = "Special",
                AutoSize = false,
                TextAlign = ContentAlignment.MiddleLeft,
                CheckAlign = ContentAlignment.MiddleLeft,
                Checked = currentTest.GetCurrentWord().Special
            };
            special.SetDefaultAppearance();
            special.Resize += TestButtonsResize;
            page.AddControl(special);

            // Button - submit answer
            Button showAnswer = new Button()
            {
                Name = "btnSubmitAnswer",
                Text = "Submit Answer",
                FlatStyle = FlatStyle.Flat
            };
            showAnswer.SetDefaultAppearance();
            showAnswer.BackColor = currentPalette.BackAccent;
            showAnswer.Resize += TestButtonsResize;
            showAnswer.Click += FillInTheBlanksSubmitClick;
            page.AddControl(showAnswer);

            // Button - next question
            Button nextQuestion = new Button()
            {
                Name = "btnNextQuestion",
                Text = "Next Question",
                FlatStyle = FlatStyle.Flat,
                Enabled = false
            };
            nextQuestion.SetDefaultAppearance();
            nextQuestion.BackColor = currentPalette.BackAccent;
            nextQuestion.Resize += TestButtonsResize;
            nextQuestion.Click += FillInTheBlanksNextClick;
            page.AddControl(nextQuestion);

            return page;
        }

        /* Event handler to execute to resize answer groupbox
         * for Fill in the Blanks tests.
         */ 
        private static void FillInTheBlanksAnswerResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Groupbox size change.
            int width = panel.Width / 2 - (int)(DEFAULT_MARGIN.All * 2.5);
            int height = (int)(panel.Height / 1.4);
            control.Size = new Size(width, height);

            // Groupbox font size change.
            int fontSizeWidth = width / 32;
            int fontSizeHeight = height / 31;
            control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);

            // Main answer input resize, position change, & font size change
            int innerWidth = (int)(control.Font.Size * 22.5);
            int innerHeight = (int)(control.Font.Size * 4);
            int innerX = DEFAULT_MARGIN.All;
            int innerY = DEFAULT_MARGIN.All + (int)control.Font.Size;
            control.Controls[0].Size = new Size(innerWidth, innerHeight);
            control.Controls[0].Location = new Point(innerX, innerY);
            foreach (Control c in control.Controls[0].Controls)
            {
                c.Font = new Font(control.Font.Name, (int)(control.Font.Size * 1.25));
            }

            // Answer output box resize, position change, & font size change
            innerHeight = (int)(control.Font.Size * 15);
            innerY = innerY + control.Controls[0].Height + DEFAULT_MARGIN.All;
            control.Controls[1].Size = new Size(innerWidth, innerHeight);
            control.Controls[1].Location = new Point(innerX, innerY);
            control.Controls[1].Font = new Font(control.Font.Name, (int)(control.Font.Size * 1.25));
        }

        /* Event handler to execute when Submit button is clicked.
         * Gets answer, compares it to expected answer.
         * Extra text is added to the answer textbox to display
         * whether or not user was correct.
         * If the test is graded, the external database is edited.
         */
        private static async void FillInTheBlanksSubmitClick(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Gets other relevant controls.
            Controls.MultiPrompt prmAnswerInput = (Controls.MultiPrompt)panel.Controls.Find("prmAnswerInput", true)[0];
            TextBox txtAnswerOutput = (TextBox)panel.Controls.Find("txtAnswerOutput", true)[0];
            Button btnNextQuestion = (Button)panel.Controls.Find("btnNextQuestion", true)[0];

            // Get user's answer
            Dictionary<string, string> userAnswers = new Dictionary<string, string>();
            for (int index = 0; index < prmAnswerInput.Prompts.Count() && 
                 index < prmAnswerInput.Answers.Count(); index++)
            {
                userAnswers[prmAnswerInput.Prompts[index]] = prmAnswerInput.Answers[index];
            }

            // Determines if answer is correct,
            // updates textbox to display results.
            bool correct = currentTest.SubmitAnswer(userAnswers);
            if (correct)
            {
                txtAnswerOutput.Text = "Correct!";
            }
            else
            {
                txtAnswerOutput.Text = "Sorry, that answer is incorrect.";
                txtAnswerOutput.Text += ENTER + ENTER + "The correct answer:" + ENTER + ENTER;
                txtAnswerOutput.Text += FormatDictionary(currentTest.GetCurrentAnswer());
            }

            // Updates external database if test is graded.
            Word oldWord = (Word)currentTest.GetCurrentWord().Clone();
            currentTest.UpdateScore(correct);
            if (currentTest.Graded)
            {
                await currentDB.EditWordAsync(oldWord, currentTest.GetCurrentWord());
            }

            // Prevents double-submission &
            // allows test to progress.
            control.Enabled = false;
            btnNextQuestion.Enabled = true;
        }

        /* Event handler to execute when Next button is clicked.
         * Moves on to next question, or results screen if test is over.
         * Additionally, edits "special" value if checkbox is checked.
         */
        private static async void FillInTheBlanksNextClick(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Gets other relevant controls.
            TextBox txtPrompt = (TextBox)panel.Controls.Find("txtPrompt", true)[0];
            Controls.MultiPrompt prmAnswerInput = (Controls.MultiPrompt)panel.Controls.Find("prmAnswerInput", true)[0];
            TextBox txtAnswerOutput = (TextBox)panel.Controls.Find("txtAnswerOutput", true)[0];
            CheckBox chkSpecial = (CheckBox)panel.Controls.Find("chkSpecial", true)[0];
            Button btnSubmitAnswer = (Button)panel.Controls.Find("btnSubmitAnswer", true)[0];

            // Changes special value in external database.
            if (chkSpecial.Checked != currentTest.GetCurrentWord().Special)
            {
                Word oldWord = (Word)currentTest.GetCurrentWord().Clone();
                Word newWord = currentTest.GetCurrentWord();
                newWord.Special = chkSpecial.Checked;

                await currentDB.EditWordAsync(oldWord, newWord);
            }

            // Goes to next question, if it exists,
            // or ends the test.
            Dictionary<string, string> prompt = currentTest.NextPrompt();
            if (prompt is null)
            {
                Page.UpdatePage(ResultsPage());
                return;
            }

            // Reset controls for next question.
            txtPrompt.Text = FormatDictionary(prompt);
            for (int index = 0; index < prmAnswerInput.Answers.Count(); index++)
            {
                prmAnswerInput.Answers[index] = "";
            }
            ((ComboBox)prmAnswerInput.Controls[1]).SelectedIndex = 0;
            prmAnswerInput.Controls[0].Text = "";
            txtAnswerOutput.Text = "";
            chkSpecial.Checked = currentTest.GetCurrentWord().Special;
            control.Enabled = false;
            btnSubmitAnswer.Enabled = true;
        }

        #endregion

        #region Results page shown after a test - ResultsPage()

        /* Creates a Page to display results of a test.
         */ 
        public static Page ResultsPage()
        {
            Page page = new Page();

            // Label - shows results of test.
            Label results = new Label()
            {
                Name = "lblResults",
                AutoSize = false,
                TextAlign = ContentAlignment.MiddleCenter,
                Text = SetResultsText()
            };
            results.SetDefaultAppearance();
            results.BackColor = currentPalette.BackAccent;
            results.Resize += ResultsLabelResize;
            page.AddControl(results);

            // Button - return to language home.
            Button returnHome = new Button()
            {
                Name = "btnReturnHome",
                Text = "Return Home",
                FlatStyle = FlatStyle.Flat,
            };
            returnHome.SetDefaultAppearance();
            returnHome.BackColor = currentPalette.BackAccent;
            returnHome.Resize += ResultsButtonResize;
            returnHome.Click += ResultsButtonClick;
            page.AddControl(returnHome);

            return page;
        }

        /* Helper function to create results text to display in label.
         */ 
        private static string SetResultsText()
        {
            double percent = (double)currentTest.NumberCorrect / currentTest.TestLength;

            string endingText;
            if (percent > 0.8)
            {
                endingText = "Great job!";
            }
            else if (percent > 0.6)
            {
                endingText = "Not bad!";
            }
            else
            {
                endingText = "Could be better...";
            }

            string results = currentTest.NumberCorrect + " correct out of " +
                             currentTest.TestLength + " terms" + ENTER +
                             (Math.Round(percent, 3) * 100) + "%" + ENTER + ENTER +
                             endingText;

            return results;
        }

        /* Event handler for label resize.
         */ 
        public static void ResultsLabelResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Label size change.
            int width = panel.Width - (DEFAULT_MARGIN.All * 3);
            int height = (int)(panel.Height / 1.5);
            control.Size = new Size(width, height);

            // Font size change.
            int fontSizeWidth = width / 25;
            int fontSizeHeight = height / 11;
            control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);
        }

        /* Event handler for button resize.
         */ 
        public static void ResultsButtonResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Label size change.
            int width = panel.Width - (DEFAULT_MARGIN.All * 3);
            int height = panel.Height / 7;
            control.Size = new Size(width, height);

            // Font size change.
            int fontSizeWidth = width / 50;
            int fontSizeHeight = height / 5;
            control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);
        }

        /* Event handler for button clicks.
         * Returns to language home page.
         */ 
        public static void ResultsButtonClick(object sender, EventArgs e)
        {
            currentTest = null;
            Page.UpdatePage(StartupPages.LanguageHomePage());
        }

        #endregion
    }
}
