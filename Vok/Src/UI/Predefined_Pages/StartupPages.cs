﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Vok.Src.Database;
using static Vok.Src.Common.Common;

namespace Vok.Src.UI.Predefined_Pages
{
    /* Sets up various Pages for Vok.
     * These pages are seen at startup:
     *  - HomePage()
     *  - NewLanguagePage()
     *  - LanguageHomePage()
     *  - ErrorPage(string)
     */ 
    static class StartupPages
    {
        #region Home page creation - HomePage()

        /* Creates a Page that serves as the home page for Vok.
         */
        public static Page HomePage()
        {
            Page page = new Page();

            // Label - prompt.
            Label prompt = new Label()
            {
                Name = "lblPrompt",
                Text = "Welcome, " + Environment.UserName + ".\n" +
                       "Which language would you like to study?",
                AutoSize = false,
                TextAlign = ContentAlignment.MiddleCenter,
                Size = new Size(500, 200),
            };
            prompt.SetDefaultAppearance();
            prompt.BackColor = currentPalette.BackAccent;
            prompt.Resize += HomePromptResize;
            page.AddControl(prompt);

            // Listbox - choice of language.
            ListBox choices = new ListBox()
            {
                Name = "lstChoices",
                ScrollAlwaysVisible = true
            };
            choices.SetDefaultAppearance();
            choices.BackColor = currentPalette.BackAccent;
            choices.Resize += HomeChoicesResize;
            choices.DoubleClick += HomeChoicesDoubleClick;
            // Items: languages + new language option
            foreach (string language in FileIO.ExistingLanguages())
            {
                choices.Items.Add(language);
            }
            choices.Items.Add(NEW_LANGUAGE);
            page.AddControl(choices);

            return page;
        }

        /* Event handler to resize the Home page's label.
         */ 
        private static void HomePromptResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Label size change.
            int width = panel.Width - (DEFAULT_MARGIN.All * 3);
            int height = (panel.Height / 2) - (DEFAULT_MARGIN.All * 4);
            control.Size = new Size(width, height);

            // Font size change.
            int fontSizeWidth = width  / 30;
            int fontSizeHeight = height / 10;
            control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);
        }

        /* Event handler to resize the Home page's listbox.
         */ 
        private static void HomeChoicesResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Listbox size change.
            int width = panel.Width - (DEFAULT_MARGIN.All * 3);
            int height = (panel.Height / 2) - DEFAULT_MARGIN.All;
            control.Size = new Size(width, height);

            // Font size change.
            int fontSizeWidth = width / 35;
            int fontSizeHeight = height / 13;
            control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);
        }

        /* Event handler to open new page upon doubleclick of listbox item.
         */ 
        private static async void HomeChoicesDoubleClick(object sender, EventArgs e)
        {
            ListBox listbox = (ListBox)sender;
            string choice = (string)listbox.SelectedItem;

            // Update page based on user's choice.
            switch (choice)
            {
                case NEW_LANGUAGE:
                    Page.UpdatePage(NewLanguagePage());
                    break;
                case null:
                    break;
                default:
                    currentDB = await Database.Database.CreateDatabase(choice);
                    Page.UpdatePage(LanguageHomePage());
                    break;
            }
        }

        #endregion

        #region New Language page creation - NewLanguagePage()

        /* Creates a Page that allows user to create new language.
         */
        public static Page NewLanguagePage()
        {
            Page page = new Page();

            // Groupbox + textbox: Language name
            TextBox languageName = new TextBox()
            {
                Name = "txtLanguageName",
                BorderStyle = BorderStyle.FixedSingle
            };
            GroupBox grpLanguageName = new GroupBox()
            {
                Name = "grpLanguageName",
                Text = "Language"
            };
            grpLanguageName.SetDefaultAppearance();
            grpLanguageName.Controls.Add(languageName);
            grpLanguageName.BackColor = currentPalette.BackAccent;
            languageName.SetDefaultAppearance();
            grpLanguageName.Resize += LanguageNameResize;
            page.AddControl(grpLanguageName);

            // Groupbox + multiline textbox: alternative spelling types
            TextBox alternativeSpellings = new TextBox()
            {
                Name = "txtAlternativeSpellingTypes",
                BorderStyle = BorderStyle.FixedSingle,
                RightToLeft = LeftToRight ? RightToLeft.No : RightToLeft.Yes,
                Multiline = true,
                ScrollBars = ScrollBars.Vertical
            };
            GroupBox grpAlternativeSpellings = new GroupBox()
            {
                Name = "grpAlternativeSpellingTypes",
                Text = "Alternative Spelling Types"
            };
            grpAlternativeSpellings.SetDefaultAppearance();
            grpAlternativeSpellings.Controls.Add(alternativeSpellings);
            grpAlternativeSpellings.BackColor = currentPalette.BackAccent;
            alternativeSpellings.SetDefaultAppearance();
            grpAlternativeSpellings.Resize += AlternativeSpellingTypesResize;
            page.AddControl(grpAlternativeSpellings);

            // Button: Manually add terms
            Button addTerms = new Button()
            {
                Name = "btnAddTerms",
                Text = "Add Terms",
                FlatStyle = FlatStyle.Flat
            };
            addTerms.SetDefaultAppearance();
            addTerms.BackColor = currentPalette.BackAccent;
            addTerms.Resize += ButtonResize;
            addTerms.Click += ManuallyAddClick;
            page.AddControl(addTerms);

            // TO-DO: import terms

            return page;
        }

        /* Event handler to resize the New language page's 
         * language name input groupbox.
         */
        public static void LanguageNameResize(object sender, EventArgs e)
        {
            GroupBox control = (GroupBox)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Groupbox size change.
            int width = panel.Width - (DEFAULT_MARGIN.All * 3);
            int height = (panel.Height / 3) - (DEFAULT_MARGIN.All * 4);
            control.Size = new Size(width, height);

            // Font size change.
            int fontSizeWidth = width / 55;
            int fontSizeHeight = height / 10;
            control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);

            // Textbox location change.
            TextBox textbox = (TextBox)control.Controls[0];
            int txtX = DEFAULT_MARGIN.All;
            int txtY = DEFAULT_MARGIN.All + (int)control.Font.Size;
            textbox.Location = new Point(txtX, txtY);

            // Textbox size & font size change.
            textbox.Font = new Font(control.Font.Name, (int)(control.Font.Size * 1.5));
            textbox.Size = new Size((int)(textbox.Font.Size * 20), textbox.Height);
        }

        /* Event handler to resize the New language page's 
         * alternative spelling types input groupbox.
         */
        public static void AlternativeSpellingTypesResize(object sender, EventArgs e)
        {
            GroupBox control = (GroupBox)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Groupbox size change.
            int width = panel.Width - (DEFAULT_MARGIN.All * 3);
            int height = (panel.Height / 2) - (DEFAULT_MARGIN.All * 4);
            control.Size = new Size(width, height);

            // Font size change - same size as other groupbox
            // despite them being different sizes.
            control.Font = control.FindForm().Controls.Find("grpLanguageName", true)[0].Font;

            // Textbox location change.
            TextBox textbox = (TextBox)control.Controls[0];
            int txtX = DEFAULT_MARGIN.All;
            int txtY = DEFAULT_MARGIN.All + (int)control.Font.Size;
            textbox.Location = new Point(txtX, txtY);

            // Textbox size & font size change.
            int txtHeight = 3 * control.FindForm().Controls.Find("grpLanguageName", true)[0].Controls[0].Height;
            textbox.Font = new Font(control.Font.Name, (int)(control.Font.Size * 1.5));
            textbox.Size = new Size((int)(textbox.Font.Size * 20), txtHeight);
        }

        /* Event handler to resize the New language page's button.
         */
        public static void ButtonResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Button size change.
            int width = panel.Width - (DEFAULT_MARGIN.All * 3);
            int height = (panel.Height / 4) - (DEFAULT_MARGIN.All * 4);
            control.Size = new Size(width, height);

            // Font size change.
            int fontSizeWidth = width / 30;
            int fontSizeHeight = height / 3;
            control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);
        }

        /* Event handler to create new language upon button click.
         */ 
        public static async void ManuallyAddClick(object sender, EventArgs e)
        {
            // Get textboxes that contain data.
            TextBox txtLanguageName = (TextBox)currentPage.Controls.Find((Control c) => c.Name.Equals("grpLanguageName")).Controls[0];
            TextBox txtAlternativeSpellingTypes = (TextBox)currentPage.Controls.Find((Control c) => c.Name.Equals("grpAlternativeSpellingTypes")).Controls[0];

            // Create new language, assuming language does not already exist.
            if (!txtLanguageName.Text.Equals("") && !FileIO.ExistingLanguages().Contains(txtLanguageName.Text))
            {
                await FileIO.NewLanguageAsync(txtLanguageName.Text);
                currentDB = await Database.Database.CreateDatabase(txtLanguageName.Text);

                foreach (string item in txtAlternativeSpellingTypes.Lines)
                {
                    await currentDB.AddAlternativeSpellingTypeAsync(item);
                }

                Page.UpdatePage(EditDatabasePages.WordInputPage(new List<Word>()));
            }
            else
            {
                MessageBox.Show("Not a valid new language.", "Vok",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        #endregion

        #region Language Home page creation - LanguageHomePage()

        /* Creates a Page that serves as a hub to interact with the database.
         */
        public static Page LanguageHomePage()
        {
            Page page = new Page();

            // Label - language name
            Label languageName = new Label()
            {
                Name = "lblLanguageName",
                Text = currentDB?.Language,
                AutoSize = false,
                TextAlign = ContentAlignment.MiddleCenter
            };
            languageName.SetDefaultAppearance();
            languageName.Resize += LanguageLabelResize;
            page.AddControl(languageName);

            // Groupbox - database-editing buttons
            GroupBox databaseBox = new GroupBox()
            {
                Name = "grpDatabase",
                Text = "Edit Database"
            };
            databaseBox.SetDefaultAppearance();
            databaseBox.BackColor = currentPalette.BackAccent;
            databaseBox.Resize += DictionaryGroupResize;

            // Buttons - Add To, Search, Edit, Remove From
            string[] buttonTexts = { "Add To", "Search", "Edit", "Remove From" };
            string[] buttonNames = { "btnAddTo", "btnSearch", "btnEdit", "btnRemoveFrom" };
            for (int index = 0; index < 4; index++)
            {
                Button button = new Button()
                {
                    Name = buttonNames[index],
                    Text = buttonTexts[index]
                };
                databaseBox.Controls.Add(button);
                button.BackColor = currentPalette.BackDefault;
                button.Click += DictionaryButtonClick;
            }

            page.AddControl(databaseBox);

            // Groupbox - test-creation buttons
            GroupBox testBox = new GroupBox()
            {
                Name = "grpTest",
                Text = "Set Up Test"
            };
            testBox.SetDefaultAppearance();
            testBox.BackColor = currentPalette.BackAccent;
            testBox.Resize += DictionaryGroupResize;

            // Buttons - Flashcards, Flashcards Draw, Fill in the Blanks
            buttonNames = new string[]{ "btnFlashcards", "btnFlashcardsDraw", "btnFillInTheBlanks" };
            for (int index = 0; index < 3; index++)
            {
                Button button = new Button()
                {
                    Name = buttonNames[index],
                    Text = TEST_OPTIONS[index]
                };
                testBox.Controls.Add(button);
                button.BackColor = currentPalette.BackDefault;
                button.Click += TestButtonClick;
            }

            page.AddControl(testBox);

            return page;
        }

        /* Event handler to resize the label that displays 
         * the name of the language at the top of the page.
         */ 
        public static void LanguageLabelResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Label size change.
            int width = panel.Width - (DEFAULT_MARGIN.All * 3);
            int height = panel.Height / 5;
            control.Size = new Size(width, height);

            // Font size change.
            int fontSizeWidth = width / 30;
            int fontSizeHeight = height / 10;
            control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);
        }

        /* Event handler to resize the groupbox that contains
         * the buttons used to edit the dictionary.
         */ 
        public static void DictionaryGroupResize(object sender, EventArgs e)
        {
            GroupBox control = (GroupBox)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Groupbox size change.
            int width = panel.Width - (DEFAULT_MARGIN.All * 3);
            int height = (panel.Height / 3) - (DEFAULT_MARGIN.All * 2);
            control.Size = new Size(width, height);

            // Font size change.
            int fontSizeWidth = width / 55;
            int fontSizeHeight = height / 10;
            control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);

            // Button size & font size changes.
            int buttonWidth = (int)(control.Font.Size * 8.5);
            int buttonHeight = (int)(control.Font.Size * 4.5);
            int buttonX = DEFAULT_MARGIN.All;
            int buttonY = DEFAULT_MARGIN.All + (int)control.Font.Size; ;
            for (int index = 0; index < control.Controls.Count; index++)
            {
                control.Controls[index].Font = new Font(control.Font.Name, (int)(control.Font.Size * 1.5));
                control.Controls[index].Size = new Size(buttonWidth, buttonHeight);
                control.Controls[index].Location = new Point(buttonX, buttonY);
                buttonX += buttonWidth + DEFAULT_MARGIN.All;
            }
        }

        /* Event handler to open the appropriate dictionary-editing page 
         * upon a button click.
         */ 
        public static void DictionaryButtonClick(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            switch (button.Name)
            {
                case "btnAddTo":
                    Page.UpdatePage(EditDatabasePages.WordInputPage());
                    break;
                case "btnSearch":
                    Page.UpdatePage(EditDatabasePages.SearchFiltersPage(NextPageCode.SearchDictionary));
                    break;
                case "btnEdit":
                    Page.UpdatePage(EditDatabasePages.SearchFiltersPage(NextPageCode.EditDictionary));
                    break;
                case "btnRemoveFrom":
                    Page.UpdatePage(EditDatabasePages.SearchFiltersPage(NextPageCode.RemoveFromDictionary));
                    break;
                default:
                    break;
            }
        }

        /* Event handler to open the appropriate test-setup page 
         * upon a button click.
         */
        public static void TestButtonClick(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            switch (button.Name)
            {
                case "btnFlashcards":
                    Page.UpdatePage(TestPages.TestSetupPage(NextPageCode.Flashcards));
                    break;
                case "btnFlashcardsDraw":
                    Page.UpdatePage(TestPages.TestSetupPage(NextPageCode.FlashcardsDraw));
                    break;
                case "btnFillInTheBlanks":
                    Page.UpdatePage(TestPages.TestSetupPage(NextPageCode.FillInTheBlanks));
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region Error page creation - ErrorPage(string)

        /* Creates a Page that displays if an error occurs.
         * String parameter represents the error message
         * to be displayed.
         */ 
        public static Page ErrorPage(string error)
        {
            Page page = new Page();

            // Label - error message.
            Label message = new Label()
            {
                Name = "lblMessage",
                Text = error,
                AutoSize = false,
                TextAlign = ContentAlignment.MiddleCenter
            };
            message.SetDefaultAppearance();
            message.BackColor = currentPalette.BackAccent;
            message.Resize += ErrorResize;
            page.AddControl(message);

            return page;
        }

        /* Event handler to resize the error message label.
         */ 
        public static void ErrorResize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            Control panel = control.FindForm().Controls.Find(PAGE_PANEL_NAME, false)[0];

            // Label size change.
            int width = panel.Width - (DEFAULT_MARGIN.All * 3);
            int height = panel.Height - (DEFAULT_MARGIN.All * 3);
            control.Size = new Size(width, height);

            // Font size change.
            int fontSizeWidth = width / 33;
            int fontSizeHeight = height / 20;
            control.Font = new Font(control.Font.Name, fontSizeWidth > fontSizeHeight ? fontSizeWidth : fontSizeHeight);
        }

        #endregion
    }
}
