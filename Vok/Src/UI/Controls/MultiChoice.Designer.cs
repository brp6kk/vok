﻿namespace Vok.Src.UI.Controls
{
    partial class MultiChoice
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ddlPossible = new System.Windows.Forms.ComboBox();
            this.lstChosen = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // ddlPossible
            // 
            this.ddlPossible.Dock = System.Windows.Forms.DockStyle.Top;
            this.ddlPossible.FormattingEnabled = true;
            this.ddlPossible.Location = new System.Drawing.Point(0, 0);
            this.ddlPossible.Name = "ddlPossible";
            this.ddlPossible.Size = new System.Drawing.Size(181, 24);
            this.ddlPossible.TabIndex = 0;
            this.ddlPossible.SelectedIndexChanged += new System.EventHandler(this.ddlPossible_SelectedIndexChanged);
            this.ddlPossible.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ddlPossible_KeyDown);
            // 
            // lstChosen
            // 
            this.lstChosen.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lstChosen.FormattingEnabled = true;
            this.lstChosen.ItemHeight = 16;
            this.lstChosen.Location = new System.Drawing.Point(0, 24);
            this.lstChosen.Name = "lstChosen";
            this.lstChosen.Size = new System.Drawing.Size(181, 116);
            this.lstChosen.TabIndex = 1;
            this.lstChosen.DoubleClick += new System.EventHandler(this.lstChosen_DoubleClick);
            // 
            // MultiChoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lstChosen);
            this.Controls.Add(this.ddlPossible);
            this.Name = "MultiChoice";
            this.Size = new System.Drawing.Size(181, 140);
            this.Load += new System.EventHandler(this.MultiChoice_Load);
            this.Resize += new System.EventHandler(this.MultiChoice_Resize);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox ddlPossible;
        private System.Windows.Forms.ListBox lstChosen;
    }
}
