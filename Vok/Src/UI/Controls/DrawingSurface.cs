﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Vok.Src.UI.Controls
{
    /* Control that acts as a drawing surface.
     * Drawing with a mouse adds lines onto the surface.
     */ 
    public partial class DrawingSurface : UserControl
    {
        // Public properties.
        public bool DarkButtons { get; set; } = true;
        public Color PenColor { get; set; } = Color.Black;
        public float PenWidth { get; set; } = 3.0F;
        public Color EraserColor { get; set; } = SystemColors.Control;
        public float EraserWidth { get; set; } = 10.0F;

        // Private properties.
        private bool Drawing = false;
        private Pen DrawingPen;
        private Pen ErasingPen;
        private Pen CurrentPen;
        private Point? LastPoint = null;
        private Graphics Picture;
        
        public DrawingSurface()
        {
            InitializeComponent();
            this.Invalidated += DrawingSurface_Load;
        }

        /* Meant to execute on load of a DrawingSurface.
         * Defines Graphics surface and pens, and changes color of buttons if necessary.
         */ 
        public void DrawingSurface_Load(object sender, EventArgs e)
        {
            Picture = this.CreateGraphics();

            DrawingPen = new Pen(PenColor) { Width = PenWidth, LineJoin = System.Drawing.Drawing2D.LineJoin.Round };
            ErasingPen = new Pen(EraserColor) { Width = EraserWidth, LineJoin = System.Drawing.Drawing2D.LineJoin.Round };
            CurrentPen = DrawingPen;

            if (!DarkButtons)
            {
                this.BtnPen.BackgroundImage = Properties.Resources.Pencil_White;
                this.BtnErase.BackgroundImage = Properties.Resources.Eraser_White;
                this.BtnClear.BackgroundImage = Properties.Resources.Recycle_White;
            }
        }

        /* Meant to execute when the BackColor of a DrawingSurface is changed.
         * Changes the eraser color to be the same as the new BackColor.
         */ 
        private void DrawingSurface_BackColorChanged(object sender, EventArgs e)
        {
            EraserColor = this.BackColor;
        }

        /* Meant to execute when a MouseDown event occurs.
         * Begins drawing a line.
         */ 
        private void DrawingSurface_MouseDown(object sender, MouseEventArgs e)
        {
            Drawing = true;
        }

        /* Meant to execute when a MouseUp event occurs.
         * Stops drawing a line.
         */ 
        private void DrawingSurface_MouseUp(object sender, MouseEventArgs e)
        {
            Drawing = false;
            LastPoint = null;
        }

        /* Meant to execute when a MouseMove event occurs.
         * If a line is being drawn, then it is updated based on the new position of the mouse.
         */ 
        private void DrawingSurface_MouseMove(object sender, MouseEventArgs e)
        {
            if (Drawing)
            {
                Point newPoint = e.Location;

                if (LastPoint.HasValue)
                {
                    Picture.DrawLine(CurrentPen, LastPoint.Value, newPoint);
                }

                LastPoint = newPoint;
            }
        }

        /* Meant to execute when the Pen button is clicked.
         * Changes the current pen to the drawing pen.
         */ 
        private void BtnPen_Click(object sender, EventArgs e)
        {
            CurrentPen = DrawingPen;
        }

        /* Meant to execute when the Eraser button is clicked.
         * Changes the current pen to the eraser.
         */ 
        private void BtnErase_Click(object sender, EventArgs e)
        {
            CurrentPen = ErasingPen;
        }

        /* Meant to execute when the Clear button is clicked.
         * Clears the drawing off of the DrawingSurface.
         */ 
        private void BtnClear_Click(object sender, EventArgs e)
        {
            Picture.Clear(this.BackColor);
        }
    }
}
