﻿using System;
using System.Windows.Forms;

namespace Vok.Src.UI.Controls
{
    /* Control that stores several prompts in a combobox.
     * Answers are inputted into a textbox and saved
     * so that the response in Answers[i] corresponds to 
     * the prompt in Prompts[i].
     */ 
    public partial class MultiPrompt : UserControl
    {
        public string[] Prompts { get; set; } = new string[0];
        public string[] Answers { get; set; } = new string[0];

        /* Constructor.
         */ 
        public MultiPrompt()
        {
            InitializeComponent();
        }

        /* Adds prompts to storage, sets all answers to be empty strings.
         */ 
        private void MultiPrompt_Load(object sender, EventArgs e)
        {
            Answers = new string[Prompts.Length];

            // Adds prompt to combobox & sets default answers blank.
            for (int index = 0; index < Prompts.Length; index++)
            {
                ddlPrompts.Items.Add(Prompts[index]);
                Answers[index] = "";
            }

            // Sets default prompt to first prompt.
            if (Prompts.Length > 0)
            {
                ddlPrompts.Text = Prompts[0];
            }
        }

        /* Resizes the two textboxes appropriately on resize of whole control.
         */
        private void MultiPrompt_Resize(object sender, EventArgs e)
        {
            this.Height = ddlPrompts.Height > txtAnswer.Height ? ddlPrompts.Height : txtAnswer.Height;
            int newWidth = this.Width / 2 - (int)(this.Height * 1.5) ;
            ddlPrompts.Width = newWidth;
            txtAnswer.Width = newWidth;
        }

        /* Helper function to determine the index corresponding to the current prompt.
         */ 
        private int GetCurrentPromptIndex()
        {
            for (int index = 0; index < Prompts.Length; index++)
            {
                if (Prompts[index].Equals(ddlPrompts.Text))
                {
                    return index;
                }
            }

            return -1;
        }

        /* Updates appropriate Answer property when the value in the textbox is changed.
         */ 
        private void txtAnswer_TextChanged(object sender, EventArgs e)
        {
            int index = GetCurrentPromptIndex();

            if (index >= 0)
            {
                Answers[index] = txtAnswer.Text;
            }
        }

        /* Changes value in textbox to the appropriate Answer property 
         * when the value in the combobox is changed.
         */ 
        private void ddlPrompts_TextChanged(object sender, EventArgs e)
        {
            int index = GetCurrentPromptIndex();
            if (index >= 0)
            {
                txtAnswer.Text = Answers[index];
            }
        }
    }
}
