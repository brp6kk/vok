﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Vok.Src.UI.Controls
{
    /* Control that contains two textbox with 
     * vertical scroll synchronization.
     * Scrolling up/down in one textbox scrolls 
     * up/down identically in the other box.
     */ 
    public partial class DoubleTextbox : UserControl
    {
        public string[] LeftLines { get; set; }
        public string[] RightLines { get; set; }
        public bool LeftTextBoxReadOnly { get; set; } = false;
        public bool RightTextBoxReadOnly { get; set; } = false;

        /* Constructor.
         */ 
        public DoubleTextbox()
        {
            InitializeComponent();
        }

        private void DoubleTextbox_Load(object sender, EventArgs e)
        {
            txtLeft.Lines = LeftLines;
            txtRight.Lines = RightLines;
            txtLeft.ReadOnly = LeftTextBoxReadOnly;
            txtRight.ReadOnly = RightTextBoxReadOnly;
            txtLeft.UpdateVisibleLines();
            txtRight.UpdateVisibleLines();
        }

        /* Resizes the two textboxes appropriately on resize of whole control.
         */ 
        private void DoubleTextbox_Resize(object sender, EventArgs e)
        {
            txtRight.Width = this.Width / 2;
            txtLeft.Width = this.Width / 2;
        }

        /* Checks to see if a scroll occurred in a textbox,
         * and updates other textbox's visible area if so.
         */ 
        private void KeyUpEvent(object sender, KeyEventArgs e)
        {
            ((SyncTextBox)sender).ScrollBuddy();
        }

        /* Checks to see if a scroll occurred in a textbox,
         * and updates other textbox's visible area if so.
         */ 
        private void MouseUpEvent(object sender, MouseEventArgs e)
        {
            ((SyncTextBox)sender).ScrollBuddy();
        }

        /* Updates Lines property of left textbox.
         */ 
        private void txtLeft_TextChanged(object sender, EventArgs e)
        {
            LeftLines = txtLeft.Lines;
        }

        /* Updates Lines property of right textbox.
         */ 
        private void txtRight_TextChanged(object sender, EventArgs e)
        {
            RightLines = txtRight.Lines;
        }
    }

    class SyncTextBox : TextBox
    {
        public SyncTextBox Buddy { get; set; }
        public int FirstVisibleLine { get; private set; }
        public int LastVisibleLine { get; private set; }

        // Prevents Buddy from scrolling this
        private static bool scrolling;

        // TO-DO: Put in common?
        private const int WM_VSCROLL = 0x115;
        private const int WM_MOUSEWHEEL = 0x20A;

        public void UpdateVisibleLines()
        {
            FirstVisibleLine = GetFirstVisibleLineIndex();
            LastVisibleLine = GetLastVisibleLineIndex();
        }

        /* Helper function to calculate the height of a line of this textbox.
         */ 
        private int GetLineHeight()
        {
            return TextRenderer.MeasureText("X", this.Font).Height;
        }

        /* Helper function to calculate the first visible line of this textbox.
         */ 
        private int GetFirstVisibleLineIndex()
        {
            int midY = this.GetLineHeight() / 2;
            int lineNum = this.GetLineFromCharIndex(this.GetCharIndexFromPosition(new Point(0, midY)));
            return lineNum;
        }

        /* Helper function to calculate the last visible line of this textbox.
         */ 
        private int GetLastVisibleLineIndex()
        {
            int lineHeight = this.GetLineHeight();
            int numVisLines = (this.Height - SystemInformation.HorizontalScrollBarHeight) / lineHeight;
            int lineIndex = this.GetFirstVisibleLineIndex() + numVisLines - 1;
            return lineIndex;
        }

        /* Scrolls Buddy if this control's visible area changed.
         */ 
        public void ScrollBuddy()
        {
            // Prevents errors.
            if (!scrolling && Buddy != null && Buddy.IsHandleCreated)
            {
                // Difference between the new visible area & the previous visible area.
                int firstIndex = GetFirstVisibleLineIndex();
                int lastIndex = GetLastVisibleLineIndex();
                int diff = firstIndex - FirstVisibleLine;

                // This control scrolled down.
                if (diff > 0)
                {
                    for (int i = 0; i < diff; i++)
                    {
                        scrolling = true;
                        SendMessage(Buddy.Handle, WM_VSCROLL, (IntPtr)(1), IntPtr.Zero);
                        scrolling = false;
                    }
                }
                // This control scrolled up.
                else if (diff < 0)
                {
                    diff = Math.Abs(diff);
                    for (int i = 0; i < diff; i++)
                    {
                        scrolling = true;
                        SendMessage(Buddy.Handle, WM_VSCROLL, (IntPtr)(0), IntPtr.Zero);
                        scrolling = false;
                    }
                }

                this.UpdateVisibleLines();
                Buddy.UpdateVisibleLines();
            }
        }

        /* Overrides WndProc to add additional functionality - scrolling this control also scrolls Buddy.
         */ 
        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);

            // Trap scroll message and send it to Buddy control
            if ((m.Msg == WM_VSCROLL || m.Msg == WM_MOUSEWHEEL) && !scrolling && Buddy != null && Buddy.IsHandleCreated)
            {
                scrolling = true;
                SendMessage(Buddy.Handle, m.Msg, m.WParam, m.LParam);
                scrolling = false;

                // Update visible line properties
                this.UpdateVisibleLines();
            }
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wp, IntPtr lp);
    }
}
