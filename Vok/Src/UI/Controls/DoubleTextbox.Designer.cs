﻿namespace Vok.Src.UI.Controls
{
    partial class DoubleTextbox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtLeft = new Vok.Src.UI.Controls.SyncTextBox();
            this.txtRight = new Vok.Src.UI.Controls.SyncTextBox();
            this.SuspendLayout();
            // 
            // txtLeft
            // 
            this.txtLeft.Buddy = this.txtRight;
            this.txtLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtLeft.Location = new System.Drawing.Point(0, 0);
            this.txtLeft.Multiline = true;
            this.txtLeft.Name = "txtLeft";
            this.txtLeft.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtLeft.Size = new System.Drawing.Size(200, 172);
            this.txtLeft.TabIndex = 3;
            this.txtLeft.WordWrap = false;
            this.txtLeft.TextChanged += new System.EventHandler(this.txtLeft_TextChanged);
            this.txtLeft.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpEvent);
            this.txtLeft.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MouseUpEvent);
            // 
            // txtRight
            // 
            this.txtRight.Buddy = this.txtLeft;
            this.txtRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.txtRight.Location = new System.Drawing.Point(200, 0);
            this.txtRight.Multiline = true;
            this.txtRight.Name = "txtRight";
            this.txtRight.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtRight.Size = new System.Drawing.Size(200, 172);
            this.txtRight.TabIndex = 2;
            this.txtRight.WordWrap = false;
            this.txtRight.TextChanged += new System.EventHandler(this.txtRight_TextChanged);
            this.txtRight.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpEvent);
            this.txtRight.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MouseUpEvent);
            // 
            // DoubleTextbox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtLeft);
            this.Controls.Add(this.txtRight);
            this.Name = "DoubleTextbox";
            this.Size = new System.Drawing.Size(400, 172);
            this.Load += new System.EventHandler(this.DoubleTextbox_Load);
            this.Resize += new System.EventHandler(this.DoubleTextbox_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private SyncTextBox txtRight;
        private SyncTextBox txtLeft;
    }
}
