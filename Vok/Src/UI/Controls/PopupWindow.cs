﻿using System;
using System.Windows.Forms;
using static Vok.Src.Common.Common;

namespace Vok.Src.UI.Controls
{
    public partial class PopupWindow : Form
    {
        /* Constructor.
         */ 
        public PopupWindow()
        {
            InitializeComponent();
            this.Load += LoadAppearance;
        }

        /* Load event handler method for this form.
         */
        public void LoadAppearance(object sender, EventArgs e)
        {
            this.BackColor = currentPalette.BackDefault;
            this.BackColor = currentPalette.ForeDefault;
            this.Font = DEFAULT_FONT;
            this.Text = "Vok";
            this.MinimumSize = MINIMUM_POPUP_SIZE;
            this.Size = MINIMUM_POPUP_SIZE;
            this.ShowIcon = false;
        }
    }
}
