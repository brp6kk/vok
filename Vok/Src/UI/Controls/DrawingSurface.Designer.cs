﻿namespace Vok.Src.UI.Controls
{
    partial class DrawingSurface
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DrawingSurface));
            this.BtnClear = new System.Windows.Forms.Button();
            this.BtnErase = new System.Windows.Forms.Button();
            this.BtnPen = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BtnClear
            // 
            this.BtnClear.BackgroundImage = global::Vok.Properties.Resources.Recycle_Black;
            this.BtnClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnClear.Location = new System.Drawing.Point(3, 75);
            this.BtnClear.Name = "BtnClear";
            this.BtnClear.Size = new System.Drawing.Size(30, 30);
            this.BtnClear.TabIndex = 2;
            this.BtnClear.TabStop = false;
            this.BtnClear.UseVisualStyleBackColor = true;
            this.BtnClear.Click += new System.EventHandler(this.BtnClear_Click);
            // 
            // BtnErase
            // 
            this.BtnErase.BackgroundImage = global::Vok.Properties.Resources.Eraser_Black;
            this.BtnErase.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnErase.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnErase.Location = new System.Drawing.Point(3, 39);
            this.BtnErase.Name = "BtnErase";
            this.BtnErase.Size = new System.Drawing.Size(30, 30);
            this.BtnErase.TabIndex = 1;
            this.BtnErase.TabStop = false;
            this.BtnErase.UseVisualStyleBackColor = true;
            this.BtnErase.Click += new System.EventHandler(this.BtnErase_Click);
            // 
            // BtnPen
            // 
            this.BtnPen.BackColor = System.Drawing.Color.Transparent;
            this.BtnPen.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtnPen.BackgroundImage")));
            this.BtnPen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnPen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnPen.Location = new System.Drawing.Point(3, 3);
            this.BtnPen.Name = "BtnPen";
            this.BtnPen.Size = new System.Drawing.Size(30, 30);
            this.BtnPen.TabIndex = 0;
            this.BtnPen.TabStop = false;
            this.BtnPen.UseVisualStyleBackColor = false;
            this.BtnPen.Click += new System.EventHandler(this.BtnPen_Click);
            // 
            // DrawingSurface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.BtnClear);
            this.Controls.Add(this.BtnErase);
            this.Controls.Add(this.BtnPen);
            this.Name = "DrawingSurface";
            this.Size = new System.Drawing.Size(321, 233);
            this.Load += new System.EventHandler(this.DrawingSurface_Load);
            this.BackColorChanged += new System.EventHandler(this.DrawingSurface_BackColorChanged);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DrawingSurface_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DrawingSurface_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DrawingSurface_MouseUp);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BtnPen;
        private System.Windows.Forms.Button BtnErase;
        private System.Windows.Forms.Button BtnClear;
    }
}
