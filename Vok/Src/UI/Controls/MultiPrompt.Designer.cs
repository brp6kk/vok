﻿namespace Vok.Src.UI.Controls
{
    partial class MultiPrompt
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ddlPrompts = new System.Windows.Forms.ComboBox();
            this.txtAnswer = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // ddlPrompts
            // 
            this.ddlPrompts.Dock = System.Windows.Forms.DockStyle.Left;
            this.ddlPrompts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlPrompts.FormattingEnabled = true;
            this.ddlPrompts.Location = new System.Drawing.Point(0, 0);
            this.ddlPrompts.Name = "ddlPrompts";
            this.ddlPrompts.Size = new System.Drawing.Size(125, 24);
            this.ddlPrompts.TabIndex = 0;
            this.ddlPrompts.TextChanged += new System.EventHandler(this.ddlPrompts_TextChanged);
            // 
            // txtAnswer
            // 
            this.txtAnswer.Dock = System.Windows.Forms.DockStyle.Right;
            this.txtAnswer.Location = new System.Drawing.Point(125, 0);
            this.txtAnswer.Name = "txtAnswer";
            this.txtAnswer.Size = new System.Drawing.Size(125, 22);
            this.txtAnswer.TabIndex = 1;
            this.txtAnswer.TextChanged += new System.EventHandler(this.txtAnswer_TextChanged);
            // 
            // MultiPrompt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtAnswer);
            this.Controls.Add(this.ddlPrompts);
            this.Name = "MultiPrompt";
            this.Size = new System.Drawing.Size(250, 25);
            this.Load += new System.EventHandler(this.MultiPrompt_Load);
            this.Resize += new System.EventHandler(this.MultiPrompt_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ddlPrompts;
        private System.Windows.Forms.TextBox txtAnswer;
    }
}
