﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Vok.Src.UI.Controls
{
    /* Control that stores several choices in a combobox.
     * When a choice is selected, it is added to the listbox.
     * If a choice in the listbox is double-clicked, it is removed from the listbox.
     */
    public partial class MultiChoice : UserControl
    {
        public ComboBoxStyle DropDownStyle { get; set; } = ComboBoxStyle.DropDown;
        // Combobox values.
        public string[] Choices { get; set; } = new string[0];
        // Listbox values.
        public List<string> ChosenValues { get; } = new List<string>();

        /* Constructor.
         */
        public MultiChoice()
        {
            InitializeComponent();
            // Update Choices when invalidated.
            Invalidated += MultiChoice_Load;
        }

        /* Adds choices to combobox, updates properties.
         */ 
        private void MultiChoice_Load(object sender, EventArgs e)
        {
            ddlPossible.DropDownStyle = DropDownStyle;

            ddlPossible.Items.Clear();
            // Adds prompt to combobox & sets default answers blank.
            for (int index = 0; index < Choices.Length; index++)
            {
                ddlPossible.Items.Add(Choices[index]);
            }
        }

        /* Resizes the listbox appropriately on resize of whole control.
         */
        private void MultiChoice_Resize(object sender, EventArgs e)
        {
            int newListHeight = this.Height - (int)(ddlPossible.Height * 1.7);
            lstChosen.Height = newListHeight;
        }

        /* Adds item typed in combobox to listbox when the enter key is pressed.
         */ 
        private void ddlPossible_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string str = ddlPossible.Text;
                AddChosenValue(str);

                // Prevents 'ding' on enter
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        /* Adds item selected from combobox when a new item is chosen from the dropdown list.
         */ 
        private void ddlPossible_SelectedIndexChanged(object sender, EventArgs e)
        {
            string str = (string)ddlPossible.SelectedItem;
            AddChosenValue(str);
        }

        /* Removes an item from the listbox when it is double-clicked.
         */ 
        private void lstChosen_DoubleClick(object sender, EventArgs e)
        {
            int index = lstChosen.SelectedIndex;
            RemoveChosenValue(index);
        }

        /* Helper function to determine if string str 
         * is identical to a string contained in the listbox.
         */
        private bool IsInListBox(string str)
        {
            foreach (string item in lstChosen.Items)
            {
                if (item.Equals(str))
                {
                    return true;
                }
            }

            return false;
        }

        /* Adds string str to the listbox and ChosenValues.
         */
        public void AddChosenValue(string str)
        {
            if (!IsInListBox(str))
            {
                lstChosen.Items.Add(str);
                ChosenValues.Add(str);
            }
        }

        /* Removes the string at index from the listbox and ChosenValues.
         */
        public void RemoveChosenValue(int index)
        {
            if (index >= 0 && index < lstChosen.Items.Count && index < ChosenValues.Count)
            {
                lstChosen.Items.RemoveAt(index);
                ChosenValues.RemoveAt(index);
            }
        }
    }
}
