﻿using System;
using System.Collections.Generic;
using System.Linq;
using static Vok.Src.Common.Common;

namespace Vok.Src.Common
{
    /* Container that stores Trees.
     * Duplicate node values are not allowed anywhere within the Forest.
     */
    class Forest<T>
    {
        public List<Tree<T>> Trees { get; private set; }

        /* Constructor.
         */ 
        public Forest(params Tree<T>[] trees)
        {
            Trees = new List<Tree<T>>();
            foreach (Tree<T> tree in trees)
            {
                AddTree(tree);
            }
        }

        /* Adds Tree to Forest 
         * if no nodes in the Tree exist anywhere in the Forest.
         */
        public void AddTree(Tree<T> tree)
        {
            if (tree != null)
            {
                bool found = false;

                // Ensures that no elements of tree are in this Forest.
                List<T> vals = tree.ToList();
                for (int index = 0; index < vals.Count && !found; index++)
                {
                    if (Search(vals[index]) != null)
                    {
                        found = true;
                    }
                }

                if (!found)
                {
                    Trees.Add(tree);
                }
            }
        }

        /* Removes Tree from Forest.
         */
        public void RemoveTree(Tree<T> tree)
        {
            if (tree != null)
            {
                // Checks all Trees in Forest for the tree to remove.
                bool removed = false;
                for (int index = 0; index < Trees.Count && !removed; index++)
                {
                    Tree<T> searchVal = Trees[index].Search(tree.Node);
                    if (searchVal != null && searchVal == tree)
                    {
                        // Tries removing full Tree from Forest, then tries subtrees.
                        // Only one will succeed, but the other one does nothing on failure.
                        Trees.Remove(tree);
                        Trees[index].RemoveChild(tree);
                        removed = true;
                    }
                }
            }
        }

        /* Searches Forest for val.
         * If val is found, the subtree with Node val is returned.
         * If val is not found, null is returned.
         */
        public Tree<T> Search(T val)
        {
            foreach (Tree<T> tree in Trees)
            {
                Tree<T> result = tree.Search(val);
                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }

        /* Determines if a directory of nodes can be added to Forest.
         * Nodes must be unique.
         * Example: /first/second/third
         * 1. 'third' cannot exist anywhere within the Tree
         * 2. One of the following must be true:
         *   a. '/first/second' is an already existing directory
         *   b. Neither 'first' nor second' already exist in the Tree.
         */
        public bool ValidNewDirectory(string dir)
        {
            bool result = false;

            if (dir != null)
            {
                // Tries converting all nodes in directory to type T.
                List<string> strNodes = dir.Split(Common.DIRECTORY_DELIM).ToList();
                strNodes.Clean();
                T[] nodes = new T[strNodes.Count];
                try
                {
                    for (int index = 0; index < strNodes.Count; index++)
                    {
                        nodes[index] = (T)Convert.ChangeType(strNodes[index], typeof(T));
                    }
                }
                catch (InvalidCastException)
                {
                    return false;
                }

                // For loop generates two tests at once:
                // 1. componentExists - Sees if any of the preceding directories
                //    already exists in the Characteristics Forest.
                // 2. directory - Generates the full name of the preceding directory.
                bool nodeExists = false;
                string directory = "";
                for (int index = 0; index < nodes.Length - 1; index++)
                {
                    nodeExists = nodeExists || this.Search(nodes[index]) != null;
                    directory += Common.DIRECTORY_DELIM + strNodes[index];
                }

                result = (!nodeExists || this.ToListWithDirectories().Contains(directory)) &&
                         this.Search(nodes[nodes.Length - 1]) is null;
            }

            return result;
        }

        /* Converts a list of directories to a Forest of nodes.
         */
        public static Forest<string> ListToForest(List<string> directories)
        {
            Forest<string> forest = new Forest<string>();

            foreach (string dir in directories)
            {
                // Get individual directories of characteristic.
                List<string> split = dir.Split(DIRECTORY_DELIM).ToList();
                split.Clean();

                // Add characteristic to forest if none of the aspects already exist in forest.
                if (split.Count > 0 && forest.ValidNewDirectory(dir))
                {
                    bool placed = false;
                    List<Tree<string>> check = forest.Trees;

                    for (int index = 0; index < split.Count && !placed; index++)
                    {
                        // Moves into the directory with name split[index] if it exists.
                        for (int jindex = 0; jindex < check.Count; jindex++)
                        {
                            if (split[index].Equals(check[jindex].Node))
                            {
                                check = check[jindex].Children;
                            }
                        }

                        // Checks if the directories after split[index] already exist in Forest.
                        // If so, go to next iteration of for loop to try moving into next directory.
                        bool exists = false;
                        for (int jindex = index; jindex < split.Count && !exists; jindex++)
                        {
                            if (forest.Search(split[jindex]) != null)
                            {
                                exists = true;
                            }
                        }

                        // None of the subdirectories exist in Forest,
                        // so place the characteristic in this directory.
                        if (!exists)
                        {
                            Tree<string> tree = new Tree<string>(split[index]);
                            Tree<string> temp = tree; // Iterator to create tree.
                            for (int jindex = index + 1; jindex < split.Count; jindex++)
                            {
                                temp.AddChild(new Tree<string>(split[jindex]));
                                temp = temp.Children[0];
                            }

                            check.Add(tree);
                            placed = true;
                        }
                    }
                }
            }

            return forest;
        }

        /* Converts a Forest to a list of Nodes.
         */
        public List<T> ToList()
        {
            List<T> list = new List<T>();

            foreach (Tree<T> tree in Trees)
            {
                list.AddRange(tree.ToList());
            }

            return list;
        }

        /* Makes list of the root Nodes of all Trees in a Forest.
         */
        public List<T> RootsToList()
        {
            List<T> list = new List<T>();

            foreach (Tree<T> tree in Trees)
            {
                list.Add(tree.Node);
            }

            return list;
        }

        /* Converts a Forest to a list of Nodes,
         * with each Node value preceded by the values of the parents.
         * For example:
         *  - Input: new Forest<int>(new Tree<int>(1), new Tree<int>(2, new Tree<int>(3), new Tree<int>(4)), new Tree<int>(5, new Tree<int>(6))
         *  - Output: { "/1", "/2", "/2/3", "/2/4", "/5", "/5/6" }
         */
        public List<string> ToListWithDirectories()
        {
            List<string> list = new List<string>();

            foreach (Tree<T> tree in Trees)
            {
                list.AddRange(tree.ToListWithDirectories());
            }

            list.Sort();
            return list;
        }

        /* Overloads == operator for Forest type.
         * Two Forests are equal if all Trees are identical.
         * Note that Trees do not need to be stored in Forests 
         * in the same order for two Forests to be identical.
         */
        public static bool operator ==(Forest<T> a, Forest<T> b)
        {
            // Null checks.
            if (a is null && b is null)
            {
                return true;
            }
            else if (a is null || b is null)
            {
                return false;
            }

            // Number of trees must be identical.
            if (a.Trees.Count != b.Trees.Count)
            {
                return false;
            }

            // Loops through all Trees in a, 
            // comparing them to all Trees in b until an identical Tree is found.
            // If no identical Tree is found, the Forests aren't equal.
            for (int aindex = 0; aindex < a.Trees.Count; aindex++)
            {
                bool found = false;
                for (int bindex = 0; bindex < b.Trees.Count && !found; bindex++)
                {
                    if (a.Trees[aindex] == b.Trees[bindex])
                    {
                        found = true;
                    }
                }
                if (!found)
                {
                    return false;
                }
            }

            // Getting through the for loop indicates that 
            // all Trees in a had an identical Tree in b.
            return true;
        }

        /* Overloads != operator for Forest type.
         * Two Forest are not equal if there exists 
         * at least one non-identical Tree.
         */
        public static bool operator !=(Forest<T> a, Forest<T> b)
        {
            return !(a == b);
        }

        /* Overrides Equals() - same functionality as ==.
         */
        public override bool Equals(object other)
        {
            return this == (Forest<T>)other;
        }

        /* Overrides GetHashCode - same functionality as base.GetHashCode().
         * Overridden to make Visual Studio happy.
         */
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
