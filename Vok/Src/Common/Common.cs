﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using Vok.Src.UI;
using Vok.Src.Database;

namespace Vok.Src.Common
{
    /* Various constants, enums, and extension methods for Vok.
     */
    static class Common
    {
        // Database constants.
        public const char ENTRY_DELIM = ';';
        public const char DIRECTORY_DELIM = '/';
        public const char COMMAND_DELIM = ' ';
        public const string TEST_DB = "../../Validation/testDB.mdb";
        public const string TEST_CONNECTION = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + TEST_DB + ";Jet OLEDB:Database Password=dr@gonwe11";
        public const string VOK_DB = "VokDB.mdb";
        public const string VOK_CONNECTION = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + VOK_DB + ";Jet OLEDB:Database Password=dr@gonwe11";
        public const string TERM_TABLE_TEMPLATE = "Terms_Template";
        public const string CHARACTERISTICS_TABLE_TEMPLATE = "Characteristics_Template";
        public const string NEW_LANGUAGE = "New Language";
        // Database static variables.
        public static string dbName = VOK_DB;
        public static string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + dbName + ";Jet OLEDB:Database Password=dr@gonwe11";
        public static Database.Database currentDB;
        public static Test.Test currentTest;

        // UI constants.
        public readonly static ColorPalette LIGHT_MODE =
            new ColorPalette(backDefault: Color.FromArgb(235, 235, 245), backAccent: Color.FromArgb(225, 225, 235),
                             backDark: Color.FromArgb(215, 215, 225), foreDefault: Color.FromArgb(10, 10, 15),
                             foreAccent: Color.FromArgb(20, 20, 25), foreLight: Color.FromArgb(30, 30, 35));
        public readonly static ColorPalette DARK_MODE =
            new ColorPalette(backDefault: Color.FromArgb(30, 30, 35), backAccent: Color.FromArgb(20, 20, 25),
                             backDark: Color.FromArgb(10, 10, 15), foreDefault: Color.FromArgb(215, 215, 225),
                             foreAccent: Color.FromArgb(225, 225, 235), foreLight: Color.FromArgb(235, 235, 245));
        public readonly static Font DEFAULT_FONT = new Font("DengXian", 14);
        public readonly static Padding DEFAULT_PADDING = new Padding(10);
        public readonly static Padding DEFAULT_MARGIN = new Padding(20);
        public readonly static Size MINIMUM_WIN_SIZE = new Size(1080, 720);
        public readonly static Size MINIMUM_POPUP_SIZE = new Size(720, 480);
        public const string PAGE_PANEL_NAME = "pnlPage";
        public readonly static string ENTER = Environment.NewLine;
        // UI static variables.
        public static Form currentForm;
        public static Page currentPage;
        public static ColorPalette currentPalette = LIGHT_MODE;
        public static bool LeftToRight = true;

        public const int NUM_FIELD_NAMES = 8;
        public enum DBFieldNames
        {
            ID, Term, Definitions, Part_of_Speech,
            Characteristics, Special, Number_Times_Studied,
            Number_Times_Correct
        }
        public static IReadOnlyList<string> DB_FIELD_NAMES { get; } = Enum.GetNames(typeof(DBFieldNames));
        public static IReadOnlyList<string> SEARCH_COMMAND_FIELDS { get; } = searchCommandFields();
        private static List<string> searchCommandFields()
        {
            List<string> list = Enum.GetNames(typeof(DBFieldNames)).ToList();
            list.Add("Percentage_Correct");
            return list;
        }

        /* Represents possible parts of speech of a Word.
         */
        public enum PartOfSpeech
        {
            Adjective, Adverb, Conjunction, Idiom,
            Interjection, MeasureWord, ModalVerb, Noun,
            Numeral, Particle, ProperNoun, Pronoun,
            Prefix, Preposition, QuestionParticle, QuestionPronoun,
            TimeWord, Verb, VerbPlusComplement, VerbPlusObject
        }

        public enum NextPageCode
        {
            SearchDictionary, EditDictionary, RemoveFromDictionary,
            Flashcards, FlashcardsDraw, FillInTheBlanks
        }

        public static IReadOnlyList<string> TEST_TERMS_OPTIONS { get; } = new List<string>() { "Random", "Least Studied", "Most Missed" };
        public static IReadOnlyList<string> TEST_OPTIONS { get; } = new List<string>() { "Flashcards", "Flashcards Draw", "Fill in the Blanks" };

        /* Converts a string to title case.
         * Input: "this is a title"
         * Output: "This Is A Title"
         */ 
        public static string ToTitleCase(this string str)
        {
            string[] words = str.Split(' ');
            string converted = "";

            foreach (string word in words)
            {
                converted += word.Substring(0, 1).ToUpper();
                converted += word.Substring(1).ToLower();
                converted += " ";
            }

            // Removes final space.
            converted = converted.Substring(0, converted.Length - 1);

            return converted;
        }

        /* Converts a string to a nullable PartOfSpeech.
         * Returns the appropriate PartOfSpeech if the string can be converted,
         * returns null if a corresponding PartOfSpeech cannot be found.
         */ 
        public static PartOfSpeech? ToPartOfSpeech(this string str)
        {
            if (str is null || str.Trim().Equals(""))
            {
                return null;
            }

            // Converts string to format used for PartOfSpeech values.
            string[] words = str.ToTitleCase().Split(' ');
            string converted = "";
            foreach (string word in words)
            {
                converted += word;
            }

            PartOfSpeech? part;

            // Converts string to PartOfSpeech, if the appropriate value exists.
            try
            {
                part = (PartOfSpeech)Enum.Parse(typeof(PartOfSpeech), converted);
            }
            catch (ArgumentException)
            {
                part = null;
            }

            return part;
        }

        /* Converts a nullable PartOfSpeech into a string.
         * Returns null if passed null,
         * returns the part of speech with spaces inserted between words 
         * if passed a valid part of speech
         */ 
        public static string ToStringName(this PartOfSpeech? part)
        {
            if (part is null)
            {
                return null;
            }

            string str = Enum.GetName(typeof(PartOfSpeech), part);
            
            // Adds space before capital letters (except for the first)
            for (int index = 1; index < str.Length; index++)
            {
                if (str[index] >= 'A' && str[index] <= 'Z')
                {
                    string newstr = str.Substring(0, index) + " " + str.Substring(index);
                    str = newstr;
                    index++;
                }
            }

            return str;
        }

        /* Removes null, empty, and whitespace-only strings 
         * from a list of strings.
         */ 
        public static void Clean(this List<string> list)
        {
            for (int index = 0; index < list.Count; index++)
            {
                bool remove = false;
                if (list[index] is null)
                {
                    remove = true;
                }
                else
                {
                    string temp = list[index].Trim();
                    if (temp.Equals(""))
                    {
                        remove = true;
                    }
                }

                if (remove)
                {
                    list.RemoveAt(index);
                    index--;
                }
            }
        }

        /* Adds an extra apostrophe after every instance 
         * of an apostrophe in this string.
         */
        public static string DoubleApostrophe(this string str)
        {
            string output = "";

            if (str != null)
            {
                string[] vals = str.Split('\'');
                foreach (string val in vals)
                {
                    output += val + "''";
                }

                output = output.Substring(0, output.Length - 2);
            }

            return output;
        }

        /* Tries parsing an object to a string.
         * If this parse cannot happen, the string is set to "".
         */
        public static string ParseTextField(object ob)
        {
            string str;
            try
            {
                str = (string)ob;
            }
            catch (InvalidCastException)
            {
                str = "";
            }

            return str;
        }

        /* Tries parsing an object to an int.
         * If this parse cannot happen, 0 is returned.
         */
        public static int ParseNumberField(object ob)
        {
            int val;
            try
            {
                val = (int)ob;
            }
            catch (InvalidCastException)
            {
                val = 0;
            }

            return val;
        }

        /* Converts a list of strings to a single string in a tuple format,
         * as needed for an SQL insert into statement.
         */
        public static string ToTupleString(IEnumerable<string> list)
        {
            string str = "(";

            foreach (string val in list)
            {
                str += val + ", ";
            }

            str = str.Substring(0, str.Length - 2);
            str += ")";

            return str;
        }

        /* Changes various appearance properties of a control
         * to match the Vok default.
         */
        public static void SetDefaultAppearance(this Control control)
        {
            control.BackColor = currentPalette.BackDefault;
            control.ForeColor = currentPalette.ForeDefault;

            control.Font = DEFAULT_FONT;
            control.Padding = DEFAULT_PADDING;
            control.Margin = DEFAULT_MARGIN;

            if (control is UI.Controls.DrawingSurface ds)
            {
                ds.PenColor = currentPalette.ForeDefault;
            }
        }

        /* Helper function to convert a word to a string,
         * formatted to display as a search result on a page.
         */
        public static string FormatSearchResult(Word word)
        {
            if (word is null)
            {
                return null;
            }

            // Term (Part of speech) *
            // (Part of speech) not added if there is not part of speech
            // * not added if special == false
            string term = word.Term + (word.Part is null ? " " : " (" + word.Part + ") ") +
                         (word.Special ? "*" : "") + Environment.NewLine;

            // Add alternative spelling types if they exist
            // Spelling type: spelling; Spelling type: spelling; ...
            if (word.AlternativeSpellings.Count > 0)
            {
                foreach (KeyValuePair<string, string> entry in word.AlternativeSpellings)
                {
                    // Don't add entry if the value is empty.
                    if (!(entry.Value is null || entry.Value.Equals("")))
                    {
                        term += entry.Key + ": " + entry.Value + ENTRY_DELIM + " ";
                    }
                }
                term += Environment.NewLine;
            }

            // Definition; definition; ...
            foreach (string definition in word.Definitions)
            {
                if (!(definition is null || definition.Equals("")))
                {
                    term += definition + ENTRY_DELIM + " ";
                }
            }
            term += Environment.NewLine;

            // Add characteristics if they exist
            // Characteristic; Characteristic; ...
            if (word.Characteristics.Count > 0)
            {
                foreach (string characteristic in word.Characteristics)
                {
                    term += characteristic + ENTRY_DELIM + " ";
                }
                term += Environment.NewLine;
            }

            // Number of times studied: x; Percent correct: xx.xx
            // Note that percent correct is presented as a percentage,
            // rounded to two decimal places.
            term += "Number of times studied: " + word.NumTimesStudied + ENTRY_DELIM +
                    " Percent correct: " + Math.Round(word.PercentageCorrect() * 100, 2) + "%";

            return term;
        }
    }
}
