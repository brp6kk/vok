﻿using System.Drawing;
using System.Windows.Forms;
using static Vok.Src.Common.Common;

namespace Vok.Src.Common
{
    /* Represents a color palette.
     * A color palette is split into two components:
     *  - BackColor
     *  - ForeColor
     * both of which each have three components:
     *  - Default
     *  - Accent
     *  - Dark (BackColor)/Light (ForeColor)
     */ 
    class ColorPalette
    {
        /* Properties.
         */ 
        public Color BackDefault { get; private set; }
        public Color BackAccent { get; private set; }
        public Color BackDark { get; private set; }
        public Color ForeDefault { get; private set; }
        public Color ForeAccent { get; private set; }
        public Color ForeLight { get; private set; }

        /* Constructor.
         * Parameters:
         *  - backDefault - default (lightest) background color
         *  - backAccent - color slightly darker than backDefault,
         *    meant to be accent color
         *  - backDark - color slightly darker than backAccent,
         *    meant to be used for sharp contrast
         *  - foreDefault - default (darkest) background color
         *  - foreAccent - color slightly lighter than foreDefault,
         *    meant to be accent color
         *  - foreLight - color slightly lighter than foreAccent,
         *    meant to be used for sharp contrast
         */
        public ColorPalette(Color backDefault, Color backAccent, Color backDark, 
                            Color foreDefault, Color foreAccent, Color foreLight)
        {
            BackDefault = backDefault;
            BackAccent = backAccent;
            BackDark = backDark;
            ForeDefault = foreDefault;
            ForeAccent = foreAccent;
            ForeLight = foreLight;
        }

        /* If control has colors in oldPalette, switches these colors to
         * corresponding ones in newPalette.
         */
        private static void SwitchColorPalette(Control control, ColorPalette oldPalette,
                                              ColorPalette newPalette)
        {
            // Change BackColor.
            if (control.BackColor.Equals(oldPalette.BackDefault))
            {
                control.BackColor = newPalette.BackDefault;
            }
            else if (control.BackColor.Equals(oldPalette.BackAccent))
            {
                control.BackColor = newPalette.BackAccent;
            }
            else if (control.BackColor.Equals(oldPalette.BackDark))
            {
                control.BackColor = newPalette.BackDark;
            }

            // Change ForeColor.
            if (control.ForeColor.Equals(oldPalette.ForeDefault))
            {
                control.ForeColor = newPalette.ForeDefault;
            }
            else if (control.ForeColor.Equals(oldPalette.ForeAccent))
            {
                control.ForeColor = newPalette.ForeAccent;
            }
            else if (control.ForeColor.Equals(oldPalette.ForeLight))
            {
                control.ForeColor = newPalette.ForeLight;
            }

            // Update all child controls.
            foreach (Control c in control.Controls)
            {
                SwitchColorPalette(c, oldPalette, newPalette);
            }

            // Menustrip items need to be changed manually
            // since they are not considered controls.
            if (control is MenuStrip strip && strip.Items.Count > 0)
            {
                // Get the colors that item would update to
                // if it were a control, and change to the color.
                Control temp = new Control() { ForeColor = strip.Items[0].ForeColor, BackColor = strip.Items[0].BackColor };
                SwitchColorPalette(temp, oldPalette, newPalette);
                foreach (ToolStripMenuItem item in strip.Items)
                {
                    item.ForeColor = temp.ForeColor;
                }
            }

            if (control is UI.Controls.DrawingSurface ds)
            {
                ds.PenColor = newPalette.ForeDefault;
                ds.Invalidate();
            }
        }

        /* Changes colors of all open forms of Vok to newPalette.
         */ 
        public static void UpdateColorPalette(ColorPalette newPalette)
        {
            if (newPalette != null)
            {
                foreach (Form f in Application.OpenForms)
                {
                    SwitchColorPalette(f, currentPalette, newPalette);
                }

                currentPalette = newPalette;
            }
        }
    }
}
