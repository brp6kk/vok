﻿using System.Collections.Generic;

namespace Vok.Src.Common
{
    /* Container that stores a value and several children Trees.
     * Duplicate values are not allowed.
     */ 
    class Tree<T>
    {
        public T Node { get; set; }
        public List<Tree<T>> Children { get; private set; }

        /* Constructor.
         */
        public Tree(T node, params Tree<T>[] children)
        {
            Node = node;

            Children = new List<Tree<T>>();
            foreach (Tree<T> child in children)
            {
                AddChild(child);
            }
        }

        /* Adds child to Tree if no nodes in child exist in Tree.
         */ 
        public void AddChild(Tree<T> child)
        {
            if (child != null)
            {
                bool found = false;

                // Ensures that no elements of child are in this Tree.
                List<T> vals = child.ToList();
                for (int index = 0; index < vals.Count && !found; index++)
                {
                    if (Search(vals[index]) != null)
                    {
                        found = true;
                    }
                }

                if (!found)
                {
                    Children.Add(child);
                }
            }
        }

        /* Removes a child from a Tree.
         * Removes the full child, not just the node.
         * The child has to match an exact subtree.
         */ 
        public void RemoveChild(Tree<T> child)
        {
            if (child != null)
            {
                Tree<T> val = Search(child.Node);
                if (val != null && val == child)
                {
                    Children.Remove(val);
                }
            }
        }

        /* Searches Tree for val.
         * If val is found, the subtree with Node val is returned.
         * If val is not found, null is returned.
         */ 
        public Tree<T> Search(T val)
        {
            // Checks Node.
            if (Node.Equals(val))
            {
                return this;
            }

            // Checks all children.
            foreach (Tree<T> child in Children)
            {
                Tree<T> check = child.Search(val);

                if (check != null)
                {
                    return check;
                }
            }

            // val never found.
            return null;
        }

        /* Converts a Tree to a list of Nodes.
         * List is converted depth-first, for example:
         *  - Input: new Tree<int>(1, new Tree<int>(2, new Tree<int>(3), new Tree<int>(4)), new Tree<int>(5, new Tree<int>(6))
         *  - Output: { 1, 2, 3, 4, 5, 6 }
         */ 
        public List<T> ToList()
        {
            List<T> list = new List<T>() { Node };

            // Converts all children to lists and adds them to above list.
            foreach(Tree<T> child in Children)
            {
                list.AddRange(child.ToList());
            }
            
            return list;
        }

        /* Converts a Tree to a list of Nodes.
         * List is converted depth-first, for example:
         *  - Input: new Tree<int>(1, new Tree<int>(2, new Tree<int>(3), new Tree<int>(4)), new Tree<int>(5, new Tree<int>(6))
         *  - Output: { "/1", "/1/2", "/1/2/3", "/1/2/4", "/1/5", "/1/5/6" }
         */
        public List<string> ToListWithDirectories()
        {
            List<string> list = ToListWithDirectories("");
            list.Sort();
            return list;
        }

        /* Helper for ToListWithDirectories()
         * Ensures that all preceding directories are included with each Node.
         */ 
        private List<string> ToListWithDirectories(string header)
        {
            string str = header + Common.DIRECTORY_DELIM + Node.ToString();
            List<string> list = new List<string>() { str };

            // Converts all children to lists with the appropriate header
            // and adds them to the above list.
            foreach(Tree<T> child in Children)
            {
                list.AddRange(child.ToListWithDirectories(str));
            }

            return list;
        }

        /* Overloads == operator for Tree type.
         * Two Trees are equal if all nodes are identical.
         */ 
        public static bool operator ==(Tree<T> a, Tree<T> b)
        {
            // Null checks.
            if (a is null && b is null)
            {
                return true;
            }
            else if (a is null || b is null)
            {
                return false;
            }

            // Trees aren't equal if nodes aren't equal.
            if (!a.Node.Equals(b.Node))
            {
                return false;
            }

            // Trees aren't equal if number of children aren't equal.
            if (a.Children.Count != b.Children.Count)
            {
                return false;
            }

            // Goes through all children to see if all nodes are equal.
            for (int index = 0; index < a.Children.Count; index++)
            {
                if (!(a.Children[index] == b.Children[index]))
                {
                    return false;
                }
            }

            // All above checks passed - trees are equal.
            return true;
        }

        /* Overloads != operator for Tree type.
         * Two Trees are not equal if there exists 
         * at least one non-identical node.
         */
        public static bool operator !=(Tree<T> a, Tree<T> b)
        {
            return !(a == b);
        }

        /* Overrides Equals() - same functionality as ==.
         */ 
        public override bool Equals(object other)
        {
            return this == (Tree<T>)other;
        }

        /* Overrides GetHashCode - same functionality as base.GetHashCode().
         * Overridden to make Visual Studio happy.
         */ 
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
