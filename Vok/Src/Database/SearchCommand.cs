﻿using System.Collections.Generic;
using System.Linq;
using static Vok.Src.Common.Common;

namespace Vok.Src.Database
{
    /* Represents a legal search command 
     * that can be executed on the external database.
     * Only legal search commands can be created, 
     * which is enforced via the CreateSearchCommand method
     * (the only way to create a SearchCommand).
     * This class also includes methods to create SQL from
     * a collection of SearchCommands.
     */ 
    class SearchCommand
    {
        public string SearchFunction { get; private set; }
        public string FieldName { get; private set; }
        public string ComparisonType { get; private set; }
        public string SearchValue { get; private set; }

        public static string[] LegalSearchFunctions { get; } = { "Count", "Len", "Highest", "Lowest", "Random"};
        public static string[] LegalComparisonTypes { get; } = { "~", "=", "<>", ">", "<", ">=", "<=" };
        private static string percentageCorrect = "(Iif(Number_Times_Studied=0, 0, Number_Times_Correct/Number_Times_Studied))";

        /* Constructor.
         * Private to ensure that only a valid SearchCommand can be created.
         */
        private SearchCommand(string searchFunction, string fieldName, 
                              string comparisonType, string searchValue)
        {
            SearchFunction = searchFunction;
            FieldName = fieldName;
            ComparisonType = comparisonType;
            SearchValue = searchValue;
        } 

        /* Pseudo-constructor.
         */ 
        public static SearchCommand CreateSearchCommand(string fieldName, string comparisonType, 
                                                        string searchValue)
        {
            return CreateSearchCommand(null, fieldName, comparisonType, searchValue);
        }

        /* Pseudo-constructor with optional search function.
         */ 
        public static SearchCommand CreateSearchCommand(string searchFunction, string fieldName, 
                                                        string comparisonType, string searchValue)
        {
            if (IsLegalSearchCommand(searchFunction, fieldName, comparisonType, searchValue))
            {
                return new SearchCommand(searchFunction, fieldName, comparisonType, searchValue);
            }
            else
            {
                return null;
            }
        }

        /* Determines if a search command with the passed values is allowed.
         * Only certain values for searchFunction, fieldName, and comparisonType are legal
         * and depending on those values, only certain search values are legal.
         */ 
        public static bool IsLegalSearchCommand(string searchFunction, string fieldName, 
                                                string comparisonType, string searchValue)
        {
            // Null values are illegal for all parameters except for search function.
            if (fieldName is null ||  comparisonType is null || searchValue is null)
            {
                return false;
            }

            // Either illegal search function, illegal field name, or illegal comparison type.
            if (!(searchFunction is null || LegalSearchFunctions.Contains(searchFunction)) ||
                !(SEARCH_COMMAND_FIELDS.Contains(fieldName) || (currentDB != null && currentDB.AlternativeSpellingTypes.Contains(fieldName))) ||
                !LegalComparisonTypes.Contains(comparisonType))
            {
                return false;
            }

            int num;
            bool test = false;

            // Local functions to help with switch statement.
            bool validComparisonType(int first, int count) =>
                (new List<string>(LegalComparisonTypes).GetRange(first, count)).Contains(comparisonType);
            bool validSearchFunction(int first, int count) =>
                searchFunction != null && (new List<string>(LegalSearchFunctions).GetRange(first, count)).Contains(searchFunction);
            bool properValsForFunction() =>
                validComparisonType(1, 6) && int.TryParse(searchValue, out num);

            // LegalSearchFunctions = { "Count", "Len", "Highest", "Lowest", "Random"}
            // LegalComparisonTypes = { "~", "=", "<>", ">", "<", ">=", "<=" }

            // Tests for one of the following:
            // 1. Valid comparison type and search value for field name
            // 2. Valid function for field name,
            //    and valid comparison type and search value for function.
            switch (fieldName)
            {
                case "Term":
                    test = searchFunction is null || (validSearchFunction(1, 3) && properValsForFunction());
                    break;
                case "Definitions":
                    test = (searchFunction is null && validComparisonType(0, 3)) ||
                           (validSearchFunction(0, 1) && properValsForFunction());
                    break;
                case "Part_of_Speech":
                    test = (searchFunction is null && validComparisonType(0, 3) && searchValue.ToPartOfSpeech() != null) ||
                           (validSearchFunction(1, 1) && properValsForFunction());
                    break;
                case "Characteristics":
                    test = (searchFunction is null && validComparisonType(0, 3)) ||
                           (validSearchFunction(0, 1) && properValsForFunction());
                    break;
                case "Special":
                    test = searchFunction is null && validComparisonType(1, 2) && bool.TryParse(searchValue, out bool result);
                    break;
                case "Number_Times_Studied":
                case "Number_Times_Correct":
                    test = (searchFunction is null && validComparisonType(1, 6) && int.TryParse(searchValue, out num)) ||
                           (validSearchFunction(2, 2) && properValsForFunction());
                    break;
                case "Percentage_Correct":
                    test = (searchFunction is null && validComparisonType(1, 6) && double.TryParse(searchValue, out double percent)) ||
                           (validSearchFunction(2, 2) && properValsForFunction());
                    break;
                default: // Alternative spelling types
                    test = searchFunction is null || (validSearchFunction(1, 3) && properValsForFunction());
                    break;
            }

            // Checks random function.
            test = test || (LegalSearchFunctions[4].Equals(searchFunction) && int.TryParse(searchValue, out num));

            return test;
        }

        /* Converts this SearchCommand to a component of an SQL where clause.
         * Note that the keyword "where" is not inserted 
         * to make it easier to combine multiple search commands.
         */ 
        public string ToSQLWhere()
        {
            // Highest, Lowest, and Random cannot be converted to component of where clause.
            if (LegalSearchFunctions[2].Equals(SearchFunction) || LegalSearchFunctions[3].Equals(SearchFunction) || 
                LegalSearchFunctions[4].Equals(SearchFunction))
            {
                return null;
            }

            string str = "";

            if (SEARCH_COMMAND_FIELDS[8].Equals(FieldName)) // Percentage_Correct
            {
                FieldName = percentageCorrect;
            }

            if (LegalSearchFunctions[0].Equals(SearchFunction)) // Count
            {
                int val = int.Parse(SearchValue);
                if (val-- > 0)
                {
                    str = "(Len(" + FieldName + ") - Len(Replace(" + FieldName + ", '" + ENTRY_DELIM + "', '')))=" + val;
                }
                else
                {
                    str = FieldName + "=''";
                }
            }
            else if (LegalSearchFunctions[1].Equals(SearchFunction)) // Len
            {
                str = SearchFunction + "(" + FieldName + ")" + ComparisonType + SearchValue;
            }
            else // No function
            {
                str = FieldName;
                if (ComparisonType.Equals("~"))
                {
                    str += " like '%" + SearchValue + "%'";
                }
                else
                {
                    str += ComparisonType;
                    if (FieldName.Equals("Special") || FieldName.Equals("Number_Times_Studied") || 
                        FieldName.Equals("Number_Times_Correct") || FieldName.Equals(percentageCorrect))
                    {
                        str += SearchValue;
                    }
                    else // String search value
                    {
                        str += "'" + SearchValue + "'";
                    }
                }
            }

            return str;
        }

        /* Converts a collection of SearchCommands into a full SQL command string.
         */ 
        public static string ToSQL(IEnumerable<SearchCommand> commands)
        {
            if (commands is null)
            {
                return null;
            }

            SearchCommand highestlowest = null;
            List<string> sqlWhere = new List<string>();

            // Convert all search commands to SQL where clause component
            // The first Highest or Lowest function found is stored in highestlowest.
            foreach (SearchCommand cmd in commands)
            {
                if (cmd is null)
                {
                    return null;
                }

                string clause = cmd.ToSQLWhere();

                if (clause is null && highestlowest is null)
                {
                    highestlowest = cmd;
                }
                else if (clause != null)
                {
                    sqlWhere.Add(clause);
                }
            }

            string fullClause = "";

            // Builds clause with components chained by 'and'
            if (sqlWhere.Count > 0)
            {
                foreach (string str in sqlWhere)
                {
                    fullClause += str + " and ";
                }
                fullClause = " where " + fullClause.Substring(0, fullClause.Length - 5);
            }

            string command = "";
            // Simple select.
            if (highestlowest is null)
            {
                command = "Select * from " + currentDB.Language + "_Terms" + fullClause;
            }
            // Select highest/lowest/random number of values.
            else
            {
                if (highestlowest.SearchFunction.Equals(LegalSearchFunctions[4])) // Random
                {
                    highestlowest.FieldName = "rnd(int(now*ID)-now*ID)";
                }
                if (highestlowest.FieldName.Equals(SEARCH_COMMAND_FIELDS[8])) // Percentage_Correct
                {
                    highestlowest.FieldName = percentageCorrect;
                }

                // "Select top 3 * from ChineseA_Terms where Special=true and Part_of_Speech='Verb' order by Number_Times_Studied desc";
                command = "Select top " + highestlowest.SearchValue + " * from " + currentDB.Language +
                          "_Terms" + fullClause + " order by " + highestlowest.FieldName;
                
                if (highestlowest.SearchFunction.Equals("Highest"))
                {
                    command += " desc";
                }
            }

            return command;
        }
    }
}
