﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using System.Data.Common;
using Vok.Src.Common;
using static Vok.Src.Common.Common;

namespace Vok.Src.Database
{
    /* Provides mechanisms in which an in-code Database
     * queries and edits the external database.
     */ 
    static class FileIO
    {
        /* Checks that the database file actually exists,
         * checking the same directory as the executable.
         */ 
        public static bool DatabaseExists()
        {
            return File.Exists(dbName);
        }

        /* Queries the database as defined by the sql parameter, 
         * returning a list of Words that meet the query.
         */ 
        public static async Task<List<Word>> QueryDatabaseAsync(string sql)
        {
            OleDbConnection connection = new OleDbConnection(connectionString);
            OleDbCommand command;
            DbDataReader reader;
            List<Word> words = new List<Word>();

            try
            {
                connection.Open();
                command = new OleDbCommand(sql, connection);

                reader = await command.ExecuteReaderAsync();

                // Gets alternative spelling types.
                DataTable schemaTable = reader.GetSchemaTable();
                List<string> types = new List<string>();
                for (int index = NUM_FIELD_NAMES; index < schemaTable.Rows.Count; index++)
                {
                    types.Add(schemaTable.Rows[index].Field<string>("ColumnName"));
                }

                while (reader.Read())
                {
                    words.Add(RowToWord(reader, types));
                }

                command.Connection.Close();
            }
            catch (OleDbException exp)
            {
                System.Console.WriteLine(exp);
                words = null;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return words;
        }

        /* Edits the database as defined by the sql parameter.
         */
        public static async Task EditDatabaseAsync(string sql)
        {
            OleDbConnection connection = new OleDbConnection(connectionString);
            OleDbCommand command;

            try
            {
                connection.Open();
                command = new OleDbCommand(sql, connection);

                await command.ExecuteNonQueryAsync();
                command.Connection.Close();
            }
            catch (OleDbException) { }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        /* Backs up the terms and characteristics tables for the passed language.
         */
        public static async Task BackupDatabaseAsync(string language)
        {
            // Remove old backup table, if it exists.
            string sql = "Drop table " + language + "_Backup_Terms";
            await EditDatabaseAsync(sql);
            sql = "Drop table " + language + "_Backup_Characteristics";
            await EditDatabaseAsync(sql);

            // Backup current language state.
            // If language doesn't exist, nothing will happen.
            sql = "Select * into " + language + "_Backup_Terms from " + language + "_Terms";
            await EditDatabaseAsync(sql);
            sql = "Select * into " + language + "_Backup_Characteristics from " + language + "_Characteristics";
            await EditDatabaseAsync(sql);
        }
        
        /* Resets the language database to be identical to the state
         * of the backup database.
         */ 
        public static async Task ResetDatabaseAsync(string language)
        {
            // Ensure backup exists.
            string sql = "Select * from " + language + "_Backup_Terms";
            List<Word> words = await QueryDatabaseAsync(sql);
            bool check = words != null;
            sql = "Select * from " + language + "_Backup_Characteristics";
            Forest<string> characteristics = await GetCharacteristicsAsync(language + "_Backup");
            check = check && characteristics != null;

            if (check)
            {
                // Remove language table.
                sql = "Drop table " + language + "_Terms";
                await EditDatabaseAsync(sql);
                sql = "Drop table " + language + "_Characteristics";
                await EditDatabaseAsync(sql);

                // Set backup as language table.
                sql = "Select * into " + language + "_Terms from " + language + "_Backup_Terms";
                await EditDatabaseAsync(sql);
                sql = "Select * into " + language + "_Characteristics from " + language + "_Backup_Characteristics";
                await EditDatabaseAsync(sql);
            }
        }

        /* Gets the names of the different alternative spelling types for the passed language.
         */ 
        public static async Task<List<string>> GetAlternativeSpellingTypesAsync(string language)
        {
            string sql = "Select * from " + language + "_Terms";
            OleDbConnection connection = new OleDbConnection(connectionString);
            OleDbCommand command;
            DbDataReader reader;
            List<string> types = new List<string>();

            try
            {
                connection.Open();
                command = new OleDbCommand(sql, connection);

                reader = await command.ExecuteReaderAsync();

                // Gets alternative spelling types.
                DataTable schemaTable = reader.GetSchemaTable();
                for (int index = NUM_FIELD_NAMES; index < schemaTable.Rows.Count; index++)
                {
                    types.Add(schemaTable.Rows[index].Field<string>("ColumnName"));
                }

                command.Connection.Close();
            }
            catch (OleDbException)
            {
                types = null;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return types;
        }

        /* Gets the characteristics of the passed language.
         */ 
        public static async Task<Forest<string>> GetCharacteristicsAsync(string language)
        {
            string sql = "Select * from " + language + "_Characteristics";
            OleDbConnection connection = new OleDbConnection(connectionString);
            OleDbCommand command;
            DbDataReader reader;
            List<string> vals = new List<string>();
            Forest<string> characteristics = new Forest<string>();

            try
            {
                connection.Open();
                command = new OleDbCommand(sql, connection);

                reader = await command.ExecuteReaderAsync();

                while (reader.Read())
                {
                    vals.Add(ParseTextField(reader.GetValue(1)));
                }
                characteristics = Forest<string>.ListToForest(vals);

                command.Connection.Close();
            }
            catch (OleDbException)
            {
                characteristics = null;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return characteristics;
        }

        /* Creates a new language database,
         * assuming a database with the given language
         * does not already exist.
         */ 
        public static async Task NewLanguageAsync(string language)
        {
            if (language != null && !ExistingLanguages().Contains(language))
            {
                // Remove old table, if it exists.
                // Note that even though ExistingLanguages check passed,
                // one of these tables may still exist.
                string sql = "Drop table " + language + "_Terms";
                await EditDatabaseAsync(sql);
                sql = "Drop table " + language + "_Characteristics";
                await EditDatabaseAsync(sql);

                // Create new language database
                sql = "Select * into " + language + "_Terms from " + TERM_TABLE_TEMPLATE + "";
                await EditDatabaseAsync(sql);
                sql = "Select * into " + language + "_Characteristics from " + CHARACTERISTICS_TABLE_TEMPLATE + "";
                await EditDatabaseAsync(sql);
            }
        }

        /* Returns a list of strings where each string
         * is the name of a language in the database.
         */ 
        public static List<string> ExistingLanguages()
        {
            // Code in try block based on:
            // https://www.c-sharpcorner.com/uploadfile/vendettamit/how-to-get-list-of-tables-in-ms-access-using-C-Sharp-code/
            OleDbConnection connection = new OleDbConnection(connectionString);
            List<string> vals = new List<string>();

            try
            {
                connection.Open();

                DataTable dt = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                foreach (DataRow row in dt.Rows)
                {
                    string tableName = row["TABLE_NAME"].ToString();
                    if (row["TABLE_TYPE"].ToString() == "TABLE")
                    {
                        vals.Add(tableName);
                    }
                }

                vals = GetLanguages(vals);
            }
            catch (OleDbException)
            {
                vals = null;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return vals;
        }

        /* Helper to go through a list of table names
         * and determine what the actual languages are.
         */ 
        private static List<string> GetLanguages(List<string> vals)
        {
            List<string> languages = null;

            if (vals != null)
            {
                languages = new List<string>();
                vals.Clean();

                foreach (string val in vals)
                {
                    if (val.Length > 6 && val.Substring(val.Length - 6).Equals("_Terms"))
                    {
                        languages.Add(val.Substring(0, val.Length - 6));
                    }
                }

                for (int index = 0; index < languages.Count; index++)
                {
                    if (!vals.Contains(languages[index] + "_Characteristics"))
                    {
                        languages.RemoveAt(index--);
                    }
                }
            }

            return languages;
        }

        /* Converts a row from a table to a Word.
         * Parameter types - List of alternative spelling types.
         */ 
        private static Word RowToWord(DbDataReader row, List<string> types)
        {
            // Obtains values.
            object[] fields = new object[NUM_FIELD_NAMES + types.Count];
            int numColumns = row.GetValues(fields);

            // Parses values.
            string term = ParseTextField(fields[(int)DBFieldNames.Term]);
            string[] definitions = ParseTextField(fields[(int)DBFieldNames.Definitions]).Split(ENTRY_DELIM);
            PartOfSpeech? part = ParseTextField(fields[(int)DBFieldNames.Part_of_Speech]).ToPartOfSpeech();
            string[] characteristics = ParseTextField(fields[(int)DBFieldNames.Characteristics]).Split(ENTRY_DELIM);
            bool special = (bool)fields[(int)DBFieldNames.Special];
            int numTimesStudied = ParseNumberField(fields[(int)DBFieldNames.Number_Times_Studied]);
            int numTimesCorrect = ParseNumberField(fields[(int)DBFieldNames.Number_Times_Correct]);
            List<string> typeVals = new List<string>();
            for (int index = 0; index < types.Count && index + NUM_FIELD_NAMES < fields.Length; index++)
            {
                typeVals.Add(ParseTextField(fields[index + NUM_FIELD_NAMES]));
            }

            // Creates word from values.
            Word word = new Word(term, part, special, numTimesStudied, numTimesCorrect);
            foreach (string def in definitions)
            {
                word.AddDefinition(def);
            }
            foreach (string characteristic in characteristics)
            {
                word.AddCharacteristic(characteristic);
            }
            for (int index = 0; index < types.Count && index < typeVals.Count; index++)
            {
                word.AddAlternativeSpelling(types[index], typeVals[index]);
            }

            return word;
        }
    }
}
