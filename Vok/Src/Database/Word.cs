﻿using System;
using System.Collections.Generic;
using static Vok.Src.Common.Common;

namespace Vok.Src.Database
{
    /* Represents a word, which has several expected properties including
     * a term, definitions, a part of speech, and alternative spellings.
     * Additionally, the number of times studied and the number of times correct
     * when studied are tracked; the special marker indicates that this word
     * needs special attention; characteristics are used to sort a word.
     */ 
    class Word : IComparable, ICloneable
    {
        public string Term { get; set; }
        public Dictionary<string, string> AlternativeSpellings { get; private set; }
        public List<string> Definitions { get; private set;}
        public PartOfSpeech? Part { get; private set; }
        public int NumTimesStudied { get; private set; }
        public int NumTimesCorrect { get; private set; }
        public bool Special { get; set; }
        public List<string> Characteristics { get; private set; }

        /* Constructor - only term is required, but part of speech can't be changed later.
         */ 
        public Word(string term, PartOfSpeech? part = null, bool special = false, int numTimesStudied = 0, int numTimesCorrect = 0)
        {
            Term = term;
            Part = part;
            Special = special;
            NumTimesStudied = numTimesStudied;
            NumTimesCorrect = numTimesCorrect;

            AlternativeSpellings = new Dictionary<string, string>();
            Definitions = new List<string>();
            Characteristics = new List<string>();
        }

        /* Adds an alternative spelling type type with value val 
         * to list of all alternative spellings.
         * Assumes that type is a valid spelling type.
         */ 
        public void AddAlternativeSpelling(string type, string val)
        {
            if (type != null && val != null)
            {
                AlternativeSpellings[type] = val;
            }
        }

        /* Removes an alternative spelling type and its associated value 
         * from the list of all alternative spellings.
         * If the type does not exist, no action occurs.
         */ 
        public void RemoveAlternativeSpelling(string type)
        {
            if (type != null)
            {
                AlternativeSpellings.Remove(type);
            }
        }

        /* Adds a definition to this Word.
         */ 
        public void AddDefinition(string definition)
        {
            if (definition != null)
            {
                string[] definitions = definition.Split(ENTRY_DELIM);
                foreach (string def in definitions)
                {
                    if (!Definitions.Exists(x => x.Equals(def)) && !"".Equals(def.Trim()))
                    {
                        Definitions.Add(def);
                    }
                }
            }
        }

        /* Removes a definition from this Word.
         * If the definition does not exist, no action occurs.
         */ 
        public void RemoveDefinition(string definition)
        {
            if (definition != null)
            {
                Definitions.Remove(definition);
            }
        }

        /* Adds a characteristic to the word.
         */
        public void AddCharacteristic(string val)
        {
            if (val != null && !ContainsCharacteristic(val))
            {
                Characteristics.Add(val);
            }
        }

        /* Returns true if this word contains the passed characteristic.
         * Returns false if this words does not contain it.
         */
        public bool ContainsCharacteristic(string val)
        {
            return Characteristics.Exists(x => x.Equals(val));
        }

        /* Removes a characteristic from a word.
         * If the characteristic does not exist, no action occurs.
         */
        public void RemoveCharacteristic(string val)
        {
            if (val != null)
            {
                Characteristics.Remove(val);
            }
        }

        /* Increments the counter representing the number of times
         * this word has been studied by one.
         */
        public void IncrementNumTimesStudied()
        {
            NumTimesStudied++;
        }

        /* Increments the counter representing the number of times
         * this word was correctly studied by one.
         */ 
        public void IncrementNumTimesCorrect()
        {
            NumTimesCorrect++;
        }

        /* Resets the counters representing the number of times 
         * this word has been studied and the number of times
         * this word was correctly studied to zero.
         */ 
        public void ResetScoreStats()
        {
            NumTimesStudied = 0;
            NumTimesCorrect = 0;
        }

        /* Calculates and returns the percentage that this word
         * was correctly studied based on the number of times studied.
         */ 
        public double PercentageCorrect()
        {
            return NumTimesStudied == 0 ? 0 : (double)NumTimesCorrect / NumTimesStudied;
        }

        /* Overloads == operator for the Word type.
         * Two words are equal if the term properties are identical.
         */ 
        public static bool operator ==(Word w1, Word w2)
        {
            // Null checks.
            if (w1 is null && w2 is null)
            {
                return true;
            }
            else if (w1 is null || w2 is null)
            {
                return false;
            }

            return w1.Term.Equals(w2.Term);
        }

        /* Overloads != operator for the Word type.
         * Two words are not equal if the term properties are not identical.
         */ 
        public static bool operator !=(Word w1, Word w2)
        {
            return !(w1 == w2);
        }

        /* Overloads < operator for the Word type.
         * Word A is less than Word B if A comes before B in the dictionary.
         */ 
        public static bool operator <(Word w1, Word w2)
        {
            if (w1 is null || w2 is null)
            {
                return false;
            }

            return w1.Term.CompareTo(w2.Term) < 0;
        }

        /* Overloads > operator for the Word type.
         * Word A is greater than Word B if A comes after B in the dictionary.
         */ 
        public static bool operator >(Word w1, Word w2)
        {
            if (w1 is null || w2 is null)
            {
                return false;
            }

            return w1.Term.CompareTo(w2.Term) > 0;
        }

        /* Overloads <= operator for the Word type.
         * A <= B -> A < B || A == B
         */ 
        public static bool operator <=(Word w1, Word w2)
        {
            return w1 == w2 || w1 < w2;
        }

        /* Overloads >= operator for the Word type.
         * A >= B -> A > B || A == B
         */ 
        public static bool operator >=(Word w1, Word w2)
        {
            return w1 == w2 || w1 > w2;
        }

        /* Implements CompareTo for the Word class.
         * Returns x:
         *  - x < 0  -> this < ob
         *  - x == 0 -> this == ob
         *  - x > 0  -> this > ob
         */ 
        public int CompareTo(object ob)
        {
            return this.Term.CompareTo(((Word)ob).Term);
        }

        /* Overrides Equals() - same functionality as ==.
         */
        public override bool Equals(object obj)
        {
            return this == (Word)obj;
        }

        /* Overrides GetHashCode - same functionality as base.GetHashCode().
         * Overridden to make Visual Studio happy.
         */
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /* Implements Clone for the Word class.
         * Returns deep copy of this.
         */ 
        public object Clone()
        {
            Word w = new Word(this.Term, this.Part, this.Special, this.NumTimesStudied, this.NumTimesCorrect);
            
            foreach (string definition in this.Definitions)
            {
                w.AddDefinition(definition);
            }

            foreach (string characteristic in this.Characteristics)
            {
                w.AddCharacteristic(characteristic);
            }

            foreach (string key in currentDB.AlternativeSpellingTypes)
            {
                try
                {
                    w.AddAlternativeSpelling(key, this.AlternativeSpellings[key]);
                }
                catch (KeyNotFoundException)
                {
                    w.AddAlternativeSpelling(key, "");
                }
            }

            return w;
        }
    }
}
