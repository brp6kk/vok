﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vok.Src.Common;
using static Vok.Src.Common.Common;

namespace Vok.Src.Database
{
    /* Represents a database consisting of the words in a language.
     * The actual words are stored in an external database, but
     * this class provides a mechanism to interface with this database
     * in code.
     * Within this class, the name of the language, a list of all
     * characteristics in the language, and a list of types of legal
     * alternative spellings are stored.
     */ 
    class Database
    {
        public string Language { get; private set; }
        public Forest<string> Characteristics { get; private set; }
        public List<string> AlternativeSpellingTypes { get; private set; }

        /* Constructor.
         */ 
        private Database(string language)
        {
            Language = language;
        }

        /* Creates a new Database and loads in its
         * characteristics and alternative spelling types.
         */ 
        public static async Task<Database> CreateDatabase(string language)
        {
            Database db = new Database(language);
            await db.LoadLanguageAsync();
            return db;
        }

        /* Gets the alternative spelling types and characteristics
         * associated with a language.
         */ 
        public async Task LoadLanguageAsync()
        {
            if (AlternativeSpellingTypes is null)
            {
                AlternativeSpellingTypes = await FileIO.GetAlternativeSpellingTypesAsync(Language);
            }

            if (Characteristics is null)
            {
                Characteristics = await FileIO.GetCharacteristicsAsync(Language);
            }
        }

        /* Adds a word to the language's external database.
         */ 
        public async Task AddWordAsync(Word newWord)
        {
            await AddWordsAsync(new List<Word>() { newWord });
        }

        /* Adds the words in a List to the language's external database.
         */ 
        public async Task AddWordsAsync(List<Word> newWords)
        {
            if (newWords != null)
            {
                foreach (Word word in newWords)
                {
                    if (word != null && !await DoesWordExist(word))
                    {
                        string sql = "Insert into " + Language + "_Terms ";
                        sql += FieldHeadersTuple();
                        sql += " values " + WordTuple(word);
                        await FileIO.EditDatabaseAsync(sql);
                    }
                }
            }
        }

        /* Removes a word from the language's external database.
         */ 
        public async Task RemoveWordAsync(Word oldWord)
        {
            await RemoveWordsAsync(new List<Word>() { oldWord });
        }

        /* Removes the words in a List from the language's external database.
         */ 
        public async Task RemoveWordsAsync(List<Word> oldWords)
        {
            if (oldWords != null)
            {
                foreach (Word word in oldWords)
                {
                    if (word != null)
                    {
                        string sql = "Delete from " + Language + "_Terms where ";
                        sql += "Term='" + word.Term.DoubleApostrophe() + "' and ";
                        sql += "Part_of_Speech='" + word.Part.ToStringName() + "' and ";

                        string definitions = "";
                        foreach (string def in word.Definitions)
                        {
                            definitions += def + ENTRY_DELIM;
                        }
                        definitions = definitions.Substring(0, definitions.Length - 1);
                        sql += "Definitions='" + definitions.DoubleApostrophe() + "'";

                        await FileIO.EditDatabaseAsync(sql);
                    }
                }
            }
        }
        
        /* Changes the data of oldWord to that in newWord in the external database.
         */ 
        public async Task EditWordAsync(Word oldWord, Word newWord)
        {
            if (oldWord != null && newWord != null && 
                await DoesWordExist(oldWord))// && !await DoesWordExist(newWord))
            {
                await RemoveWordAsync(oldWord);
                await AddWordAsync(newWord);
            }
        }

        /* Searches external database based on commands, returning a List of Words
         * that satisfy the commands.
         */ 
        public async Task<List<Word>> SearchAsync(IEnumerable<SearchCommand> commands)
        {
            // Null not allowed.
            if (commands is null || commands.Contains(null))
            {
                return null;
            }

            // Invalid SearchCommand(s)
            string sql = SearchCommand.ToSQL(commands);
            if (sql is null)
            {
                return null;
            }

            List<Word> results = await FileIO.QueryDatabaseAsync(sql);

            return results;
        }

        /* Returns num number of random Words in the external database, stored in a List.
         */ 
        public async Task<List<Word>> RandomTermsAsync(int num)
        {
            List<Word> list = null;

            if (num > 0)
            {
                SearchCommand command = SearchCommand.CreateSearchCommand("Random", "Term", "=", num.ToString());
                string sql = SearchCommand.ToSQL(new List<SearchCommand>() { command });
                list = await FileIO.QueryDatabaseAsync(sql);
            }

            return list;
        }

        /* Adds a characteristic to a database,
         * both in this class and the external database.
         */ 
        public async Task AddCharacteristicAsync(string characteristic)
        {
            if (characteristic != null && Characteristics.ValidNewDirectory(characteristic))
            {
                string sql = "Insert into " + Language + 
                             "_Characteristics (Characteristic) values('" + characteristic + "')";
                await FileIO.EditDatabaseAsync(sql);
                Characteristics = await FileIO.GetCharacteristicsAsync(Language);
            }
        }

        /* Removes a characteristic from a database,
         * both in this class and the external database.
         */ 
        public async Task RemoveCharacteristicAsync(string characteristic)
        {
            if (characteristic != null)
            {
                string sql = "Delete from " + Language + 
                             "_Characteristics where Characteristic='" + characteristic + "'";
                await FileIO.EditDatabaseAsync(sql);
                Characteristics = await FileIO.GetCharacteristicsAsync(Language);
            }
        }

        /* Adds an alternative spelling type to a database,
         * both in this class and the external database.
         */ 
        public async Task AddAlternativeSpellingTypeAsync(string type)
        {
            if (type != null && !AlternativeSpellingTypes.Contains(type))
            {
                string sql = "Alter table " + Language + "_Terms add " + type + " varchar(255)";
                await FileIO.EditDatabaseAsync(sql);
                AlternativeSpellingTypes.Add(type);
                
            }
        }

        /* Removes an alternative spelling type from a database,
         * both in this class and the external database.
         */ 
        public async Task RemoveAlternativeSpellingTypeAsync(string type)
        {
            if (type != null && AlternativeSpellingTypes.Contains(type))
            {
                string sql = "Alter table " + Language + "_Terms drop column " + type;
                await FileIO.EditDatabaseAsync(sql);
                AlternativeSpellingTypes.Remove(type);
            }
        }

        /* Converts a word to a string in a tuple format,
         * as needed for an SQL insert into statement.
         */ 
        private string WordTuple(Word word)
        {
            // "Term", "Definitions", "Part_of_Speech", "Characteristics", "Special", "Num_Times_Studied", "Num_Times_Correct"
            List<string> vals = new List<string>();

            vals.Add("'" + word.Term.DoubleApostrophe() + "'");

            string definitions = "";
            foreach (string def in word.Definitions)
            {
                definitions += def + ENTRY_DELIM;
            }
            definitions = definitions.Substring(0, definitions.Length - 1);
            vals.Add("'" + definitions.DoubleApostrophe() + "'");

            vals.Add("'" + word.Part.ToStringName() + "'");

            string characteristics = "";
            foreach (string chr in word.Characteristics)
            {
                characteristics += chr + ENTRY_DELIM;
            }
            if (characteristics.Length > 0)
            {
                characteristics = characteristics.Substring(0, characteristics.Length - 1);
            }

            vals.Add("'" + characteristics.DoubleApostrophe() + "'");

            vals.Add(word.Special.ToString());

            vals.Add(word.NumTimesStudied.ToString());

            vals.Add(word.NumTimesCorrect.ToString());

            foreach (string type in this.AlternativeSpellingTypes)
            {
                try
                {
                    vals.Add("'" + word.AlternativeSpellings[type].DoubleApostrophe() + "'");
                }
                catch (KeyNotFoundException)
                {
                    vals.Add("''");
                }
                catch (NullReferenceException)
                {
                    vals.Add("''");
                }
            }

            return ToTupleString(vals);
        }

        /* Converts field headers to a string in a tuple format,
         * as needed for an SQL insert into statement.
         */
        private string FieldHeadersTuple()
        {
            List<string> headers = DB_FIELD_NAMES.ToList();
            headers.RemoveAt(0); // Remove ID

            foreach (string type in AlternativeSpellingTypes)
            {
                headers.Add(type);
            }

            return ToTupleString(headers);
        }

        /* Determines if two database entries are equal.
         * This occurs when the terms and parts of speech are identical
         * and if all of the definitions in the first are found in the second.
         */ 
        private static bool IsEqualEntry(Word w1, Word w2)
        {
            bool result = false;

            if (w1 != null && w2 != null)
            {
                result = w1 == w2 && w1.Part == w2.Part;

                foreach (string def in w1.Definitions)
                {
                    result = result && w2.Definitions.Contains(def);
                }
            }

            return result;
        }

        /* Determines if the word already exists in the database.
         */ 
        private async Task<bool> DoesWordExist(Word word)
        {
            bool alreadyExists = false;

            if (word != null)
            {
                // Check that identical word (same term, part of speech, & definitions)
                // doesn't already exist.
                string sql = "Select * from " + Language + "_Terms where Term='" + word.Term + "'";
                List<Word> results = await FileIO.QueryDatabaseAsync(sql);
                
                foreach (Word searchWord in results)
                {
                    alreadyExists = alreadyExists || IsEqualEntry(word, searchWord);
                }
            }

            return alreadyExists;
        }
    }
}
