﻿using System.Collections.Generic;
using System.Linq;
using Vok.Src.Database;
using static Vok.Src.Common.Common;

namespace Vok.Src.Test
{
    /* Represents a vocabulary test, where a user 
     * is fed prompts and submits answers.
     * Prompts and answers can be any aspect of a word.
     * Tests can either update a word's score or not.
     */ 
    class Test
    {
        private List<Word> Terms { get; set; }
        public int CurrentWordIndex { get; private set; }
        public int TestLength { get; private set; }
        public int NumberCorrect { get; private set; } = 0;
        public bool Graded { get; }
        // Stores field names used for prompts/answers
        private List<string> PromptFormat { get; set; }
        private List<string> AnswerFormat { get; set; }

        /* Constructor.
         * Private to ensure that only a valid Test can be created.
         */
        private Test(List<Word> terms, List<string> promptFormat, 
                     List<string> answerFormat, bool graded)
        {
            Terms = terms;
            PromptFormat = promptFormat;
            AnswerFormat = answerFormat;
            Graded = graded;

            CurrentWordIndex = 0;
            TestLength = Terms.Count;
            NumberCorrect = 0;     
        }

        /* Pseudo-constructor.
         * Ensures only a valid Test can be created.
         */
        public static Test CreateTest(List<Word> terms, List<string> promptFormat, 
                                      List<string> answerFormat, bool graded)
        {
            // List cannot be null or empty.
            // Terms list cannot contain null.
            bool isBadList<T>(List<T> list) => list is null || list.Count == 0;
            if (isBadList(terms) || terms.Contains(null) || 
                isBadList(promptFormat) || isBadList(answerFormat))
            {
                return null;
            }

            // Prompts can be: Term, Definitions, Alternative spelling types
            // Answers can be: Term, Definitions, Alternative spelling types,
            //                 Part of Speech, Characteristics
            List<string> validPromptFormats = DB_FIELD_NAMES.Skip(1).Take(2).ToList();
            validPromptFormats.AddRange(currentDB?.AlternativeSpellingTypes);
            List<string> validAnswerFormats = DB_FIELD_NAMES.Skip(3).Take(2).ToList();
            validAnswerFormats.AddRange(validPromptFormats);
            bool hasBadElement(List<string> check, List<string> valid)
            {
                foreach(string elem in check)
                {
                    if (!valid.Contains(elem))
                    {
                        return true;
                    }
                }

                return false;
            }

            if (hasBadElement(promptFormat, validPromptFormats) || 
                hasBadElement(answerFormat, validAnswerFormats))
            {
                return null;
            }

            return new Test(terms, promptFormat, answerFormat, graded);
        }

        /* Returns the word associated with
         * the current test question.
         * Returns null if the test is over.
         */ 
        public Word GetCurrentWord()
        {
            return IsTestOver() ? null : Terms[CurrentWordIndex];
        }

        /* Returns a dictionary where
         * key = field name 
         * value = field value for current word
         * The key, value pairs returned make up the prompt 
         * of the current test question.
         * Returns null if the test is over.
         */ 
        public Dictionary<string, string> GetCurrentPrompt()
        {
            return IsTestOver() ? null : CreateDictionary(PromptFormat, Terms[CurrentWordIndex]);
        }

        /* Returns a dictionary where
         * key = field name
         * value = field value for current word
         * The key, value pairs returned make up the answer 
         * of the current test question.
         * Returns null if the test is over.
         */ 
        public Dictionary<string, string> GetCurrentAnswer()
        {
            return IsTestOver() ? null : CreateDictionary(AnswerFormat, Terms[CurrentWordIndex]);
        }

        /* Submits a set of key, value pairs to be compared with the answer.
         * Returns true if the answer matches and thus is correct.
         * Returns false if:
         * - any aspect of the parameter 
         *   does not match the current expected answer
         * - the test is over
         */ 
        public bool SubmitAnswer(Dictionary<string, string> answer)
        {
            if (IsTestOver() || answer is null)
            {
                return false;
            }

            Word currentWord = Terms[CurrentWordIndex];
            foreach (KeyValuePair<string, string> response in answer)
            {
                switch (response.Key)
                {
                    case nameof(DBFieldNames.Term):
                        if (!currentWord.Term.Equals(response.Value))
                        {
                            return false;
                        }
                        break;
                    case nameof(DBFieldNames.Definitions):
                        // Full definition not expected - just submit one.
                        if (!currentWord.Definitions.Contains(response.Value))
                        {
                            return false;
                        }
                        break;
                    case nameof(DBFieldNames.Part_of_Speech):
                        if (!currentWord.Part.ToStringName().Equals(response.Value))
                        {
                            return false;
                        }
                        break;
                    case nameof(DBFieldNames.Characteristics):
                        // Full characteristic list not expected - 
                        // just submit one.
                        if (!currentWord.Characteristics.Contains(response.Value))
                        {
                            return false;
                        }
                        break;
                    default: // Alternative spelling types
                        try
                        {
                            if (!currentWord.AlternativeSpellings[response.Key]
                                .Equals(response.Value))
                            {
                                return false;
                            }
                        }
                        catch (KeyNotFoundException)
                        {
                            return false;
                        }
                        break;
                }
            }

            return true;
        }

        /* If the test is not over, updates the current score,
         * adding a point if the answer was correct.
         * Additionally updates NumberTimesStudied and
         * NumberTimesCorrect values of current word
         * if this is a graded test.
         */ 
        public void UpdateScore(bool correct = true)
        {
            if (IsTestOver())
            {
                return;
            }

            if (correct)
            {
                NumberCorrect++;
            }

            if (Graded)
            {
                Terms[CurrentWordIndex].IncrementNumTimesStudied();

                if (correct)
                {
                    Terms[CurrentWordIndex].IncrementNumTimesCorrect();
                }
            }
        }

        /* Moves the test to the next prompt index.
         * Returns the next prompt, if it exists.
         * Returns null if the test is over.
         */ 
        public Dictionary<string, string> NextPrompt()
        {
            CurrentWordIndex++;
            if (IsTestOver())
            {
                return null;
            }

            return GetCurrentPrompt();
        }

        /* Returns true if the test has gone through all of the words.
         * Returns false if there are still some words yet to be tested.
         */ 
        public bool IsTestOver()
        {
            return CurrentWordIndex >= TestLength;
        }

        /* Helper to create a Dictionary where
         * key = field name
         * value = field value for passed word
         */ 
        private static Dictionary<string, string> CreateDictionary(List<string> fields, Word word)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();

            foreach (string field in fields)
            {
                switch (field)
                {
                    case nameof(DBFieldNames.Term):
                        dict[field] = word.Term;
                        break;
                    case nameof(DBFieldNames.Definitions):
                        dict[field] = "";
                        // Add all definitions, separating with entry delim and space.
                        // Then remove final entry delim and space.
                        foreach (string definition in word.Definitions)
                        {
                            dict[field] += definition + ENTRY_DELIM + " ";
                        }
                        dict[field] = dict[field].Length > 0 ? 
                            dict[field].Substring(0, dict[field].Length - 2) : "";
                        break;
                    case nameof(DBFieldNames.Part_of_Speech):
                        dict[field] = word.Part.ToStringName();
                        break;
                    case nameof(DBFieldNames.Characteristics):
                        dict[field] = "";
                        // Add all characteristics, separating with entry delim and space.
                        // Then remove final entry delim and space.
                        foreach (string characteristic in word.Characteristics)
                        {
                            dict[field] += characteristic + ENTRY_DELIM + " ";
                        }
                        dict[field] = dict[field].Length > 0 ? 
                            dict[field].Substring(0, dict[field].Length - 2) : "";
                        break;
                    default: // Alternative spelling types
                        try
                        {
                            dict[field] = word.AlternativeSpellings[field];
                        }
                        catch (KeyNotFoundException)
                        {
                            dict[field] = "";
                        }
                        break;
                }
            }

            return dict;
        }
    }
}
