﻿using System;
using System.Drawing;
using System.Windows.Forms;
using static Vok.Src.Common.Common;
using Vok.Src.UI;
using Vok.Src.UI.Predefined_Pages;
using Vok.Src.UI.Controls;
using static Vok.Src.Database.FileIO;

namespace Vok
{
    public partial class Window : Form
    {
        /* Constructor.
         */ 
        public Window()
        {
            InitializeComponent();
            currentForm = this;
            this.Load += LoadAppearance;
        }

        /* Load event handler method for this form.
         */ 
        public void LoadAppearance(object sender, EventArgs e)
        {
            MenuStrip menu = new MenuStrip() { Name = "mnuMain" };
            this.Controls.Add(menu);
            this.MainMenuStrip = menu;

            LoadFormAppearance();
            LoadMenuStripAppearance(menu);

            if (DatabaseExists())
            {
                Page.UpdatePage(StartupPages.HomePage());
            }
            else
            {
                string error = "Error - Database file not found.\n" +
                               "Please download VokDB.mdb and place it " +
                               "in the same folder as this program.\n\n" +
                               "https://gitlab.com/brp6kk/vok/";
                Page.UpdatePage(StartupPages.ErrorPage(error));
                menu.Enabled = false;
            }
        }

        /* Edits this form's appearance to default starting values.
         */ 
        private void LoadFormAppearance()
        {
            this.BackColor = currentPalette.BackDefault;
            this.ForeColor = currentPalette.ForeDefault;
            this.Font = DEFAULT_FONT;
            this.Text = "Vok";
            this.MinimumSize = MINIMUM_WIN_SIZE;
            this.Size = MINIMUM_WIN_SIZE;
            this.ShowIcon = false;
        }

        /* Edits the menustrip's appearance to default starting values.
         * Adds event handler to each menu item's click event.
         */ 
        private void LoadMenuStripAppearance(MenuStrip menu)
        {
            menu.Font = new Font(this.Font.Name, 10);
            menu.BackColor = currentPalette.BackDark;
            string[] menuItems = { "Home", "Options", "Help", "Quit" };
            EventHandler[] events = { HomeClick, OptionsClick, HelpClick, QuitClick };

            for (int index = 0; index < menuItems.Length && index < events.Length; index++)
            {
                ToolStripMenuItem tool = new ToolStripMenuItem()
                {
                    Name = "tool" + menuItems[index],
                    Text = menuItems[index],
                    ForeColor = currentPalette.ForeAccent
                };
                tool.Click += events[index];

                menu.Items.Add(tool);
            }
        }

        /* Method to execute on Home button click.
         * Confirms that user wants to return home,
         * and if so, changes to home page.
         */ 
        private void HomeClick(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to return home?", "Vok", 
                                                  MessageBoxButtons.YesNo, MessageBoxIcon.Warning,
                                                  MessageBoxDefaultButton.Button2);
            if (result.Equals(DialogResult.Yes))
            {
                Page.UpdatePage(StartupPages.HomePage());
            }
        }

        /* Method to execute on Options button click.
         * Shows a form with several changeable options.
         */ 
        private void OptionsClick(object sender, EventArgs e)
        {
            PopupWindow popup = new PopupWindow();

            Page page = PopupPages.OptionsPage();
            page.LoadPage();
            popup.Controls.Add(page.Panel);
            page.Panel.BringToFront();

            popup.Show();
        }

        /* Method to execute on Help button click.
         * Shows a form with a description of Vok.
         */ 
        private void HelpClick(object sender, EventArgs e)
        {
            PopupWindow popup = new PopupWindow();

            Page page = PopupPages.HelpPage();
            page.LoadPage();
            popup.Controls.Add(page.Panel);
            page.Panel.BringToFront();

            popup.Show();
        }

        /* Method to execute on Quit button click.
         * Closes the application.
         */ 
        private void QuitClick(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
